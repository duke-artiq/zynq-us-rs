///! Cache maintenance operations
use core::arch::asm;
use core::mem::size_of_val;

use libregister::RegisterRW;

use crate::asm::{dmb_is, dmb_sys, dsb_is, dsb_sys};

use super::regs::SCTLR_EL3;

// Same for all Cortex-A53s (L1 refers to the D-Cache)
const L1_NWAYS: usize = 4;
const L1_BIT_POS_OF_WAY: usize = 30; // 32 - log2(L1_NWAYS)
pub const LINELEN: usize = 64; // L1 and L2
const LINE_MASK: usize = LINELEN - 1;
const L2_NWAYS: usize = 16;
const L2_BIT_POS_OF_WAY: usize = 28; // 32 - log2(L2_ASSOC)
const BIT_POS_OF_SET: usize = 6; // log2(LINELEN)

// Zynq US+ specific
// const L1_SIZE: usize = 32_768;  // 32 KB
const L1_NSETS: usize = 128; // L1_SIZE / (L1D_ASSOC * LINELEN)
                             // const L2_SIZE: usize = 1_048_576;  // 1 MB
const L2_NSETS: usize = 1024; // L2_SIZE / (L2_ASSOC * LINELEN)

/// Level field for Set/Way-based instructions (Zynq US+ has no L3 Cache)
pub enum CacheLevel {
    L1 = 0b00,
    L2 = 0b01,
}

#[inline(always)]
pub fn enable_icache_el3() {
    SCTLR_EL3.modify(|_, w| w.i(true));
}

#[inline(always)]
pub fn enable_dcache_el3() {
    SCTLR_EL3.modify(|_, w| w.c(true));
}

#[inline(always)]
pub fn translate_set_way_level(set: usize, way: usize, level: CacheLevel) -> usize {
    let (nsets, nways, bit_pos_of_way) = match level {
        CacheLevel::L1 => (L1_NSETS, L1_NWAYS, L1_BIT_POS_OF_WAY),
        CacheLevel::L2 => (L2_NSETS, L2_NWAYS, L2_BIT_POS_OF_WAY),
    };
    assert!(set < nsets, "Invalid set provided");
    assert!(way < nways, "Invalid way provided");
    (set << BIT_POS_OF_SET) | (way << bit_pos_of_way) | ((level as usize) << 1)
}

/// Instruction cache invalidate all to PoU Inner Shareable
#[inline(always)]
pub fn ic_ialluis() {
    unsafe { asm!("ic ialluis") }
}

/// Instruction cache invalidate all to PoU
#[inline(always)]
pub fn ic_iallu() {
    unsafe { asm!("ic iallu") }
}

/// Instruction cache invalidate by virtual address (VA) to PoU
#[inline(always)]
pub fn ic_ivau(addr: usize) {
    unsafe { asm!("ic ivau, {0}", in(reg) addr) }
}

/// Data cache invalidate by VA to PoC
/// Unsafe as it invalidates the entire line containing the address
#[inline(always)]
pub unsafe fn dc_ivac(addr: usize) {
    asm!("dc ivac, {0}", in(reg) addr)
}

/// Data cache invalidate by set/way
#[inline(always)]
pub fn dc_isw(set: usize, way: usize, level: CacheLevel) {
    unsafe { asm!("dc isw, {0}", in(reg) translate_set_way_level(set, way, level)) }
}

/// Data cache clean by set/way
#[inline(always)]
pub fn dc_csw(set: usize, way: usize, level: CacheLevel) {
    unsafe { asm!("dc csw, {0}", in(reg) translate_set_way_level(set, way, level)) }
}

/// Data cache clean and invalidate by set/way
#[inline(always)]
pub fn dc_cisw(set: usize, way: usize, level: CacheLevel) {
    unsafe { asm!("dc cisw, {0}", in(reg) translate_set_way_level(set, way, level)) }
}

/// Data cache clean by VA to PoC
#[inline(always)]
pub fn dc_cvac(addr: usize) {
    unsafe { asm!("dc cvac, {0}", in(reg) addr) }
}

/// Data cache clean by VA to PoU
#[inline(always)]
pub fn dc_cvau(addr: usize) {
    unsafe { asm!("dc cvau, {0}", in(reg) addr) }
}

/// Data cache clean and invalidate by VA to PoC
#[inline(always)]
pub fn dc_civac(addr: usize) {
    unsafe { asm!("dc civac, {0}", in(reg) addr) }
}

#[inline(always)]
pub fn dci_all_l1() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_isw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
}

#[inline(always)]
pub fn dci_all() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_isw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
    for set in 0..L2_NSETS {
        for way in 0..L2_NWAYS {
            dc_isw(set, way, CacheLevel::L2);
        }
    }
    dsb_is();
}

#[inline(always)]
pub fn dcc_all_l1() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_csw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
}

#[inline(always)]
pub fn dcc_all() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_csw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
    for set in 0..L2_NSETS {
        for way in 0..L2_NWAYS {
            dc_csw(set, way, CacheLevel::L2);
        }
    }
    dsb_is();
}

#[inline(always)]
pub fn dcci_all_l1() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_cisw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
}

#[inline(always)]
pub fn dcci_all() {
    dmb_is();
    for set in 0..L1_NSETS {
        for way in 0..L1_NWAYS {
            dc_cisw(set, way, CacheLevel::L1);
        }
    }
    dsb_is();
    for set in 0..L2_NSETS {
        for way in 0..L2_NWAYS {
            dc_cisw(set, way, CacheLevel::L2);
        }
    }
    dsb_is();
}

#[inline]
fn cache_line_addrs(
    start_addr: usize,
    end_addr: usize,
    strict_align: bool,
) -> impl Iterator<Item = usize> {
    if strict_align {
        assert_eq!(
            start_addr & LINE_MASK,
            0,
            "Start address is not aligned to a cache line"
        );
        assert_eq!(
            end_addr & LINE_MASK,
            LINE_MASK,
            "End address is not aligned to a cache line"
        );
    }
    let start_line_addr = start_addr & !LINE_MASK;

    (start_line_addr..=end_addr).step_by(LINELEN)
}

#[inline]
fn object_cache_line_addrs<T>(object: &T, strict_align: bool) -> impl Iterator<Item = usize> {
    let start_addr = object as *const _ as usize;
    let end_addr = start_addr + size_of_val(object) - 1;
    cache_line_addrs(start_addr, end_addr, strict_align)
}

#[inline]
fn slice_cache_line_addrs<T>(slice: &[T], strict_align: bool) -> impl Iterator<Item = usize> {
    let start_addr = &slice[0] as *const _ as usize;
    let end_addr = start_addr + size_of_val(slice) - 1;
    cache_line_addrs(start_addr, end_addr, strict_align)
}

pub fn dcci_poc<T>(object: &T) {
    dmb_sys();
    for addr in object_cache_line_addrs(object, false) {
        dc_civac(addr);
    }
    dsb_sys();
}

pub fn dcci_slice_poc<T>(slice: &[T]) {
    if slice.len() == 0 {
        return;
    }
    dmb_sys();
    for addr in slice_cache_line_addrs(slice, false) {
        dc_civac(addr);
    }
    dsb_sys();
}

pub fn dcc_poc<T>(object: &T) {
    dmb_sys();
    for addr in object_cache_line_addrs(object, false) {
        dc_cvac(addr);
    }
    dsb_sys();
}

pub fn dcc_pou<T>(object: &T) {
    dmb_is();
    for addr in object_cache_line_addrs(object, false) {
        dc_cvau(addr);
    }
    dsb_is();
}

pub fn dcc_slice_poc<T>(slice: &[T]) {
    if slice.len() == 0 {
        return;
    }
    dmb_sys();
    for addr in slice_cache_line_addrs(slice, false) {
        dc_cvac(addr);
    }
    dsb_sys();
}

pub fn dcc_slice_pou<T>(slice: &[T]) {
    if slice.len() == 0 {
        return;
    }
    dmb_is();
    for addr in slice_cache_line_addrs(slice, false) {
        dc_cvau(addr);
    }
    dsb_is();
}

pub fn dci_poc<T>(object: &T) {
    dmb_sys();
    for addr in object_cache_line_addrs(object, true) {
        unsafe {
            dc_ivac(addr);
        }
    }
    dsb_sys();
}

pub fn dci_slice_poc<T>(slice: &[T]) {
    if slice.len() == 0 {
        return;
    }
    dmb_sys();
    for addr in slice_cache_line_addrs(slice, true) {
        unsafe {
            dc_ivac(addr);
        }
    }
    dsb_sys();
}

/// Invalidate all stage 1 translations used at EL1
#[inline(always)]
pub fn tlbi_alle1() {
    unsafe { asm!("tlbi alle1") }
}

/// Invalidate all stage 1 translations used at EL2
#[inline(always)]
pub fn tlbi_alle2() {
    unsafe { asm!("tlbi alle2") }
}

/// Invalidate all stage 1 translations used at EL3
#[inline(always)]
pub fn tlbi_alle3() {
    unsafe { asm!("tlbi alle3") }
}

/// Invalidate all stage 1 translations used at EL3 - inner shareable
#[inline(always)]
pub fn tlbi_alle3is() {
    unsafe { asm!("tlbi alle3is") }
}

/// Invalidate the TLB entry used to translate this address
#[inline(always)]
pub fn tlbi_vae3(addr: usize) {
    unsafe { asm!("tlbi vae3, {0}", in(reg) addr) }
}

/// Invalidate the TLB entries used to translate this address - inner shareable
#[inline(always)]
pub fn tlbi_vae3is(addr: usize) {
    unsafe { asm!("tlbi vae3is, {0}", in(reg) addr) }
}
