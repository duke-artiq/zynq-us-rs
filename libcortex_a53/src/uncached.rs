// Copied from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: be672ab
// File: libcortex_a9/src/uncached.rs
// Modified for different MMU structure.
// From what I can tell
// (https://developer.arm.com/documentation/ddi0406/c/Appendices/ARMv4-and-ARMv5-Differences/Application-level-memory-support/Memory-model-and-memory-ordering?lang=en),
// setting bufferable to false in the Cortex A9 is equivalent to marking the region as
// write-through cacheable. For the Zynq/ZynqMP ethernet buffer descriptors, that's sufficient.
// However, the Cortex A53 doesn't support write-through, so I had to make the memory region fully
// non-cacheable.
use crate::{
    mmu::{lookup_level, update_block, BlockAttrs, Level, L2_BLOCK_SIZE},
    regs::Shareability,
};
use alloc::alloc::{dealloc, Layout, LayoutError};
use core::{
    mem::{align_of, size_of},
    ops::{Deref, DerefMut},
};

pub struct UncachedSlice<T: 'static> {
    layout: Layout,
    slice: &'static mut [T],
}

impl<T> UncachedSlice<T> {
    /// allocates in chunks of 2 MB
    pub fn new<F: Fn() -> T>(len: usize, default: F) -> Result<Self, LayoutError> {
        // round to full pages
        let size = ((len * size_of::<T>() - 1) | (L2_BLOCK_SIZE - 1)) + 1;
        let align = align_of::<T>().max(L2_BLOCK_SIZE);
        let layout = Layout::from_size_align(size, align)?;
        let ptr = unsafe { alloc::alloc::alloc(layout).cast::<T>() };
        assert!(matches!(lookup_level(ptr), Level::L2));
        let start = ptr as usize;
        assert_eq!(start & (L2_BLOCK_SIZE - 1), 0);

        // TODO: calculate neighboring (as it pertains to the contiguous bit) blocks and set their
        // contiguous bit to 0 -- not doing so means we can't reliably utilize that functionality
        // for any of the SDRAM

        for block_start in (start..(start + size)).step_by(L2_BLOCK_SIZE) {
            // non-shareable device
            update_block(
                block_start as *const (),
                |old_maybe_attrs| match old_maybe_attrs {
                    Some(old_attrs) => Some(BlockAttrs {
                        xn: true,
                        shareability: Shareability::OuterShareable,
                        attr_indx: 2, // normal non-cacheable
                        ..*old_attrs
                    }),
                    None => panic!(
                        "Translation table not initialized at {:#08X}",
                        block_start as *const () as usize
                    ),
                },
            );
        }

        let slice = unsafe { core::slice::from_raw_parts_mut(ptr, len) };
        // initialize
        for e in slice.iter_mut() {
            *e = default();
        }
        Ok(UncachedSlice { layout, slice })
    }
}

// TODO: why not?
/// Does not yet mark the pages cachable again
impl<T> Drop for UncachedSlice<T> {
    fn drop(&mut self) {
        unsafe {
            dealloc(self.slice.as_mut_ptr() as *mut _ as *mut u8, self.layout);
        }
    }
}

impl<T> Deref for UncachedSlice<T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        self.slice
    }
}

impl<T> DerefMut for UncachedSlice<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.slice
    }
}
