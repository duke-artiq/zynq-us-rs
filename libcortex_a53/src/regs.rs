#![allow(non_camel_case_types)]
use core::arch::asm;
use libregister::{
    register_bit, register_bits, register_bits_typed, RegisterR, RegisterRW, RegisterW,
};

// Macros based on https://git.m-labs.hk/M-labs/zynq-rs/libregister
macro_rules! register_common {
    ($mod_name: ident, $struct_name: ident, $inner: ty) => {
        #[repr(C)]
        pub struct $struct_name;

        pub mod $mod_name {
            #[derive(Copy, Clone)]
            pub struct Read {
                pub inner: $inner,
            }
            #[derive(Copy, Clone)]
            pub struct Write {
                pub inner: $inner,
            }
        }
    };
}
macro_rules! register_r {
    ($mod_name: ident, $struct_name: ident, $inner: ty, $asm_instr: tt) => {
        impl RegisterR for $struct_name {
            type R = $mod_name::Read;

            #[inline]
            #[allow(asm_sub_register)]
            fn read(&self) -> Self::R {
                let inner: $inner;
                unsafe { asm!($asm_instr, out(reg) inner) }
                $mod_name::Read { inner }
            }
        }
    };
}

macro_rules! register_w {
    ($mod_name: ident, $struct_name: ident, $inner: ty, $asm_instr: tt) => {
        impl RegisterW for $struct_name {
            type W = $mod_name::Write;

            #[inline]
            fn zeroed() -> $mod_name::Write {
                $mod_name::Write { inner: 0 }
            }

            #[inline]
            #[allow(asm_sub_register)]
            fn write(&mut self, value: Self::W) {
                unsafe {
                    asm!($asm_instr, in(reg) value.inner);
                }
            }
        }
    };
}

macro_rules! register_rw {
    ($mod_name: ident, $struct_name: ident) => {
        impl RegisterRW for $struct_name {
            #[inline]
            fn modify<F: FnOnce(Self::R, Self::W) -> Self::W>(&mut self, f: F) {
                let r = self.read();
                self.write(f(r, $mod_name::Write { inner: r.inner }));
            }
        }
    };
}

macro_rules! register {
    ($mod_name: ident, $struct_name: ident, RO, $inner: ty, $read_asm: tt) => {
        register_common!($mod_name, $struct_name, $inner);
        register_r!($mod_name, $struct_name, $inner, $read_asm);
    };
    ($mod_name: ident, $struct_name: ident, RW, $inner: ty, $read_asm: tt, $write_asm: tt) => {
        register_common!($mod_name, $struct_name, $inner);
        register_r!($mod_name, $struct_name, $inner, $read_asm);
        register_w!($mod_name, $struct_name, $inner, $write_asm);
        register_rw!($mod_name, $struct_name);
    };
}

// Stack Pointer
register!(sp, SP, RW, u64, "mov {0}, sp", "mov sp, {0}");

// Multiprocessor Affinity Register
register!(mpidr_el1, MPIDR_EL1, RO, u64, "mrs {0}, mpidr_el1");
register_bit!(mpidr_el1, single_core, 30);
register_bit!(mpidr_el1, mt, 24);
register_bits!(mpidr_el1, aff2, u8, 16, 23);
register_bits!(mpidr_el1, aff1, u8, 8, 15);
register_bits!(mpidr_el1, cpu_id, u8, 0, 7);

// System Control Register - EL3
register!(
    sctlr_el3,
    SCTLR_EL3,
    RW,
    u32,
    "mrs {0}, sctlr_el3",
    "msr sctlr_el3, {0}"
);
register_bit!(sctlr_el3, ee, 25);
register_bit!(sctlr_el3, wxn, 19);
register_bit!(sctlr_el3, i, 12);
register_bit!(sctlr_el3, sa, 3);
register_bit!(sctlr_el3, c, 2);
register_bit!(sctlr_el3, a, 1);
register_bit!(sctlr_el3, m, 0);

// TODO: incorporate the notion of "RES1" into macros
//   in the meantime I'll just make an impl that sets those bits
impl SCTLR_EL3 {
    pub fn zeroed_res1() -> sctlr_el3::Write {
        let res1 = (0b11 << 28) | (0b11 << 22) | (1 << 18) | (1 << 16) | (1 << 11) | (0b11 << 4);
        sctlr_el3::Write { inner: res1 }
    }
}

register!(
    scr_el3,
    SCR_EL3,
    RW,
    u32,
    "mrs {0}, scr_el3",
    "msr scr_el3, {0}"
);
register_bit!(scr_el3, twe, 13);
register_bit!(scr_el3, twi, 12);
register_bit!(scr_el3, st, 11);
register_bit!(scr_el3, rw, 10);
register_bit!(scr_el3, sif, 9);
register_bit!(scr_el3, hce, 8);
register_bit!(scr_el3, smd, 7);
register_bit!(scr_el3, ea, 3);
register_bit!(scr_el3, fiq, 2);
register_bit!(scr_el3, irq, 1);
register_bit!(scr_el3, ns, 0);

impl SCR_EL3 {
    pub fn zeroed_res1() -> scr_el3::Write {
        let res1 = 0b11 << 4;
        scr_el3::Write { inner: res1 }
    }
}

#[repr(u8)]
pub enum PASize {
    S32 = 0b000,
    S36 = 0b001,
    S40 = 0b010,
}

#[repr(u8)]
pub enum GranuleSize {
    S4KB = 0b00,
    S16KB = 0b01,
}

#[repr(u8)]
#[derive(Debug)]
pub enum Shareability {
    NonShareable = 0b00,
    _Reserved = 0b01,
    OuterShareable = 0b10,
    InnerShareable = 0b11,
}

#[repr(u8)]
pub enum Cacheability {
    NonCacheable = 0b00,
    // write-back write-allocate
    WBWACacheable = 0b01,
    // write-through
    WTCacheable = 0b10,
    // write-back no write-allocate
    WBNoWACacheable = 0b11,
}
// note: reset value is undefined
register!(
    tcr_el3,
    TCR_EL3,
    RW,
    u32,
    "mrs {0}, tcr_el3",
    "msr tcr_el3, {0}"
);
// DS, 32: RES0
register_bit!(tcr_el3, tbi, 20);
register_bits_typed!(tcr_el3, ps, u8, PASize, 16, 18);
register_bits_typed!(tcr_el3, tg0, u8, GranuleSize, 14, 15);
register_bits_typed!(tcr_el3, sh0, u8, Shareability, 12, 13);
register_bits_typed!(tcr_el3, orgn0, u8, Cacheability, 10, 11);
register_bits_typed!(tcr_el3, irgn0, u8, Cacheability, 8, 9);
register_bits!(tcr_el3, t0sz, u8, 0, 5);

impl TCR_EL3 {
    pub fn zeroed_res1() -> tcr_el3::Write {
        let res1 = (1 << 31) | (1 << 23);
        tcr_el3::Write { inner: res1 }
    }
}

// note: reset value is undefined
register!(
    mair_el3,
    MAIR_EL3,
    RW,
    u64,
    "mrs {0}, mair_el3",
    "msr mair_el3, {0}"
);
register_bits!(mair_el3, attr7, u8, 56, 63);
register_bits!(mair_el3, attr6, u8, 48, 55);
register_bits!(mair_el3, attr5, u8, 40, 47);
register_bits!(mair_el3, attr4, u8, 32, 39);
register_bits!(mair_el3, attr3, u8, 24, 31);
register_bits!(mair_el3, attr2, u8, 16, 23);
register_bits!(mair_el3, attr1, u8, 8, 15);
register_bits!(mair_el3, attr0, u8, 0, 7);

// note: reset value is undefined
register!(
    ttbr0_el3,
    TTBR0_EL3,
    RW,
    u64,
    "mrs {0}, ttbr0_el3",
    "msr ttbr0_el3, {0}"
);
register_bits!(ttbr0_el3, baddr, u64, 0, 47);

// Architectural Feature Access Control Register
register!(
    cpacr_el1,
    CPACR_EL1,
    RW,
    u32,
    "mrs {0}, cpacr_el1",
    "msr cpacr_el1, {0}"
);
// A53 TRM:
// 0bX0
//  Trap any instruction in EL0 or EL1 that uses registers associated with Advanced SIMD and
//  Floating-point execution. The reset value is 0b00.
// 0b01
//  Trap any instruction in EL0 that uses registers associated with Advanced SIMD and
//  Floating-point execution. Instructions in EL1 are not trapped.
// 0b11
//  No instructions are trapped.
register_bits!(cpacr_el1, fp_en, u8, 20, 21);

// Architectural Feature Trap Register, EL3
register!(
    cptr_el3,
    CPTR_EL3,
    RW,
    u32,
    "mrs {0}, cptr_el3",
    "mrs cptr_el3, {0}"
);
register_bit!(cptr_el3, t_cpac, 31);
register_bit!(cptr_el3, t_fp, 10);

register!(
    cpu_ectlr_el1,
    CPUECTLR_EL1,
    RW,
    u64,
    "mrs {0}, S3_1_C15_C2_1",
    "msr S3_1_C15_C2_1, {0}"
);
register_bit!(cpu_ectlr_el1, smp_en, 6);
register_bits!(cpu_ectlr_el1, fp_ret_ctl, u8, 3, 5);
register_bits!(cpu_ectlr_el1, pcu_ret_ctl, u8, 0, 2);

register!(
    cntfrq_el0,
    CNTFRQ_EL0,
    RW,
    u64,
    "mrs {0}, CNTFRQ_EL0",
    "msr CNTFRQ_EL0, {0}"
);
register_bits!(cntfrq_el0, clk_frq, u32, 0, 31);

register!(cntpct_el0, CNTPCT_EL0, RO, u64, "mrs {0}, CNTPCT_EL0");
register_bits!(cntpct_el0, count, u64, 0, 63);

register!(daif, DAIF, RW, u64, "mrs {0}, DAIF", "msr DAIF, {0}");
register_bit!(daif, debug_mask, 9);
register_bit!(daif, serror_mask, 8);
register_bit!(daif, irq_mask, 7);
register_bit!(daif, fiq_mask, 6);

register!(
    cpuactlr_el1,
    CPUACTLR_EL1,
    RW,
    u64,
    "mrs {0}, S3_1_C15_C2_0",
    "msr S3_1_C15_C2_0, {0}"
);
register_bit!(cpuactlr_el1, en_dcc_as_ci, 44);
register_bit!(cpuactlr_el1, fp_di_dis, 30);
register_bit!(cpuactlr_el1, di_dis, 29);
register_bits!(cpuactlr_el1, ra_dis, u8, 27, 28);
register_bits!(cpuactlr_el1, l1_ra_dis, u8, 25, 26);
register_bit!(cpuactlr_el1, dt_ah, 24);
register_bit!(cpuactlr_el1, stb_pf_rs, 23);
register_bit!(cpuactlr_el1, stb_pf_dis, 22);
register_bit!(cpuactlr_el1, ifu_th_dis, 21);
register_bits!(cpuactlr_el1, n_pf_strm, u8, 19, 20);
register_bit!(cpuactlr_el1, dst_dis, 18);
register_bit!(cpuactlr_el1, stride, 17);
register_bits!(cpuactlr_el1, l1_pctl, u8, 13, 15);
register_bit!(cpuactlr_el1, dodmbs, 10);
register_bit!(cpuactlr_el1, l1d_ei_en, 6);

#[repr(u8)]
#[derive(Debug)]
pub enum ExceptionClass {
    // skipping irrelevant exceptions (lower EL trapping, AArch32 stuff, etc.)
    Unknown = 0b000000,
    TrappedFPU = 0b000111, // FPU, SME, SVE, AdvSIMD
    IllegalExecutionState = 0b001110,
    SVCFromAArch64 = 0b010101,
    HVCFromAArch64 = 0b010110,
    SMCFromAArch64 = 0b010111,
    InstructionAbortSameEL = 0b100001,
    PCAlignmentFault = 0b100010,
    DataAbortSameEL = 0b100101,
    SPAlignmentFault = 0b100110,
    TrappedFPException = 0b101100,
    SError = 0b101111,
    BRK = 0b111100,
}

register!(
    esr_el3,
    ESR_EL3,
    RW,
    u64,
    "mrs {0}, ESR_EL3",
    "msr ESR_EL3, {0}"
);
register_bits_typed!(esr_el3, ec, u8, ExceptionClass, 26, 31);
register_bit!(esr_el3, il, 25);
register_bits!(esr_el3, iss, u32, 0, 24);

register!(
    par_el1,
    PAR_EL1,
    RW,
    u64,
    "mrs {0}, PAR_EL1",
    "msr PAR_EL1, {0}"
);
// TODO: different definitions if the translation fails (f = 1)
register_bits!(par_el1, attr, u8, 56, 63);
register_bits!(par_el1, pa, u64, 12, 47);
register_bit!(par_el1, ns, 9);
register_bits_typed!(par_el1, sha, u8, Shareability, 7, 8);
register_bit!(par_el1, f, 0);
