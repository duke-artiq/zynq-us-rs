use core::ops::{Deref, DerefMut};
use core::sync::atomic::{AtomicU32, Ordering};
use core::cell::UnsafeCell;
use core::task::{Context, Poll};
use core::pin::Pin;
use core::future::Future;
use libregister::RegisterRW;

use crate::regs::DAIF;

pub fn enter_critical() -> bool {
    let mut irq_en: bool = false;
    DAIF.modify(|r, w| { irq_en = !r.irq_mask(); w.irq_mask(true) });
    irq_en
}

pub fn exit_critical(irq_en: bool) {
    DAIF.modify(|_, w| w.irq_mask(!irq_en));
}

// Copied from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: f20c008
// File: libcortex_a9/src/mutex.rs
// Original authors:  pca006132, Astro, mwojcik
const LOCKED: u32 = 1;
const UNLOCKED: u32 = 0;

/// Mutex implementation for Cortex-A53
pub struct Mutex<T> {
    locked: AtomicU32,
    inner: UnsafeCell<T>,
}

unsafe impl<T: Send> Sync for Mutex<T> {}
unsafe impl<T: Send> Send for Mutex<T> {}

struct Fut<'a, T>(&'a Mutex<T>);

impl<'a, T> Future for Fut<'a, T> {
    type Output = MutexGuard<'a, T>;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let irq = enter_critical();
        if self.0.locked.compare_exchange_weak(UNLOCKED, LOCKED, Ordering::AcqRel, Ordering::Relaxed).is_err() {
            exit_critical(irq);
            cx.waker().wake_by_ref();
            Poll::Pending
        }
        else {
            Poll::Ready(MutexGuard { mutex: self.0, irq })
        }
    }
}

impl<T> Mutex<T> {
    /// Constructor, const-fn
    pub const fn new(inner: T) -> Self {
        Mutex{
            locked: AtomicU32::new(UNLOCKED),
            inner: UnsafeCell::new(inner),
        }
    }

    /// Lock the Mutex, blocks when already locked
    pub fn lock(&self) -> MutexGuard<T> {
        let mut irq = enter_critical();
        while self.locked.compare_exchange_weak(UNLOCKED, LOCKED, Ordering::AcqRel, Ordering::Relaxed).is_err() {
            exit_critical(irq);
            irq = enter_critical();
        }
        MutexGuard { mutex: self, irq }
    }

    pub async fn async_lock(&self) -> MutexGuard<'_, T> {
        Fut(&self).await
    }

    pub fn try_lock(&self) -> Option<MutexGuard<T>> {
        let irq = enter_critical();
        if self.locked.compare_exchange_weak(UNLOCKED, LOCKED, Ordering::AcqRel, Ordering::Relaxed).is_err() {
            exit_critical(irq);
            None
        } else {
            Some(MutexGuard { mutex: self, irq })
        }
    }

    fn unlock(&self) {
        self.locked.store(UNLOCKED, Ordering::Release);
    }
}

/// Returned by `Mutex.lock()`, allows access to data via
/// `Deref`/`DerefMut`
pub struct MutexGuard<'a, T> {
    mutex: &'a Mutex<T>,
    irq: bool,
}

impl<'a, T> Deref for MutexGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &*self.mutex.inner.get() }
    }
}

impl<'a, T> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.mutex.inner.get() }
    }
}

/// Automatically `Mutex.unlock()` when this reference is dropped
impl<'a, T> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        self.mutex.unlock();
        exit_critical(self.irq);
    }
}

