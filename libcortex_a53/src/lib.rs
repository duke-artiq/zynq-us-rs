#![no_std]
#![feature(never_type)]
#![feature(naked_functions)]

extern crate alloc;

use core::arch::{asm, global_asm};

pub mod asm;
pub mod cache;
pub mod mmu;
pub mod mutex;
pub mod regs;
pub mod semaphore;
pub mod sync_channel;
pub mod uncached;

global_asm!(include_str!("exceptions.S"));

pub fn enable_fpu() {
    unsafe {
        asm!(
            // disable trapping for all ELs
            "msr CPTR_EL3, xzr",
            "msr CPTR_EL2, xzr",
            "mov {tmp}, #(0x3 << 20)",
            "msr CPACR_EL1, {tmp}",
            "isb",
            // zero floating point registers
            "fmov d0, xzr",
            "fmov d1, xzr",
            "fmov d2, xzr",
            "fmov d3, xzr",
            "fmov d4, xzr",
            "fmov d5, xzr",
            "fmov d6, xzr",
            "fmov d7, xzr",
            "fmov d8, xzr",
            "fmov d9, xzr",
            "fmov d10, xzr",
            "fmov d11, xzr",
            "fmov d12, xzr",
            "fmov d13, xzr",
            "fmov d14, xzr",
            "fmov d15, xzr",
            "fmov d16, xzr",
            "fmov d17, xzr",
            "fmov d18, xzr",
            "fmov d19, xzr",
            "fmov d20, xzr",
            "fmov d21, xzr",
            "fmov d22, xzr",
            "fmov d23, xzr",
            "fmov d24, xzr",
            "fmov d25, xzr",
            "fmov d26, xzr",
            "fmov d27, xzr",
            "fmov d28, xzr",
            "fmov d29, xzr",
            "fmov d30, xzr",
            "fmov d31, xzr",
            tmp = out(reg) _,
        );
    }
}

#[macro_export]
macro_rules! interrupt_handler {
    ($name:ident, $name2:ident, $stack0:ident, $stack1:ident, $stack2:ident, $stack3:ident, $body:block) => {
        #[link_section = ".text.boot"]
        #[no_mangle]
        #[naked]
        pub unsafe extern "C" fn $name() -> ! {
            core::arch::asm!(
                // get CPU ID within cluster (0-3)
                "mrs x1, MPIDR_EL1",
                "tbnz x1, #1, 1f",
                "tbnz x1, #0, 0f",
                // core 0
                concat!("ldr x2, =", stringify!($stack0)),
                "mov sp, x2",
                concat!("bl ", stringify!($name2)),
                "0:",
                // core 1
                concat!("ldr x2, =", stringify!($stack1)),
                "mov sp, x2",
                concat!("bl ", stringify!($name2)),
                "1:",
                "tbnz x0, #0, 2f",
                // core 2
                concat!("ldr x2, =", stringify!($stack2)),
                "mov sp, x2",
                concat!("bl ", stringify!($name2)),
                "2:",
                // core 3
                concat!("ldr x2, =", stringify!($stack3)),
                "mov sp, x2",
                concat!("bl ", stringify!($name2)),
                options(noreturn)
            );
        }

        #[no_mangle]
        pub unsafe extern "C" fn $name2() -> ! $body
    };
}
