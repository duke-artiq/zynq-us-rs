///! Memory Management Unit - AArch64 VMSAv8
use bit_field::BitField;
use core::mem::transmute;
use libregister::{RegisterR, RegisterRW, RegisterW};

use crate::{
    asm::{at_s1e3r, dsb_is, isb},
    cache::{dcc_all, ic_iallu, tlbi_alle3is, tlbi_alle3},
    regs::{
        par_el1, Cacheability, GranuleSize, PASize, Shareability, CPUECTLR_EL1, DAIF, MAIR_EL3,
        PAR_EL1, SCTLR_EL3, TCR_EL3, TTBR0_EL3,
    },
};

// Using a 64 KB granule with the A53's maximum PA size of 40 bits would mean the L1 table could
// only contain table descriptors, resulting in at least 2-level table walks and in many cases
// 3 levels since the L2 block size would be 512 MB, which is too coarse for most of the Zynq US+
// memory map.
const GRANULE_SIZE: GranuleSize = GranuleSize::S4KB;
const GRANULE_NBITS: usize = 12;
// Using the full 40-bit address space would result in TCR_EL3.T0SZ = 24 and an initial lookup
// level of 0 (see ARMv8 TRM Table D8-11), effectively indexed by a single bit.
// 36-bit addressing allows lookups to start at level 1 (block size 1GB) and is sufficient for
// the components of the Zynq US+ address map that we care about (for now, at least).
const PA_SIZE: PASize = PASize::S36;
const PA_NBITS: usize = 36;

#[repr(C)]
#[derive(Clone, Copy)]
pub struct TableEntry(u64);

/// Permissions for subsequent lookup levels, ARMv8 TRM Table D8-41.
#[repr(u8)]
pub enum APTable {
    NoEffect = 0b00,
    Privileged,
    ReadOnly,
    PrivilegedReadOnly,
}

pub struct TableAttrs {
    pub ns_table: bool,    // 63
    pub ap_table: APTable, // 62:61
    pub xn_table: bool,    // 60
                           // EL3 only supports one VA range, so PXNTable is RES0
                           // 1:0 = 0b11
}

// Everything in a block descriptor except the address
pub struct BlockAttrs {
    // no PBHA (ignored if no FEAT_HPDS2)
    pub xn: bool, // 54
    // no PXN
    // for a 4 KB granule, ARMv8 requires 16 adjacent block/page entries, 128-byte (16-entry) aligned, with the OA range 16-block/page aligned in order to correctly set the contiguous bit to 0b1
    // TODO: It's unclear whether the cortex A53 utilizes the contiguous bit to cache regions as a
    // single TLB entry. The IPA cache RAM has a contiguous field, but the IPA cache RAM is only
    // used for non-secure EL0/1 stage 2 translations. Considering that the main TLB splits 1 GB
    // blocks into 2x512 MB TLB entries, it seems safe to say the contiguous bit doesn't matter for
    // L1 block descriptors, but L2 is the main concern.
    pub contiguous: bool, // 52
    // no DBM (RES0 if no FEAT_HAFDBS)
    // no GP (RES0 if no FEAT_BTI)
    // no nT (RES0 if no FEAT_BBM)
    // no OA (RES0 if no FEAT_LPA)
    // no nG (RES0 for stage 1 translations with a single privilege level)
    pub access_flag: bool,          // 10
    pub shareability: Shareability, // 9:8
    // for stage 1 translations with a single privilege level, 7 is ap[2] and 6 is RES1
    pub read_only: bool, // ap[2]: 0b0 = RW, 0b1 = RO
    pub ns: bool,        // 5
    pub attr_indx: u8,   // 4:2
                         // 1:0 = 0b01
}

#[allow(unused)]
pub struct PageDescriptor {
    // no PBHA (ignored if no FEAT_HPDS2)
    pub xn: bool, // 54
    // no PXN
    pub contiguous: bool, // 52
    // no DBM (RES0 if no FEAT_HAFDBS)
    // no GP (RES0 if no FEAT_BTI)
    pub output_addr: u64, // 47:n where n = 12 (4 KB) or 16 (64 KB)
    // no nT (RES0 if no FEAT_BBM)
    // no OA (RES0 if no FEAT_LPA)
    // no nG (RES0 for stage 1 translations with a single privilege level)
    pub access_flag: bool, // 10, ARMv8 TRM: "Descriptors with AF set to zero can never be cached in a TLB"
    pub shareability: u8,  // 9:8
    // for stage 1 translations with a single privilege level, 7 is ap[2] and 6 is RES1
    pub read_only: bool, // ap[2]: 0b0 = RW, 0b1 = RO
    pub ns: bool,        // 5
    pub attr_indx: u8,   // 4:2
                         // 1:0 = 0b11
}

// Tables must be aligned to a translation granule
const NEXT_LEVEL_ADDR_MASK: u64 = ((1 << PA_NBITS) - 1) & !((1 << GRANULE_NBITS) - 1);

// L1 table is indexed by IA bits 38:30 (4 KB granule)
const L1_IDX_OFFSET: usize = 30;
// L2 table is indexed by IA bits 29:21 (4 KB granule)
const L2_IDX_OFFSET: usize = 21;

const L1_BLOCK_ADDR_MASK: u64 = ((1 << PA_NBITS) - 1) & !((1 << L1_IDX_OFFSET) - 1);
const L2_BLOCK_ADDR_MASK: u64 = ((1 << PA_NBITS) - 1) & !((1 << L2_IDX_OFFSET) - 1);

#[repr(u8)]
pub enum EntryType {
    Table = 0b11, // also used for page entries but this implementation doesn't have those
    Block = 0b01,
    Reserved = 0b00,
    _Invalid = 0b10,
}

pub enum Level {
    L1,
    L2,
}

pub enum EntryAttrs {
    /// Attributes for a block entry.
    ///
    /// `None` represents a reserved block.
    Block(Option<BlockAttrs>),
    /// Attributes for a next-level table entry.
    Table(TableAttrs),
}

impl TableEntry {
    /// Convert the given physical base address and attributes into the `u64` representation the
    /// translation tables expect.
    ///
    /// For block entries, `phys_base` is the starting physical address of the block.
    /// For next-level table entries, `phys_base` is the starting physical address of the next-level table.
    pub fn from_attrs(phys_base: u64, level: Level, attrs: EntryAttrs) -> Self {
        let mut self_: TableEntry;
        match attrs {
            EntryAttrs::Block(maybe_block_attrs) => {
                match level {
                    Level::L1 => assert_eq!(phys_base & !L1_BLOCK_ADDR_MASK, 0),
                    Level::L2 => assert_eq!(phys_base & !L2_BLOCK_ADDR_MASK, 0),
                }
                self_ = TableEntry(phys_base);
                if let Some(block_attrs) = maybe_block_attrs {
                    self_.set_block_attrs(block_attrs);
                }
            }
            EntryAttrs::Table(table_attrs) => {
                assert_eq!(phys_base & !NEXT_LEVEL_ADDR_MASK, 0);
                self_ = TableEntry(phys_base);
                self_.set_table_attrs(table_attrs);
            }
        }
        self_
    }

    pub fn set_attrs(&mut self, attrs: EntryAttrs) {
        match attrs {
            EntryAttrs::Block(Some(block_attrs)) => self.set_block_attrs(block_attrs),
            EntryAttrs::Block(None) => self.set_reserved(),
            EntryAttrs::Table(table_attrs) => self.set_table_attrs(table_attrs),
        }
    }

    fn set_reserved(&mut self) {
        self.0.set_bits(0..=1, EntryType::Reserved as u64);
    }

    fn set_block_attrs(&mut self, block_attrs: BlockAttrs) {
        self.0.set_bit(54, block_attrs.xn);
        self.0.set_bit(52, block_attrs.contiguous);
        self.0.set_bit(10, block_attrs.access_flag);
        self.0.set_bits(8..=9, block_attrs.shareability as u64);
        self.0.set_bit(7, block_attrs.read_only);
        self.0.set_bit(6, true);
        self.0.set_bit(5, block_attrs.ns);
        self.0.set_bits(2..=4, block_attrs.attr_indx.into());
        self.0.set_bits(0..=1, EntryType::Block as u64);
    }

    fn set_table_attrs(&mut self, table_attrs: TableAttrs) {
        self.0.set_bit(63, table_attrs.ns_table);
        self.0.set_bits(61..=62, table_attrs.ap_table as u64);
        self.0.set_bit(60, table_attrs.xn_table);
        self.0.set_bits(0..=1, EntryType::Table as u64);
    }

    pub fn entry_type(&self) -> EntryType {
        unsafe { transmute(self.0.get_bits(0..=1) as u8) }
    }

    pub fn get_attrs(&self) -> EntryAttrs {
        match self.entry_type() {
            EntryType::Block => EntryAttrs::Block(Some(self.get_block_attrs())),
            EntryType::Reserved => EntryAttrs::Block(None),
            EntryType::Table => EntryAttrs::Table(self.get_table_attrs()),
            EntryType::_Invalid => panic!("This is not a valid entry"),
        }
    }

    fn get_block_attrs(&self) -> BlockAttrs {
        BlockAttrs {
            xn: self.0.get_bit(54),
            contiguous: self.0.get_bit(52),
            access_flag: self.0.get_bit(10),
            shareability: unsafe { transmute(self.0.get_bits(8..=9) as u8) },
            read_only: self.0.get_bit(7),
            ns: self.0.get_bit(5),
            attr_indx: self.0.get_bits(2..=4) as u8,
        }
    }

    fn get_table_attrs(&self) -> TableAttrs {
        TableAttrs {
            ns_table: self.0.get_bit(63),
            ap_table: unsafe { transmute(self.0.get_bits(61..=62) as u8) },
            xn_table: self.0.get_bit(60),
        }
    }
}

// same for all levels except -1
const MAX_ENTRIES: usize = 512;

// TODO: Everything below is Zynq-specific.
//   Maybe more appropriate to put it in libboard_zynqmp?

// 0x0000_0000:0xFFFF_FFFF (4 GB)
const NUM_L2_TABLES: usize = 4;
// While an L2 table is technically defined as 512 entries, concatenating 4 into a single struct
// satisfies the same alignment requirements.
#[repr(C, align(4096))]
pub struct L2Table {
    table: [TableEntry; MAX_ENTRIES * NUM_L2_TABLES],
}

static mut L2_TABLE: L2Table = L2Table {
    table: [TableEntry(0); MAX_ENTRIES * NUM_L2_TABLES],
};

impl L2Table {
    pub fn get() -> &'static mut Self {
        unsafe { &mut L2_TABLE }
    }

    pub fn setup(&mut self) -> &Self {
        // [0x0000_0000, 0x8000_0000): DDR_LO (2 GB)
        // mark as reserved until SDRAM initialization is complete
        for i in 0..1024 {
            self.direct_mapped_block(i, None);
        }

        // [0x8000_0000, 0xC000_0000): AXI masters & (optional) VCU slave interface (1 GB)
        for i in 1024..1536 {
            self.direct_mapped_block(
                i,
                Some(BlockAttrs {
                    xn: true,
                    contiguous: true,
                    access_flag: true,
                    shareability: Shareability::NonShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 1, // device nGnRnE
                }),
            );
        }

        // [0xC000_0000, 0xE000_0000): QSPI (512 MB)
        // [0xE000_0000, 0xF000_000): PCIE_LO (256 MB)
        for i in 1536..1920 {
            self.direct_mapped_block(
                i,
                Some(BlockAttrs {
                    xn: true,
                    contiguous: true,
                    access_flag: true,
                    shareability: Shareability::NonShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 1, // device nGnRnE
                }),
            );
        }

        // [0xF000_000, 0xF800_0000): reserved (128 MB)
        for i in 1920..1984 {
            self.direct_mapped_block(i, None);
        }

        // [0xF800_000, 0xF900_0000): coresight (16 MB)
        for i in 1984..1992 {
            self.direct_mapped_block(
                i,
                Some(BlockAttrs {
                    xn: true,
                    contiguous: false,
                    access_flag: true,
                    shareability: Shareability::NonShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 1, // device nGnRnE
                }),
            );
        }

        // TODO: RPU LL port is actually 1 MB. Xilinx FSBL also gives it a 2 MB block (taking 1 MB from
        //   the following 63 MB reserved space) for convenience. Could make an L3 table instead.
        //   Wouldn't hurt TLB if contiguous bit is utilized (unknown).
        // [0xF900_000, 0xF920_0000): RPU LL port (2 MB)
        self.direct_mapped_block(
            1992,
            Some(BlockAttrs {
                xn: true,
                contiguous: false,
                access_flag: true,
                shareability: Shareability::NonShareable,
                read_only: false,
                ns: false,
                attr_indx: 1, // device nGnRnE
            }),
        );

        // [0xF920_000, 0xFD00_0000): reserved (62 MB)
        for i in 1993..2024 {
            self.direct_mapped_block(i, None);
        }

        // [0xFD00_000, 0xFE00_0000): FPD slaves (16 MB)
        // [0xFE00_000, 0xFFC0_0000): LPD slaves (28 MB)
        // [0xFFC0_000, 0xFFE0_0000): CSU/PMU (2 MB)
        for i in 2024..2047 {
            self.direct_mapped_block(
                i,
                Some(BlockAttrs {
                    xn: true,
                    contiguous: false,
                    access_flag: true,
                    shareability: Shareability::NonShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 1, // device nGnRnE
                }),
            );
        }

        // [0xFFE0_0000, 0x1_0000_0000): TCM/OCM
        self.direct_mapped_block(
            2047,
            Some(BlockAttrs {
                xn: false,
                contiguous: false,
                access_flag: true,
                shareability: Shareability::InnerShareable,
                read_only: false,
                ns: false,
                attr_indx: 0, // normal WB/WA/RA cacheable
            }),
        );

        self
    }

    #[inline(always)]
    fn direct_mapped_block(&mut self, index: usize, block_attrs: Option<BlockAttrs>) {
        assert!(index < MAX_ENTRIES * NUM_L2_TABLES);
        let base = (index as u64) << L2_IDX_OFFSET;
        self.table[index] = TableEntry::from_attrs(base, Level::L2, EntryAttrs::Block(block_attrs));
    }
}

// 36 bit address -> 64 GB address space / 1 GB blocks (1 << (PA_NBITS - L1_IDX_OFFSET))
// proper configuration of T0SZ will ensure a translation fault is generated for OOB addresses, so
// there's no need to fill remaining entries with invalid descriptors. ARMv8 TRM page D8-5091:
// > For an address translation stage, when an attempt is made to translate an address larger than the configured IA size,
// > a level 0 Translation fault at that translation stage is generated.
const L1_ENTRIES: usize = 64;

// 1 << GRANULE_NBITS
#[repr(C, align(4096))]
pub struct L1Table {
    table: [TableEntry; L1_ENTRIES],
}

static mut L1_TABLE: L1Table = L1Table {
    table: [TableEntry(0); L1_ENTRIES],
};

impl L1Table {
    pub fn get() -> &'static mut Self {
        unsafe { &mut L1_TABLE }
    }

    pub fn setup(&mut self) -> &Self {
        // set up l2 tables
        let l2_table = L2Table::get().setup();
        // [0x0000_0000, 0x1_0000_0000): L2 tables (4 GB)
        for i in 0..NUM_L2_TABLES {
            self.next_level_table(
                i,
                &(l2_table.table[i * MAX_ENTRIES]) as *const _ as u64,
                TableAttrs {
                    ns_table: false,
                    ap_table: APTable::NoEffect,
                    xn_table: false,
                },
            );
        }

        // [0x1_0000_0000, 0x4_0000_0000): reserved (12 GB)
        for i in 4..16 {
            self.direct_mapped_block(i, None)
        }

        // 8 GB AXI masters, 8 GB PCIE_HI
        for i in 16..32 {
            self.direct_mapped_block(
                i,
                Some(BlockAttrs {
                    xn: true,
                    contiguous: false,
                    access_flag: true,
                    shareability: Shareability::NonShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 1, // device nGnRnE
                }),
            );
        }

        // 32 GB DDR_HI
        // mark as reserved until SDRAM initialization is complete
        for i in 32..64 {
            self.direct_mapped_block(i, None);
        }

        self
    }

    #[inline(always)]
    fn direct_mapped_block(&mut self, index: usize, block_attrs: Option<BlockAttrs>) {
        assert!(index < L1_ENTRIES);
        let base = (index as u64) << L1_IDX_OFFSET;
        self.table[index] = TableEntry::from_attrs(base, Level::L1, EntryAttrs::Block(block_attrs));
    }

    #[inline(always)]
    fn next_level_table(&mut self, index: usize, phys_base: u64, table_attrs: TableAttrs) {
        assert!(index < L1_ENTRIES);
        self.table[index] =
            TableEntry::from_attrs(phys_base, Level::L1, EntryAttrs::Table(table_attrs));
    }
}

pub const L1_BLOCK_SIZE: usize = 1 << L1_IDX_OFFSET;
pub const L2_BLOCK_SIZE: usize = 1 << L2_IDX_OFFSET;

pub fn update_block<T, F>(ptr: *const T, f: F)
where
    F: FnOnce(&Option<BlockAttrs>) -> Option<BlockAttrs>,
{
    // find block entry and retrieve its attrs
    let l1_idx = (ptr as usize) >> L1_IDX_OFFSET;
    let entry: &mut TableEntry;
    if l1_idx < NUM_L2_TABLES {
        assert_eq!((ptr as u64) & !L2_BLOCK_ADDR_MASK, 0);
        // a more generic solution would be to read the address pointed to in the l1 entry and
        // index from there, but for this configuration that would just add unsafe pointer
        // arithmetic for no reason
        let l2_base = l1_idx * MAX_ENTRIES;
        let l2_idx = l2_base
            + (((ptr as usize) & ((L2_BLOCK_ADDR_MASK & !L1_BLOCK_ADDR_MASK) as usize))
                >> L2_IDX_OFFSET);
        entry = &mut L2Table::get().table[l2_idx];
    } else {
        assert_eq!((ptr as u64) & !L1_BLOCK_ADDR_MASK, 0);
        entry = &mut L1Table::get().table[l1_idx];
    }
    let maybe_attrs = match entry.get_attrs() {
        EntryAttrs::Table(_) => panic!("Attempted to modify a next-level table descriptor"),
        EntryAttrs::Block(maybe_block_attrs) => maybe_block_attrs,
    };
    let new_maybe_attrs = f(&maybe_attrs);

    // ARMv8 TRM Section D8.13.1: "Using break-before-make when updating translation table entries"
    // Some -> Some: potential change in attrs requiring "break" and "make"
    // Some -> None: same as just "break"
    // None -> Some: just "make"
    // None -> None: do nothing
    if maybe_attrs.is_some() {
        // break
        entry.set_attrs(EntryAttrs::Block(None));
        dsb_is();
        // FIXME: not sure why it doesn't work, don't care enough to investigate
        // tlbi_vae3is(ptr as usize);
        tlbi_alle3is();
        dsb_is();
    }
    if new_maybe_attrs.is_some() {
        // make
        entry.set_attrs(EntryAttrs::Block(new_maybe_attrs));
        dsb_is();
        isb();
    }
}

pub fn lookup_level<T>(ptr: *const T) -> Level {
    let l1_idx = (ptr as usize) >> L1_IDX_OFFSET;
    if l1_idx < NUM_L2_TABLES {
        Level::L2
    } else {
        Level::L1
    }
}

pub fn enable_mmu(l1_table: &L1Table) {
    // set SMP bit (required for MMU even in single-core clusters)
    CPUECTLR_EL1.modify(|_, w| w.smp_en(true));
    isb();

    tlbi_alle3();
    dsb_is();
    ic_iallu();
    dsb_is();
    dcc_all();
    dsb_is();
    isb();

    // configure translation control registers
    let baddr = &l1_table.table as *const _ as u64;
    assert!(baddr & 0xfff == 0);
    TTBR0_EL3.write(TTBR0_EL3::zeroed().baddr(baddr));

    // attrs referred to by `attr_indx` field of block/page entries
    // note: cortex a53 doesn't support write through
    MAIR_EL3.write(
        MAIR_EL3::zeroed()
            .attr2(0x44) // normal non-cacheable
            .attr1(0x00) // device nGnRnE
            .attr0(0xff), // normal WB/WA/RA cacheable
    );

    TCR_EL3.write(
        TCR_EL3::zeroed_res1()
            .ps(PA_SIZE)
            .tg0(GRANULE_SIZE)
            .sh0(Shareability::InnerShareable)
            .orgn0(Cacheability::WBWACacheable)
            .irgn0(Cacheability::WBWACacheable)
            .t0sz(64 - PA_NBITS as u8),
    );
    isb();

    DAIF.modify(|_, w| w.serror_mask(false));

    // enable MMU and caches
    SCTLR_EL3.write(SCTLR_EL3::zeroed().i(true).sa(true).c(true).m(true));

    dsb_is();
    isb();
}

pub fn translate_address(virtual_address: u64) -> par_el1::Read {
    at_s1e3r(virtual_address);
    isb();
    let pa = PAR_EL1.read();
    pa
}
