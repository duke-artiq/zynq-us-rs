use libboard_zynqmp::{println, stdio};
use libcortex_a53::{
    interrupt_handler,
    regs::{ESR_EL3, MPIDR_EL1},
};
use libregister::RegisterR;

interrupt_handler!(
    _synchronous_handler,
    synchronous_handler,
    __irq_stack0_start,
    __irq_stack1_start,
    __irq_stack2_start,
    __irq_stack3_start,
    {
        stdio::drop_uart();
        let cpu_id = MPIDR_EL1.read().cpu_id();
        println!("Synchronous exception on core {cpu_id}");
        let esr_el3 = ESR_EL3.read();
        println!(
            "Exception class: {:#?}, IL: {}, ISS: {:#b}",
            esr_el3.ec(),
            esr_el3.il(),
            esr_el3.iss()
        );
        loop {}
    }
);

#[cfg(feature = "dummy_irq_handler")]
interrupt_handler!(
    _irq_handler,
    irq_handler,
    __irq_stack0_start,
    __irq_stack1_start,
    __irq_stack2_start,
    __irq_stack3_start,
    {
        stdio::drop_uart();
        let cpu_id = MPIDR_EL1.read().cpu_id();
        println!("IRQ on core {cpu_id}");
        loop {}
    }
);

interrupt_handler!(
    _fiq_handler,
    fiq_handler,
    __irq_stack0_start,
    __irq_stack1_start,
    __irq_stack2_start,
    __irq_stack3_start,
    {
        stdio::drop_uart();
        let cpu_id = MPIDR_EL1.read().cpu_id();
        println!("FIQ on core {cpu_id}");
        loop {}
    }
);

interrupt_handler!(
    _system_error_handler,
    system_error_handler,
    __irq_stack0_start,
    __irq_stack1_start,
    __irq_stack2_start,
    __irq_stack3_start,
    {
        stdio::drop_uart();
        let cpu_id = MPIDR_EL1.read().cpu_id();
        println!("System error on core {cpu_id}");
        loop {}
    }
);
