use core::arch::asm;
use libboard_zynqmp::slcr::{apu, common::Unlocked, crf_apb};
use libcortex_a53::{
    cache, enable_fpu, mmu,
    regs::{CPUACTLR_EL1, MPIDR_EL1, SCR_EL3},
};
use libregister::{RegisterR, RegisterRW, RegisterW, VolatileCell};
use r0::zero_bss;

extern "C" {
    static mut __bss_start: u64;
    static mut __bss_end: u64;
    static mut _vector_table: u64;
    fn main_core0();
    fn main_cores123();
}

static mut CORE_ENABLED: [VolatileCell<bool>; 3] = [
    VolatileCell::new(false),
    VolatileCell::new(false),
    VolatileCell::new(false),
];

#[link_section = ".text.boot"]
#[no_mangle]
#[naked]
pub unsafe extern "C" fn _boot_cores() {
    asm!(
        // zero-out GP registers
        "mov x0, xzr",
        "mov x1, xzr",
        "mov x2, xzr",
        "mov x3, xzr",
        "mov x4, xzr",
        "mov x5, xzr",
        "mov x6, xzr",
        "mov x7, xzr",
        "mov x8, xzr",
        "mov x9, xzr",
        "mov x10, xzr",
        "mov x11, xzr",
        "mov x12, xzr",
        "mov x13, xzr",
        "mov x14, xzr",
        "mov x15, xzr",
        "mov x16, xzr",
        "mov x17, xzr",
        "mov x18, xzr",
        "mov x19, xzr",
        "mov x20, xzr",
        "mov x21, xzr",
        "mov x22, xzr",
        "mov x23, xzr",
        "mov x24, xzr",
        "mov x25, xzr",
        "mov x26, xzr",
        "mov x27, xzr",
        "mov x28, xzr",
        "mov x29, xzr",
        "mov x30, xzr",
        // TODO: move to boot_core functions
        // set vector base address
        "ldr x0, =_vector_table",
        "msr VBAR_EL3, x0",
        // get CPU ID within cluster (0-3)
        "mrs x1, MPIDR_EL1",
        "tbnz x1, #1, 1f",
        "tbnz x1, #0, 0f",
        // core 0
        "ldr x2, =__stack0_start",
        "mov sp, x2",
        "bl boot_cores",
        "0:",
        // core 1
        "ldr x2, =__stack1_start",
        "mov sp, x2",
        "bl boot_cores",
        "1:",
        "tbnz x0, #0, 2f",
        // core 2
        "ldr x2, =__stack2_start",
        "mov sp, x2",
        "bl boot_cores",
        "2:",
        // core 3
        "ldr x2, =__stack3_start",
        "mov sp, x2",
        "bl boot_cores",
        options(noreturn)
    );
}

#[no_mangle]
#[inline(never)]
unsafe fn boot_cores() -> ! {
    match MPIDR_EL1.read().cpu_id() {
        0 => {
            boot_core0();
        }
        1 => {
            while !CORE_ENABLED[0].get() {}
            boot_cores123();
        }
        2 => {
            while !CORE_ENABLED[1].get() {}
            boot_cores123();
        }
        3 => {
            while !CORE_ENABLED[2].get() {}
            boot_cores123();
        }
        _ => unreachable!(),
    }
}

fn apu_init() {
    // invalidate TLBs, I- and D-caches
    cache::tlbi_alle3();
    cache::ic_iallu();
    cache::dci_all_l1();

    // route external aborts/interrupts to EL3
    SCR_EL3.write(SCR_EL3::zeroed().ea(true).fiq(true).irq(true));
    // disable prefetch
    // don't need to zero -- reset values of all fields are defined
    CPUACTLR_EL1.modify(|_, w| w.n_pf_strm(0).l1_pctl(0));
}

#[inline(never)]
unsafe fn boot_core0() -> ! {
    apu_init();
    enable_fpu();
    zero_bss(&mut __bss_start, &mut __bss_end);
    let l1_table = mmu::L1Table::get().setup();
    mmu::enable_mmu(l1_table);
    // enable prefetch (2 streams, 5 outstanding prefetches allowed)
    CPUACTLR_EL1.modify(|_, w| w.n_pf_strm(0b01).l1_pctl(0b101));
    main_core0();
    panic!("return from main")
}

#[inline(never)]
unsafe fn boot_cores123() -> ! {
    apu_init();
    enable_fpu();
    let l1_table = mmu::L1Table::get();
    // FIXME: still broken for cores 2 (sometimes) & 3
    mmu::enable_mmu(l1_table);
    // enable prefetch (2 streams, 5 outstanding prefetches allowed)
    CPUACTLR_EL1.modify(|_, w| w.n_pf_strm(0b01).l1_pctl(0b101));
    main_cores123();
    panic!("return from main");
}

pub struct Core1 {}

impl Core1 {
    pub fn start(rvbar: u64) -> Self {
        // put core in reset
        crf_apb::RegisterBlock::unlocked(|crf_apb| {
            crf_apb
                .rst_fpd_apu
                .modify(|_, w| w.apu1_por(true).apu1_reset(true));
        });

        // write reset vector address
        let rvbar_l = (rvbar & 0xffff_ffff) as u32;
        let rvbar_h = (rvbar >> 32) as u8;
        assert!(rvbar_l & 0b11 == 0);
        apu::RegisterBlock::apu()
            .rvbar_addr1_l
            .write(apu::RVBarAddrL::zeroed().addr(rvbar_l >> 2));
        apu::RegisterBlock::apu()
            .rvbar_addr1_h
            .write(apu::RVBarAddrH::zeroed().addr(rvbar_h));

        unsafe {
            CORE_ENABLED[0].set(true);
            cache::dcc_poc(&CORE_ENABLED[0]);
        }

        // bring core out of reset
        crf_apb::RegisterBlock::unlocked(|crf_apb| {
            crf_apb
                .rst_fpd_apu
                .modify(|_, w| w.apu1_por(false).apu1_reset(false));
        });

        Core1 {}
    }
}
