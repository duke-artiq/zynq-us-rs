use libboard_zynqmp::{print, println, stdio};
use libcortex_a53::regs::MPIDR_EL1;
use libregister::RegisterR;

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    stdio::drop_uart();
    let cpu_id = MPIDR_EL1.read().cpu_id();
    print!("\nPanic on core {} at ", cpu_id);
    if let Some(location) = info.location() {
        print!(
            "{}:{}:{}",
            location.file(),
            location.line(),
            location.column()
        );
    } else {
        print!("unknown location");
    }
    if let Some(message) = info.message() {
        println!(": {}", message);
    } else {
        println!("");
    }
    loop {}
}
