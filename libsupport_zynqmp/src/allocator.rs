//! Memory allocator for programs running on the Cortex-A53
// Technically not copied, but very heavily based on libsupport_zynq/src/ram.rs
use core::{
    alloc::{GlobalAlloc, Layout},
    ptr::NonNull,
};
use libcortex_a53::{mutex::Mutex, regs::MPIDR_EL1};
use libregister::RegisterR;
use linked_list_allocator::Heap;
use log::debug;

extern "C" {
    static __heap0_start: usize;
    static __heap0_end: usize;
}

#[cfg(feature = "alloc_core")]
extern "C" {
    static __heap1_start: usize;
    static __heap1_end: usize;
    static __heap2_start: usize;
    static __heap2_end: usize;
    static __heap3_start: usize;
    static __heap3_end: usize;
}

struct Allocator([Mutex<Heap>; 4]);

#[global_allocator]
static ALLOCATOR: Allocator = Allocator([
    Mutex::new(Heap::empty()),
    Mutex::new(Heap::empty()),
    Mutex::new(Heap::empty()),
    Mutex::new(Heap::empty()),
]);

unsafe impl Sync for Allocator {}

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        debug!("Allocating {} bytes", layout.size());
        if cfg!(not(feature = "alloc_core")) {
            &self.0[0]
        } else {
            &self.0[MPIDR_EL1.read().cpu_id() as usize]
        }
        .lock()
        .allocate_first_fit(layout)
        .ok()
        .map_or(0 as *mut u8, |allocation| allocation.as_ptr())
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        debug!("Deallocating {} bytes", layout.size());
        if cfg!(not(feature = "alloc_core")) {
            &self.0[0]
        } else {
            &self.0[MPIDR_EL1.read().cpu_id() as usize]
        }
        .lock()
        .deallocate(NonNull::new_unchecked(ptr), layout)
    }
}

pub fn init_alloc_core0() {
    unsafe {
        let start = &__heap0_start as *const usize as usize;
        let end = &__heap0_end as *const usize as usize;
        ALLOCATOR.0[0].lock().init(start as *mut u8, end - start);
    }
}

#[cfg(feature = "alloc_core")]
pub fn init_alloc_core1() {
    unsafe {
        let start = &__heap1_start as *const usize as usize;
        let end = &__heap1_end as *const usize as usize;
        ALLOCATOR.0[1].lock().init(start as *mut u8, end - start);
    }
}

#[cfg(feature = "alloc_core")]
pub fn init_alloc_core2() {
    unsafe {
        let start = &__heap2_start as *const usize as usize;
        let end = &__heap2_end as *const usize as usize;
        ALLOCATOR.0[2].lock().init(start as *mut u8, end - start);
    }
}

#[cfg(feature = "alloc_core")]
pub fn init_alloc_core3() {
    unsafe {
        let start = &__heap3_start as *const usize as usize;
        let end = &__heap3_end as *const usize as usize;
        ALLOCATOR.0[3].lock().init(start as *mut u8, end - start);
    }
}

#[alloc_error_handler]
fn alloc_error(layout: Layout) -> ! {
    let allocator = if cfg!(not(feature = "alloc_core")) {
        &ALLOCATOR.0[0]
    } else {
        &ALLOCATOR.0[MPIDR_EL1.read().cpu_id() as usize]
    }
    .lock();
    panic!(
        "Allocation error, layout: {:?}, used memory: {} bytes, free memory: {} bytes",
        layout,
        allocator.used(),
        allocator.free()
    );
}
