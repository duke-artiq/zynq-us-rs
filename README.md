# zynqmp-rs
# Bare-metal Rust on ZynqMP

Supported boards:
- ZCU111 Evaluation Board

## Building

The development environment is managed using [Nix](https://nixos.org) flakes. To enable flakes, ensure you have Nix 2.4 or newer installed and add the line ``experimental-features = nix-command flakes`` to your ``nix.conf``.

To enter the development shell, run:

```bash
$ nix develop
```

Then to build, simply run:

```bash
$ cargo build -p tests --release
```

For convenience I've written a small script (`build.sh`) that runs the above command and copies the resulting binary to the `openocd` directory.

## Debugging

First you will need to set up your board in JTAG boot mode (see [UG1271](https://docs.xilinx.com/r/en-US/ug1271-zcu111-eval-bd/RFSoC-Device-Configuration)) and connect a micro-USB cable.

I generally use `gdb`, for which I've written a script (`gdb.sh`) that assumes the binary is in the `openocd` directory:

```bash
$ ./gdb.sh
<output>
(gdb) load
<output>
(gdb) c[ontinue]
```

Alternatively, you can use OpenOCD directly:

```bash
$ cd openocd
$ openocd -f zcu111.cfg -c 'load_image tests.elf; resume 0xfffc0000; exit'
```

UART output is available on port B of the FTDI interface (e.g. `/dev/ttyUSB1`). Note that only `\n` is used for line termination, so you may need to configure your serial terminal to add a carriage return to every newline character.
