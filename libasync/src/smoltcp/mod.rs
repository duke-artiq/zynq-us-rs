//! Copied from https://git.m-labs.hk/M-labs/zynq-rs
//! Commit: be672ab
//! modifications:
//! - smoltcp -> 0.10
use core::{
    cell::RefCell,
    task::Waker,
};
use alloc::vec::Vec;
use smoltcp::{
    iface::{Interface, SocketSet},
    phy::Device,
    time::{Duration, Instant},
};
use crate::task;

mod tcp_stream;
pub use tcp_stream::TcpStream;

pub trait LinkCheck {
    type Link;
    fn is_idle(&self) -> bool;
    fn check_link_change(&mut self) -> Option<Self::Link>;
}

static mut SOCKETS: Option<Sockets> = None;

pub struct Sockets {
    sockets: RefCell<SocketSet<'static>>,
    wakers: RefCell<Vec<Waker>>,
}

impl Sockets {
    pub fn init(max_sockets: usize) {
        let mut sockets_storage = Vec::with_capacity(max_sockets);
        for _ in 0..max_sockets {
            sockets_storage.push(Default::default());
        }
        let sockets = RefCell::new(SocketSet::new(sockets_storage));

        let wakers = RefCell::new(Vec::new());

        let instance = Sockets {
            sockets,
            wakers,
        };
        unsafe { SOCKETS = Some(instance); }
    }

    /// Block and run executor indefinitely while polling the smoltcp
    /// iface
    pub fn run<D: Device + LinkCheck>(
        iface: &mut Interface,
        device: &mut D,
        mut get_time: impl FnMut() -> Instant,
    ) -> ! {
        task::block_on(async {
            let mut last_link_check = Instant::from_millis(0);
            const LINK_CHECK_INTERVAL: u64 = 500;

            loop {
                let timestamp = get_time();
                Self::instance().poll(timestamp, device, iface);

                if device.is_idle() && timestamp >= last_link_check + Duration::from_millis(LINK_CHECK_INTERVAL) {
                    device.check_link_change();
                    last_link_check = timestamp;
                }

                task::r#yield().await;
            }
        })
    }

    pub(crate) fn instance() -> &'static Self {
        unsafe { SOCKETS.as_ref().expect("Sockets") }
    }

    fn poll<D: Device>(
        &self,
        timestamp: Instant,
        device: &mut D,
        iface: &mut Interface,
    ) {
        let processed = {
            let mut sockets = self.sockets.borrow_mut();
            iface.poll(timestamp, device, &mut sockets)
        };
        if processed {
            let mut wakers = self.wakers.borrow_mut();
            for waker in wakers.drain(..) {
                waker.wake();
            }
        }
    }

    /// TODO: this was called through eg. TcpStream, another poll()
    /// might want to send packets before sleeping for an interrupt.
    pub(crate) fn register_waker(waker: Waker) {
        let mut wakers = Self::instance().wakers.borrow_mut();
        for (i, w) in wakers.iter().enumerate() {
            if w.will_wake(&waker) {
                let last = wakers.len() - 1;
                wakers.swap(i, last);
                return;
            }
        }
        wakers.push(waker);
    }
}
