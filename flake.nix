{
  description = "Bare-metal rust on Zynq UltraScale+";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-23.05;
    zynq-rs.url = git+https://git.m-labs.hk/M-Labs/zynq-rs;
    mozilla-overlay.url = github:mozilla/nixpkgs-mozilla;
    rustManifest = {
      url = https://static.rust-lang.org/dist/2023-10-15/channel-rust-nightly.toml;
      flake = false;
    };
  };

  outputs = { self, nixpkgs, zynq-rs, mozilla-overlay, rustManifest }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; overlays = [ mozilla-overlay.overlays.rust ]; };

      rust = (pkgs.lib.rustLib.fromManifestFile rustManifest {
        inherit (pkgs) stdenv lib fetchurl patchelf;
      }).rust.override {
        extensions = [ "rust-src" ];
        targets = [ "aarch64-unknown-none" ];
      };

      rustPlatform = pkgs.makeRustPlatform {
        rustc = rust;
        cargo = rust;
      };

      mkbootimage = zynq-rs.packages.x86_64-linux.mkbootimage;
    in
    {
      devShells.x86_64-linux.default = pkgs.mkShell {
        name = "zynqmp-rs-dev-shell";
        buildInputs = (with pkgs; [
          openocd
          gdb
        ]) ++ [
          rust
          mkbootimage
        ];
      };

      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
    };
}
