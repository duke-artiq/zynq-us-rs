//! I2C Driver and Registers
use core::slice::Iter;

use libregister::{RegisterR, RegisterRW, RegisterW};
use log::{debug, error};

use super::clocks::Clocks;
use super::slcr::{common::Unlocked, crl_apb};
use super::util::div_round_closest;

pub mod regs;

// Hardware FIFO size in bytes
// u16 to avoid excessive casting
const FIFO_SIZE: u16 = 16;
// Threshold at which the `data` interrupt is triggered(?)
#[allow(unused)]
const DATA_INTR_THRESH: u8 = 14;
// Maximum size of master transfers in bytes
// UG1085 says this is 255 but the FSBL says otherwise
const MAX_TX: u8 = 252;
// Mux address (same for both)
#[cfg(feature = "target_zcu111")]
pub const MUX_ADDR: u16 = 0x75;
// Max SCL freq (DS926 Table 47)
const MAX_SCLK_FREQ: u32 = 400_000;
// Max divisors (note: actual divisor is register value + 1)
const MAX_DIV_A: u32 = 4;
const MAX_DIV_B: u32 = 64;
// Magic number tucked away in a note in UG1085 (pg. 631)
const CLK_DIV_BUILTIN: u32 = 22;

pub struct I2C {
    regs: &'static mut regs::RegisterBlock,
    ref_clk: u32,
}

/// Return type for I2C `Err` variants.
///
/// * `BusBusy`: A new transaction tried to start while the I2C bus was still busy
/// * `Interrupt(u32)`: An interrupt flag was set during a (polling) transaction.
///   Contains the last read value of [crate::i2c::regs::interrupt_status].
#[derive(Debug)]
pub enum I2CErr {
    BusBusy,
    Interrupt(u32),
}

impl I2C {
    /// Constructor for I2C0 controller.
    pub fn i2c0() -> Self {
        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.peri_rst_ctrl.modify(|_, w| w.i2c0_rst(true));
            crl_apb.peri_rst_ctrl.modify(|_, w| w.i2c0_rst(false));
        });
        let mut self_ = I2C {
            regs: regs::RegisterBlock::i2c0(),
            ref_clk: Clocks::get().i2c0_ref_clk(),
        };
        self_.reset(false);
        self_
    }

    /// Constructor for I2C1 controller.
    pub fn i2c1() -> Self {
        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.peri_rst_ctrl.modify(|_, w| w.i2c1_rst(true));
            crl_apb.peri_rst_ctrl.modify(|_, w| w.i2c1_rst(false));
        });
        let mut self_ = I2C {
            regs: regs::RegisterBlock::i2c1(),
            ref_clk: Clocks::get().i2c1_ref_clk(),
        };
        self_.reset(false);
        // glitch filter set to number of clock cycles that equals 50 ns (1 / 20 MHz)
        let num_cycles = div_round_closest(self_.ref_clk, 20_000_000);
        // 4 bit field
        assert!(
            num_cycles < 16,
            "Reference clock is too fast to configure glitch filter"
        );
        self_
            .regs
            .glitch_filter
            .write(regs::GlitchFilter::zeroed().glitch_filter(num_cycles as u8)); // 1 / 50 ns
        self_
    }

    /// Reset the I2C controller.
    ///
    /// UG1085 Table 22-3: I2C Reset
    ///
    /// * `restore_interrupts`: `bool` - whether to restore previously-enabled interrupts after reset
    pub fn reset(&mut self, _restore_interrupts: bool) {
        // disable and clear interrupts
        self.disable_interrupts();
        self.clear_interrupt_status();

        // clear FIFO
        self.clear_fifo();

        // set timeout to max value
        self.regs
            .timeout
            .write(regs::Timeout::zeroed().timeout(0xff));

        // clear other registers
        self.regs.tx_size.write(regs::TxSize::zeroed());
        self.regs.control.write(regs::Control::zeroed());
    }

    pub fn set_sclk(&mut self, mut freq: u32) {
        assert!(
            freq <= MAX_SCLK_FREQ,
            "SCL frequency cannot exceed {} Hz",
            MAX_SCLK_FREQ
        );
        // from the FSBL (and I dare not tempt fate):
        // > If frequency 400KHz is selected, 384.6KHz should be set.
        // > If frequency 100KHz is selected, 90KHz should be set.
        // > This is due to a hardware limitation.
        if freq > 384_600 {
            freq = 384_600;
        } else if freq > 90_000 && freq <= 100_000 {
            freq = 90_000;
        }

        let target_div = div_round_closest(self.ref_clk, freq * CLK_DIV_BUILTIN);
        assert!(
            target_div <= (MAX_DIV_A * MAX_DIV_B),
            "Target frequency not achievable with current reference clock"
        );

        // the above assertion guarantees we can reach <= the target frequency (and > 0 unless the
        // ref clock is 0, which shouldn't be possible), so max error should be guaranteed < freq
        let mut best_error_hz = freq;
        let mut best_div_a: u32 = 0;
        let mut best_div_b: u32 = 0;

        for div_a in 1..=MAX_DIV_A {
            let div_b = div_round_closest(target_div, div_a).min(MAX_DIV_B);
            let error_hz =
                div_round_closest(self.ref_clk, div_a * div_b * CLK_DIV_BUILTIN).abs_diff(freq);
            if error_hz < best_error_hz {
                best_div_a = div_a;
                best_div_b = div_b;
                best_error_hz = error_hz;
            }
        }

        // shouldn't happen
        debug_assert_ne!(best_div_a, 0);
        debug_assert_ne!(best_div_b, 0);

        debug!("Divisors: {best_div_a}, {best_div_b}, error: {best_error_hz} Hz for target frequency {freq} Hz");

        self.regs.control.modify(|_, w| {
            w.divisor_a((best_div_a - 1) as u8)
                .divisor_b((best_div_b - 1) as u8)
        });
    }

    /// Perform common setup steps for master mode.
    ///
    /// Sets the interface to master mode, clears the FIFO & interrupt status, sets RX/TX according
    /// to the `rx_en` argument, ACK enable, normal addressing mode, and disables all interrupts.
    ///
    /// Returns `Err(I2CErr::BusBusy)` if bus is busy and hold bit is not set, `Ok(())` otherwise.
    pub fn master_setup(&mut self, rx_en: bool) -> Result<(), I2CErr> {
        if !self.regs.control.read().hold() && self.busy() {
            return Err(I2CErr::BusBusy);
        }
        self.regs.control.modify(|_, w| {
            w.clear_fifo(true)
                .ack_en(true)
                .addr_mode(true)
                .interface_mode(true)
                .rx_en(rx_en)
        });
        self.disable_interrupts();
        Ok(())
    }

    /// Fill available space in Tx FIFO.
    ///
    /// Returns `true` if `data_iter` was exhausted while filling, `false` otherwise.
    fn tx_fifo_fill(&mut self, data_iter: &mut Iter<u8>) -> bool {
        let mut fifo_space = FIFO_SIZE as u8 - self.regs.tx_size.read().tx_size();
        while fifo_space > 0 {
            if let Some(&data) = data_iter.next() {
                self.regs.data.write(regs::Data::zeroed().data(data));
                fifo_space -= 1;
            } else {
                return true;
            }
        }
        false
    }

    /// Perform a polled write in master mode.
    ///
    /// UG1085 Chapter 22 - Programming Model and Table 22-12
    ///
    /// * `addr`: `u16` - address to read from (7 or 10 bits depending on addressing mode)
    /// * `size`: `u16` - transfer size in bytes
    /// * `data`: `&[u8]` - data to send
    ///
    /// Returns:
    /// * `Ok(())` if transfer was successful
    /// * `Err(I2CErr)` if the transfer failed
    pub fn master_write_polled(&mut self, addr: u16, size: u16, data: &[u8]) -> Result<(), I2CErr> {
        self.master_setup(false)?;

        self.regs.control.modify(|_, w| w.hold(size > FIFO_SIZE));

        self.clear_interrupt_status();

        // Fill FIFO before writing address
        let mut data_iter = data.into_iter();
        let mut done = self.tx_fifo_fill(&mut data_iter);

        self.regs.addr.write(regs::Addr::zeroed().addr(addr));

        let mut isr_read = self.regs.interrupt_status.read();
        while !Self::tx_error(&isr_read) && !done {
            if self.regs.status.read().tx_data_valid() {
                isr_read = self.regs.interrupt_status.read();
                continue;
            }
            done = self.tx_fifo_fill(&mut data_iter);
            isr_read = self.regs.interrupt_status.read();
        }

        self.regs.control.modify(|_, w| w.hold(false));

        // Wait for completion
        while !isr_read.tx_complete() {
            isr_read = self.regs.interrupt_status.read();
            if Self::tx_error(&isr_read) {
                error!(
                    "Failed to complete transaction. Last read value of ISR: {:#X}",
                    isr_read.inner
                );
                return Err(I2CErr::Interrupt(isr_read.inner));
            }
        }

        Ok(())
    }

    /// Perform a polled read in master mode.
    ///
    /// UG1085 Chapter 22 - Programming Model and Table 22-13
    ///
    /// * `addr`: `u16` - address to read from (7 or 10 bits depending on addressing mode)
    /// * `size`: `u16` - transfer size in bytes
    /// * `rx_buffer`: `&mut [u8]` - mutable slice to put the read data in
    ///
    /// Returns:
    /// * `Ok(())` if transfer was successful
    /// * `Err(u32)` if the transfer failed
    pub fn master_read_polled(
        &mut self,
        addr: u16,
        size: u16, // TODO: make option and default to buffer size(?)
        rx_buffer: &mut [u8],
    ) -> Result<(), I2CErr> {
        assert!(
            size as usize <= rx_buffer.len(),
            "Buffer (len {} not big enough for transfer size ({})",
            rx_buffer.len(),
            size
        );

        // total (not per-chunk) bytes remaining
        let mut remaining_bytes = size;
        let (mut cur_chunk_size, mut final_chunk) = if size > MAX_TX as u16 {
            (MAX_TX, false)
        } else {
            (size as u8, true)
        };

        self.master_setup(true)?;

        let mut hold = size > FIFO_SIZE;
        self.regs.control.modify(|_, w| w.hold(hold));

        self.clear_interrupt_status();

        self.regs.addr.write(regs::Addr::zeroed().addr(addr));
        self.regs
            .tx_size
            .write(regs::TxSize::zeroed().tx_size(cur_chunk_size));

        let mut isr_read = self.regs.interrupt_status.read();
        while remaining_bytes > 0 && !Self::rx_error(&isr_read) {
            while self.regs.status.read().rx_data_valid() {
                if hold && final_chunk && remaining_bytes < DATA_INTR_THRESH as u16 {
                    hold = false;
                    self.regs.control.modify(|_, w| w.hold(false));
                }
                rx_buffer[(size - remaining_bytes) as usize] = self.regs.data.read().data();
                remaining_bytes -= 1;
                cur_chunk_size -= 1;
            }
            if cur_chunk_size == 0 && remaining_bytes > 0 {
                (cur_chunk_size, final_chunk) = if remaining_bytes > MAX_TX as u16 {
                    (MAX_TX, false)
                } else {
                    (remaining_bytes as u8, true)
                };
                self.clear_interrupt_status();
                self.regs.addr.write(regs::Addr::zeroed().addr(addr));
                self.regs
                    .tx_size
                    .write(regs::TxSize::zeroed().tx_size(cur_chunk_size));
            }
            isr_read = self.regs.interrupt_status.read();
        }

        self.regs.control.modify(|_, w| w.hold(false));
        // Important: if the tx_size register isn't explicitly zeroed out, the value can be carried
        // over to the next time the master is setup in receive mode. The following behavior was
        // observed:
        //
        // 1. Receive one byte (setting tx_size to 1)
        // 2. Send one byte
        // 3. When receiving another byte:
        //   - prior to master_setup (setting regs.control.rx_en = 1), read tx_size register,
        //     value is confirmed to be zero
        //   - after master_setup, which includes clearing the FIFO (and according to UG1087, the
        //     tx_size register as well, which was already zero in the first place), tx_size register
        //     now reads a value of ONE
        //   - tried moving the clear FIFO operation to a separate call after setting to receive
        //     mode in case of a race condition, made no difference
        //
        // This carry over between transactions can result in uncaught data corruption. The
        // specific case I observed was a CRC mismatch when validating the DDR SPD EEPROM data.
        self.regs.tx_size.write(regs::TxSize::zeroed());

        if Self::rx_error(&isr_read) {
            error!(
                "Failed to complete transaction. Last read value of ISR: {:#X}",
                isr_read.inner
            );
            return Err(I2CErr::Interrupt(isr_read.inner));
        }
        Ok(())
    }

    /// Clear the FIFO and TX size register.
    pub fn clear_fifo(&mut self) {
        // self-clearing bit
        self.regs.control.modify(|_, w| w.clear_fifo(true));
    }

    /// Set ACK enable.
    pub fn set_ack_en(&mut self, enable: bool) {
        self.regs.control.modify(|_, w| w.ack_en(enable));
    }

    /// (Master mode) Set addressing mode.
    ///
    /// * `mode`: `bool` - `true` for 7-bit (normal), `false` for 10-bit (extended)
    pub fn set_addr_mode(&mut self, mode: bool) {
        self.regs.control.modify(|_, w| w.addr_mode(mode));
    }

    /// Set interface mode.
    ///
    /// * `mode`: `bool` - `true` for master, `false` for slave mode
    pub fn set_interface_mode(&mut self, mode: bool) {
        self.regs.control.modify(|_, w| w.interface_mode(mode));
    }

    /// (Master mode) RX/TX selection.
    ///
    /// * `enable`: `bool` - `true` for RX, `false` for TX
    pub fn set_rx_en(&mut self, enable: bool) {
        self.regs.control.modify(|_, w| w.rx_en(enable));
    }

    /// Clear flags in ISR.
    pub fn clear_interrupt_status(&mut self) {
        self.regs.interrupt_status.modify(|r, _| {
            // clear currently set flags
            regs::interrupt_status::Write { inner: r.inner }
        });
    }

    /// Enable all interrupts
    pub fn enable_interrupts(&mut self) {
        self.regs.interrupt_enable.write(
            regs::InterruptEnable::zeroed()
                .arb_lost(true)
                .rx_underflow(true)
                .tx_overflow(true)
                .rx_overflow(true)
                .slv_ready(true)
                .timeout(true)
                .nack(true)
                .data(true)
                .tx_complete(true),
        );
    }

    /// Disable all interrupts
    pub fn disable_interrupts(&mut self) {
        self.regs.interrupt_disable.write(
            regs::InterruptDisable::zeroed()
                .arb_lost(true)
                .rx_underflow(true)
                .tx_overflow(true)
                .rx_overflow(true)
                .slv_ready(true)
                .timeout(true)
                .nack(true)
                .data(true)
                .tx_complete(true),
        );
    }

    /// Returns whether any RX-related error flags are set
    #[inline]
    fn rx_error(isr_read: &regs::interrupt_status::Read) -> bool {
        isr_read.arb_lost() || isr_read.rx_underflow() || isr_read.rx_overflow() || isr_read.nack()
    }

    /// Returns whether any TX-related error flags are set
    #[inline]
    fn tx_error(isr_read: &regs::interrupt_status::Read) -> bool {
        isr_read.arb_lost() || isr_read.tx_overflow() || isr_read.nack()
    }

    /// Returns whether the I2C bus is busy.
    pub fn busy(&mut self) -> bool {
        self.regs.status.read().bus_active()
    }
}
