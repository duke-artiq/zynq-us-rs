// Copied from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: be672ab
// Modified for ZynqMP registers
// Modified to read time from A53 registers instead of system regs
use crate::{
    clocks::Clocks,
    slcr::iou_scntrs,
    time::{Microseconds, Milliseconds, TimeSource},
};
use core::ops::Add;
use libcortex_a53::regs::{CNTFRQ_EL0, CNTPCT_EL0};
use libregister::{RegisterR, RegisterW};
use void::Void;

/// "uptime"
#[derive(Clone, Copy)]
pub struct GlobalTimer;

impl GlobalTimer {
    /// Get the potentially uninitialized timer
    pub unsafe fn get() -> GlobalTimer {
        GlobalTimer {}
    }

    /// Get the timer with a reset
    pub fn start() -> GlobalTimer {
        let mut regs = iou_scntrs::RegisterBlock::iou_scntrs();
        Self::reset(&mut regs);
        GlobalTimer {}
    }

    fn reset(regs: &mut iou_scntrs::RegisterBlock) {
        // Disable
        regs.counter_control
            .write(iou_scntrs::CounterControl::zeroed());

        // Reset counters
        regs.current_counter_value_lower
            .write(iou_scntrs::CurrentCounterValueLower::zeroed());
        regs.current_counter_value_upper
            .write(iou_scntrs::CurrentCounterValueUpper::zeroed());

        let freq = Clocks::get().timestamp_ref_clk();
        regs.base_frequency_id
            .write(iou_scntrs::BaseFrequencyID::zeroed().freq(freq));
        CNTFRQ_EL0.write(CNTFRQ_EL0::zeroed().clk_frq(freq));

        // Start
        regs.counter_control
            .write(iou_scntrs::CounterControl::zeroed().en(true));
    }

    /// read the raw counter value
    pub fn get_counter(&self) -> u64 {
        // while `self` is unused, want to protect from undefined behavior by making the caller
        // obtain a reference via either a) `start`, or b) `get`, in which case it's explicitly
        // unsafe

        // TODO: barriers?
        //   ARMv8 TRM:
        //   "Reads of CNTPCT_EL0 can occur speculatively and out of order relative to other instructions executed on the same PE."
        CNTPCT_EL0.read().count()
    }

    /// read and convert to time
    pub fn get_time(&self) -> Milliseconds {
        let freq = CNTFRQ_EL0.read().clk_frq();

        Milliseconds(self.get_counter() / (freq as u64 / 1000))
    }

    /// read with high precision
    pub fn get_us(&self) -> Microseconds {
        let freq = CNTFRQ_EL0.read().clk_frq();

        Microseconds(1_000_000 * self.get_counter() / freq as u64)
    }

    /// return a handle that has implements
    /// `embedded_hal::timer::CountDown`
    pub fn countdown<U>(&self) -> CountDown<U>
    where
        Self: TimeSource<U>,
    {
        CountDown {
            timer: self.clone(),
            timeout: self.now(),
        }
    }
}

impl TimeSource<Milliseconds> for GlobalTimer {
    fn now(&self) -> Milliseconds {
        self.get_time()
    }
}

impl TimeSource<Microseconds> for GlobalTimer {
    fn now(&self) -> Microseconds {
        self.get_us()
    }
}

#[derive(Clone)]
pub struct CountDown<U> {
    timer: GlobalTimer,
    timeout: U,
}

/// embedded-hal async API
impl<U: Add<Output = U> + PartialOrd> embedded_hal::timer::CountDown for CountDown<U>
where
    GlobalTimer: TimeSource<U>,
{
    type Time = U;

    fn start<T: Into<Self::Time>>(&mut self, count: T) {
        self.timeout = self.timer.now() + count.into();
    }

    fn wait(&mut self) -> nb::Result<(), Void> {
        if self.timer.now() <= self.timeout {
            Err(nb::Error::WouldBlock)
        } else {
            Ok(())
        }
    }
}

impl<U: PartialOrd> CountDown<U>
where
    GlobalTimer: TimeSource<U>,
{
    pub fn waiting(&self) -> bool {
        self.timer.now() <= self.timeout
    }
}

/// embedded-hal sync API
impl embedded_hal::blocking::delay::DelayMs<u64> for GlobalTimer {
    fn delay_ms(&mut self, ms: u64) {
        use embedded_hal::timer::CountDown;

        let mut countdown = self.countdown::<Milliseconds>();
        countdown.start(Milliseconds(ms));
        nb::block!(countdown.wait()).unwrap();
    }
}

/// embedded-hal sync API
impl embedded_hal::blocking::delay::DelayUs<u64> for GlobalTimer {
    fn delay_us(&mut self, us: u64) {
        use embedded_hal::timer::CountDown;

        let mut countdown = self.countdown::<Microseconds>();
        countdown.start(Microseconds(us));
        nb::block!(countdown.wait()).unwrap();
    }
}
