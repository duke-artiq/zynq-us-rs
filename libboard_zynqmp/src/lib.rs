#![no_std]
#![feature(more_qualified_paths)]
#![feature(int_roundings)]

extern crate alloc;

pub use smoltcp;

pub mod axi_hp;
pub mod clocks;
pub mod ddr;
pub mod eth;
pub mod i2c;
pub mod interrupts;
pub mod logger;
pub mod slcr;
pub mod stdio;
pub mod time;
pub mod timer;
pub mod uart;
mod util;
