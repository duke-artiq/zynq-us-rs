///! GEM Registers
use libregister::{register, register_at, register_bit, register_bits, register_bits_typed};
use volatile_register::{RO, RW};

#[repr(C)]
pub struct RegisterBlock {
    pub network_control: NetworkControl,
    pub network_config: NetworkConfig,
    pub network_status: NetworkStatus, // 0x8
    unused0: u32,
    pub dma_config: DMAConfig, // 0x10
    pub transmit_status: TransmitStatus,
    pub receive_q_ptr: ReceiveQPtr,
    pub transmit_q_ptr: TransmitQPtr,
    pub receive_status: ReceiveStatus,
    pub int_status: IntStatus,
    pub int_enable: IntEnable,
    pub int_disable: IntDisable,
    pub int_mask: IntMask,
    pub phy_management: PhyManagement,
    pub pause_time: RO<u32>,
    pub tx_pause_quantum: RW<u32>,
    pub pbuf_txcutthru: RW<u32>,
    pub pbuf_rxcutthru: RW<u32>,
    pub jumbo_max_length: RW<u32>,
    pub external_fifo_interface: ExternalFifoInterface, // 0x4c
    unused1: u32,
    pub axi_max_pipeline: RW<u32>, // 0x54
    unused2: [u32; 10],
    pub hash_bottom: RW<u32>, // 0x80
    pub hash_top: RW<u32>,
    pub spec_add1_bottom: SpecAddBottom,
    pub spec_add1_top: SpecAddTop,
    pub spec_add2_bottom: SpecAddBottom,
    pub spec_add2_top: SpecAddTop,
    pub spec_add3_bottom: SpecAddBottom,
    pub spec_add3_top: SpecAddTop,
    pub spec_add4_bottom: SpecAddBottom,
    pub spec_add4_top: SpecAddTop,
    pub spec_type1: SpecType,
    pub spec_type2: SpecType,
    pub spec_type3: SpecType,
    pub spec_type4: SpecType,
    pub wol_register: RW<u32>,
    pub stretch_ratio: RW<u32>,
    pub stacked_vlan: RW<u32>,
    pub tx_pfc_pause: RW<u32>,
    pub mask_add1_bottom: MaskAddBottom,
    pub mask_add1_top: MaskAddTop,
    pub dma_addr_or_mask: RW<u32>,
    pub rx_ptp_unicast: RW<u32>,
    pub tx_ptp_unicast: RW<u32>,
    pub tsu_nsec_cmp: RW<u32>,
    pub tsu_sec_cmp: RW<u32>,
    pub tsu_msb_sec_cmp: RW<u32>,
    pub tsu_ptp_tx_msb_sec: RO<u32>,
    pub tsu_ptp_rx_msb_sec: RO<u32>,
    pub tsu_peer_tx_msb_sec: RO<u32>,
    pub tsu_peer_rx_msb_sec: RO<u32>,
    pub dpram_fill_dbg: RW<u32>,
    pub revision_reg: RO<u32>,
    pub octets_txed_bottom: RW<u32>,
    pub octets_txed_top: RW<u32>,
    pub frames_txed_ok: RW<u32>,
    pub broadcast_txed: RW<u32>,
    pub multicast_txed: RW<u32>,
    pub pause_frames_txed: RW<u32>,
    pub frames_txed_64: RW<u32>,
    pub frames_txed_65: RW<u32>,
    pub frames_txed_128: RW<u32>,
    pub frames_txed_256: RW<u32>,
    pub frames_txed_512: RW<u32>,
    pub frames_txed_1024: RW<u32>,
    pub frames_txed_1519: RW<u32>,
    pub tx_underruns: RW<u32>,
    pub single_collisions: RW<u32>,
    pub multiple_collisions: RW<u32>,
    pub excessive_collisions: RW<u32>,
    pub late_collisions: RW<u32>,
    pub deferred_frames: RW<u32>,
    pub crs_errors: RW<u32>,
    pub octets_rxed_bottom: RW<u32>,
    pub octets_rxed_top: RW<u32>,
    pub frames_rxed_ok: RW<u32>,
    pub broadcast_rxed: RW<u32>,
    pub multicast_rxed: RW<u32>,
    pub pause_frames_rxed: RW<u32>,
    pub frames_rxed_64: RW<u32>,
    pub frames_rxed_65: RW<u32>,
    pub frames_rxed_128: RW<u32>,
    pub frames_rxed_256: RW<u32>,
    pub frames_rxed_512: RW<u32>,
    pub frames_rxed_1024: RW<u32>,
    pub frames_rxed_1519: RW<u32>,
    pub undersize_frames: RW<u32>,
    pub excessive_rx_length: RW<u32>,
    pub rx_jabbers: RW<u32>,
    pub fcs_errors: RW<u32>,
    pub rx_length_errors: RW<u32>,
    pub rx_symbol_errors: RW<u32>,
    pub alignment_errors: RW<u32>,
    pub rx_resource_errors: RW<u32>,
    pub rx_overruns: RW<u32>,
    pub rx_ip_ck_errors: RW<u32>,
    pub rx_tcp_ck_errors: RW<u32>,
    pub rx_udp_ck_errors: RW<u32>,
    pub auto_flushed_pkts: RW<u32>, // 0x1b4
    unused3: u32,
    pub tsu_timer_incr_sub_nsec: RW<u32>, // 0x1bc
    pub tsu_timer_msb_sec: RW<u32>,
    pub tsu_strobe_msb_sec: RO<u32>,
    pub tsu_strobe_sec: RO<u32>,
    pub tsu_strobe_nsec: RO<u32>,
    pub tsu_timer_sec: RW<u32>,
    pub tsu_timer_nsec: RW<u32>,
    pub tsu_timer_adjust: RW<u32>,
    pub tsu_timer_incr: RW<u32>,
    pub tsu_ptp_tx_sec: RO<u32>,
    pub tsu_ptp_tx_nsec: RO<u32>,
    pub tsu_ptp_rx_sec: RO<u32>,
    pub tsu_ptp_rx_nsec: RO<u32>,
    pub tsu_peer_tx_sec: RO<u32>,
    pub tsu_peer_tx_nsec: RO<u32>,
    pub tsu_peer_rx_sec: RO<u32>,
    pub tsu_peer_rx_nsec: RO<u32>,
    pub pcs_control: RW<u32>,
    pub pcs_status: RO<u32>,
    pub pcs_phy_top_id: RO<u32>,
    pub pcs_phy_bot_id: RO<u32>,
    pub pcs_an_adv: RW<u32>,
    pub pcs_an_lp_base: RO<u32>,
    pub pcs_an_exp: RO<u32>,
    pub pcs_an_np_tx: RW<u32>,
    pub pcs_an_lp_np: RO<u32>, // 0x220
    unused4: [u32; 6],
    pub pcs_an_ext_status: RO<u32>, // 0x23c
    unused5: [u32; 12],
    pub rx_lpi: RW<u32>, // 0x270
    pub rx_lpi_time: RW<u32>,
    pub tx_lpi: RW<u32>,
    pub tx_lpi_time: RW<u32>,
    pub designcfg_debug1: RO<u32>,
    pub designcfg_debug2: RO<u32>,
    pub designcfg_debug3: RO<u32>,
    pub designcfg_debug4: RO<u32>,
    pub designcfg_debug5: RO<u32>,
    pub designcfg_debug6: RO<u32>,
    pub designcfg_debug7: RO<u32>,
    pub designcfg_debug8: RO<u32>,
    pub designcfg_debug9: RO<u32>,
    pub designcfg_debug10: RO<u32>, // 0x2a4
    unused6: [u32; 86],
    pub int_q1_status: RW<u32>, // 0x400
    unused7: [u32; 15],
    pub transmit_q1_ptr: TransmitQPtr, // 0x440
    unused8: [u32; 15],
    pub receive_q1_ptr: ReceiveQPtr, // 0x480
    unused9: [u32; 7],
    pub dma_rxbuf_size_q1: RW<u32>, // 0x4a0
    unused10: [u32; 6],
    pub cbs_control: RW<u32>, // 0x4bc
    unused11: [u32; 2],
    pub upper_tx_q_base_addr: UpperTxQBaseAddr, // 0x4c8
    pub tx_bd_control: RW<u32>,
    pub rx_bd_control: RW<u32>,
    pub upper_rx_q_base_addr: UpperRxQBaseAddr, // 0x4d4
    unused12: [u32; 10],
    pub screening_type_1_register_0: RW<u32>, // 0x500
    pub screening_type_1_register_1: RW<u32>,
    pub screening_type_1_register_2: RW<u32>,
    pub screening_type_1_register_3: RW<u32>, // 0x50c
    unused13: [u32; 12],
    pub screening_type_2_register_0: RW<u32>, // 0x540
    pub screening_type_2_register_1: RW<u32>,
    pub screening_type_2_register_2: RW<u32>,
    pub screening_type_2_register_3: RW<u32>, // 0x54c
    unused14: [u32; 44],
    pub int_q1_enable: RW<u32>, // 0x600
    unused15: [u32; 7],
    pub int_q1_disable: RW<u32>, // 0x620
    unused16: [u32; 7],
    pub int_q1_mask: RO<u32>, // 0x640
    unused17: [u32; 39],
    pub screening_type_2_ethertype_reg_0: RW<u32>, // 0x6e0
    pub screening_type_2_ethertype_reg_1: RW<u32>,
    pub screening_type_2_ethertype_reg_2: RW<u32>,
    pub screening_type_2_ethertype_reg_3: RW<u32>, // 0x6ec
    unused18: [u32; 4],
    pub type2_compare_0_word_0: RW<u32>, // 0x700
    pub type2_compare_0_word_1: RW<u32>,
    pub type2_compare_1_word_0: RW<u32>,
    pub type2_compare_1_word_1: RW<u32>,
    pub type2_compare_2_word_0: RW<u32>,
    pub type2_compare_2_word_1: RW<u32>,
    pub type2_compare_3_word_0: RW<u32>,
    pub type2_compare_3_word_1: RW<u32>, // 0x71c
}

register_at!(RegisterBlock, 0xFF0B_0000, gem0);
register_at!(RegisterBlock, 0xFF0C_0000, gem1);
register_at!(RegisterBlock, 0xFF0D_0000, gem2);
register_at!(RegisterBlock, 0xFF0E_0000, gem3);

register!(network_control, NetworkControl, RW, u32);
register_bit!(network_control, one_step_sync_mode, 24);
register_bit!(network_control, ext_tsu_port_enable, 23);
register_bit!(network_control, store_udp_offset, 22);
register_bit!(network_control, alt_sgmii_mode, 21);
register_bit!(network_control, ptp_unicast_ena, 20);
register_bit!(network_control, tx_lpi_en, 19);
register_bit!(network_control, flush_rx_pkt_pclk, 18);
register_bit!(network_control, transmit_pfc_priority_based_pause_frame, 17);
register_bit!(network_control, pfc_enable, 16);
register_bit!(network_control, store_rx_ts, 15);
register_bit!(network_control, tx_pause_frame_zero, 12);
register_bit!(network_control, tx_pause_frame_req, 11);
register_bit!(network_control, tx_halt_pclk, 10);
register_bit!(network_control, tx_start_pclk, 9);
register_bit!(network_control, back_pressure, 8);
register_bit!(network_control, stats_write_en, 7);
register_bit!(network_control, inc_all_stats_regs, 6);
register_bit!(network_control, clear_all_stats_regs, 5);
register_bit!(network_control, man_port_en, 4);
register_bit!(network_control, enable_transmit, 3);
register_bit!(network_control, enable_receive, 2);
register_bit!(network_control, loopback_local, 1);
register_bit!(network_control, loopback, 0);

#[repr(u8)]
pub enum DataBusWidth {
    X32 = 0b00,
    X64 = 0b01,
    _Res0 = 0b10,
    _Res1 = 0b11,
}

register!(network_config, NetworkConfig, RW, u32);
register_bit!(network_config, uni_direction_enable, 31);
register_bit!(network_config, ignore_ipg_rx_er, 30);
register_bit!(network_config, nsp_change, 29);
register_bit!(network_config, ipg_stretch_enable, 28);
register_bit!(network_config, sgmii_mode_enable, 27);
register_bit!(network_config, ignore_rx_fcs, 26);
register_bit!(network_config, en_half_duplex_rx, 25);
register_bit!(network_config, receive_checksum_offload_enable, 24);
register_bit!(network_config, disable_copy_of_pause_frames, 23);
register_bits_typed!(network_config, data_bus_width, u8, DataBusWidth, 21, 22);
register_bits!(network_config, mdc_clock_division, u8, 18, 20);
register_bit!(network_config, fcs_remove, 17);
register_bit!(network_config, length_field_error_frame_discard, 16);
register_bits!(network_config, receive_buffer_offset, u8, 14, 15);
register_bit!(network_config, pause_enable, 13);
register_bit!(network_config, retry_test, 12);
register_bit!(network_config, pcs_select, 11);
register_bit!(network_config, gigabit_mode_enable, 10);
register_bit!(network_config, external_address_match_enable, 9);
register_bit!(network_config, receive_1536_byte_frames, 8);
register_bit!(network_config, unicast_hash_enable, 7);
register_bit!(network_config, multicast_hash_enable, 6);
register_bit!(network_config, no_broadcast, 5);
register_bit!(network_config, copy_all_frames, 4);
register_bit!(network_config, jumbo_frames, 3);
register_bit!(network_config, discard_non_vlan_frames, 2);
register_bit!(network_config, full_duplex, 1);
register_bit!(network_config, speed, 0);

register!(network_status, NetworkStatus, RO, u32);
register_bit!(network_status, lpi_indicate_pclk, 7);
register_bit!(network_status, pfc_negotiate_pclk, 6);
register_bit!(network_status, mac_pause_tx_en, 5);
register_bit!(network_status, mac_pause_rx_en, 4);
register_bit!(network_status, mac_full_duplex, 3);
register_bit!(network_status, man_done, 2);
register_bit!(network_status, mdio_in, 1);
register_bit!(network_status, pcs_link_state, 0);

register!(dma_config, DMAConfig, RW, u32);
register_bit!(dma_config, dma_addr_bus_width_1, 30);
register_bit!(dma_config, tx_bd_extended_mode_en, 29);
register_bit!(dma_config, rx_bd_extended_mode_en, 28);
register_bit!(dma_config, force_max_amba_burst_tx, 26);
register_bit!(dma_config, force_max_amba_burst_rx, 25);
register_bit!(dma_config, force_discard_on_err, 24);
register_bits!(dma_config, rx_buf_size, u8, 16, 23);
register_bit!(dma_config, tx_pbuf_tcp_en, 11);
register_bit!(dma_config, tx_pbuf_size, 10);
register_bits!(dma_config, rx_pbuf_size, u8, 8, 9);
register_bit!(dma_config, endian_swap_packet, 7);
register_bit!(dma_config, endian_swap_management, 6);
register_bits!(dma_config, amba_burst_length, u8, 0, 4);

register!(transmit_status, TransmitStatus, RW, u32);
register_bit!(transmit_status, resp_not_ok, 8, WTC);
register_bit!(transmit_status, late_collision_occurred, 7, WTC);
register_bit!(transmit_status, transmit_under_run, 6, WTC);
register_bit!(transmit_status, transmit_complete, 5, WTC);
register_bit!(transmit_status, amba_error, 4, WTC);
register_bit!(transmit_status, transmit_go, 3, WTC);
register_bit!(transmit_status, retry_limit_exceeded, 2, WTC);
register_bit!(transmit_status, collision_occurred, 1, WTC);
register_bit!(transmit_status, used_bit_read, 0, WTC);

register!(receive_q_ptr, ReceiveQPtr, RW, u32);
register_bits!(receive_q_ptr, dma_rx_q_ptr, u32, 2, 31);

register!(transmit_q_ptr, TransmitQPtr, RW, u32);
register_bits!(transmit_q_ptr, dma_tx_q_ptr, u32, 2, 31);

register!(receive_status, ReceiveStatus, RW, u32);
register_bit!(receive_status, resp_not_ok, 3, WTC);
register_bit!(receive_status, receive_overrun, 2, WTC);
register_bit!(receive_status, frame_received, 1, WTC);
register_bit!(receive_status, buffer_not_available, 0, WTC);

register!(int_status, IntStatus, RW, u32);
register_bit!(int_status, tsu_timer_comparison_interrupt, 29, WTC);
register_bit!(int_status, wol_interrupt, 28, WTC);
register_bit!(
    int_status,
    receive_lpi_indication_status_bit_change,
    27,
    WTC
);
register_bit!(int_status, tsu_seconds_register_increment, 26, WTC);
register_bit!(int_status, ptp_pdelay_resp_frame_transmitted, 25, WTC);
register_bit!(int_status, ptp_pdelay_req_frame_transmitted, 24, WTC);
register_bit!(int_status, ptp_pdelay_resp_frame_received, 23, WTC);
register_bit!(int_status, ptp_pdelay_req_frame_received, 22, WTC);
register_bit!(int_status, ptp_sync_frame_transmitted, 21, WTC);
register_bit!(int_status, ptp_delay_req_frame_transmitted, 20, WTC);
register_bit!(int_status, ptp_sync_frame_received, 19, WTC);
register_bit!(int_status, ptp_delay_req_frame_received, 18, WTC);
register_bit!(int_status, pcs_link_partner_page_received, 17, WTC);
register_bit!(int_status, pcs_auto_negotiation_complete, 16, WTC);
register_bit!(int_status, external_interrupt, 15, WTC);
register_bit!(int_status, pause_frame_transmitted, 14, WTC);
register_bit!(int_status, pause_time_elapsed, 13, WTC);
register_bit!(
    int_status,
    pause_frame_with_non_zero_pause_quantum_received,
    12,
    WTC
);
register_bit!(int_status, resp_not_ok, 11, WTC);
register_bit!(int_status, receive_overrun, 10, WTC);
register_bit!(int_status, link_change, 9, WTC);
register_bit!(int_status, transmit_complete, 7, WTC);
register_bit!(int_status, amba_error, 6, WTC);
register_bit!(int_status, retry_limit_exceeded_or_late_collision, 5, WTC);
register_bit!(int_status, transmit_under_run, 4, WTC);
register_bit!(int_status, tx_used_bit_read, 3, WTC);
register_bit!(int_status, rx_used_bit_read, 2, WTC);
register_bit!(int_status, receive_complete, 1, WTC);
register_bit!(int_status, management_frame_sent, 0, WTC);

register!(int_enable, IntEnable, WO, u32);
register_bit!(int_enable, tsu_timer_comparison_interrupt, 29);
register_bit!(int_enable, wol_interrupt, 28);
register_bit!(int_enable, receive_lpi_indication_status_bit_change, 27);
register_bit!(int_enable, tsu_seconds_register_increment, 26);
register_bit!(int_enable, ptp_pdelay_resp_frame_transmitted, 25);
register_bit!(int_enable, ptp_pdelay_req_frame_transmitted, 24);
register_bit!(int_enable, ptp_pdelay_resp_frame_received, 23);
register_bit!(int_enable, ptp_pdelay_req_frame_received, 22);
register_bit!(int_enable, ptp_sync_frame_transmitted, 21);
register_bit!(int_enable, ptp_delay_req_frame_transmitted, 20);
register_bit!(int_enable, ptp_sync_frame_received, 19);
register_bit!(int_enable, ptp_delay_req_frame_received, 18);
register_bit!(int_enable, pcs_link_partner_page_received, 17);
register_bit!(int_enable, pcs_auto_negotiation_complete, 16);
register_bit!(int_enable, external_interrupt, 15);
register_bit!(int_enable, pause_frame_transmitted, 14);
register_bit!(int_enable, pause_time_elapsed, 13);
register_bit!(
    int_enable,
    pause_frame_with_non_zero_pause_quantum_received,
    12
);
register_bit!(int_enable, resp_not_ok, 11);
register_bit!(int_enable, receive_overrun, 10);
register_bit!(int_enable, link_change, 9);
register_bit!(int_enable, transmit_complete, 7);
register_bit!(int_enable, amba_error, 6);
register_bit!(int_enable, retry_limit_exceeded_or_late_collision, 5);
register_bit!(int_enable, transmit_under_run, 4);
register_bit!(int_enable, tx_used_bit_read, 3);
register_bit!(int_enable, rx_used_bit_read, 2);
register_bit!(int_enable, receive_complete, 1);
register_bit!(int_enable, management_frame_sent, 0);

register!(int_disable, IntDisable, WO, u32);
register_bit!(int_disable, tsu_timer_comparison_interrupt, 29);
register_bit!(int_disable, wol_interrupt, 28);
register_bit!(int_disable, receive_lpi_indication_status_bit_change, 27);
register_bit!(int_disable, tsu_seconds_register_increment, 26);
register_bit!(int_disable, ptp_pdelay_resp_frame_transmitted, 25);
register_bit!(int_disable, ptp_pdelay_req_frame_transmitted, 24);
register_bit!(int_disable, ptp_pdelay_resp_frame_received, 23);
register_bit!(int_disable, ptp_pdelay_req_frame_received, 22);
register_bit!(int_disable, ptp_sync_frame_transmitted, 21);
register_bit!(int_disable, ptp_delay_req_frame_transmitted, 20);
register_bit!(int_disable, ptp_sync_frame_received, 19);
register_bit!(int_disable, ptp_delay_req_frame_received, 18);
register_bit!(int_disable, pcs_link_partner_page_received, 17);
register_bit!(int_disable, pcs_auto_negotiation_complete, 16);
register_bit!(int_disable, external_interrupt, 15);
register_bit!(int_disable, pause_frame_transmitted, 14);
register_bit!(int_disable, pause_time_elapsed, 13);
register_bit!(
    int_disable,
    pause_frame_with_non_zero_pause_quantum_received,
    12
);
register_bit!(int_disable, resp_not_ok, 11);
register_bit!(int_disable, receive_overrun, 10);
register_bit!(int_disable, link_change, 9);
register_bit!(int_disable, transmit_complete, 7);
register_bit!(int_disable, amba_error, 6);
register_bit!(int_disable, retry_limit_exceeded_or_late_collision, 5);
register_bit!(int_disable, transmit_under_run, 4);
register_bit!(int_disable, tx_used_bit_read, 3);
register_bit!(int_disable, rx_used_bit_read, 2);
register_bit!(int_disable, receive_complete, 1);
register_bit!(int_disable, management_frame_sent, 0);

register!(int_mask, IntMask, RO, u32);
register_bit!(int_mask, tsu_timer_comparison_interrupt, 29);
register_bit!(int_mask, wol_interrupt, 28);
register_bit!(int_mask, receive_lpi_indication_status_bit_change, 27);
register_bit!(int_mask, tsu_seconds_register_increment, 26);
register_bit!(int_mask, ptp_pdelay_resp_frame_transmitted, 25);
register_bit!(int_mask, ptp_pdelay_req_frame_transmitted, 24);
register_bit!(int_mask, ptp_pdelay_resp_frame_received, 23);
register_bit!(int_mask, ptp_pdelay_req_frame_received, 22);
register_bit!(int_mask, ptp_sync_frame_transmitted, 21);
register_bit!(int_mask, ptp_delay_req_frame_transmitted, 20);
register_bit!(int_mask, ptp_sync_frame_received, 19);
register_bit!(int_mask, ptp_delay_req_frame_received, 18);
register_bit!(int_mask, pcs_link_partner_page_received, 17);
register_bit!(int_mask, pcs_auto_negotiation_complete, 16);
register_bit!(int_mask, external_interrupt, 15);
register_bit!(int_mask, pause_frame_transmitted, 14);
register_bit!(int_mask, pause_time_elapsed, 13);
register_bit!(
    int_mask,
    pause_frame_with_non_zero_pause_quantum_received,
    12
);
register_bit!(int_mask, resp_not_ok, 11);
register_bit!(int_mask, receive_overrun, 10);
register_bit!(int_mask, link_change, 9);
register_bit!(int_mask, transmit_complete, 7);
register_bit!(int_mask, amba_error, 6);
register_bit!(int_mask, retry_limit_exceeded_or_late_collision, 5);
register_bit!(int_mask, transmit_under_run, 4);
register_bit!(int_mask, tx_used_bit_read, 3);
register_bit!(int_mask, rx_used_bit_read, 2);
register_bit!(int_mask, receive_complete, 1);
register_bit!(int_mask, management_frame_sent, 0);

#[repr(u8)]
// NOTE: differs between clause 22/45 interfaces
// this is for clause 22
pub enum PhyOperation {
    Read = 0b10,
    Write = 0b01,
}
register!(phy_management, PhyManagement, RW, u32);
register_bit!(phy_management, clause_22, 30);
register_bits_typed!(phy_management, operation, u8, PhyOperation, 28, 29);
register_bits!(phy_management, phy_address, u8, 23, 27);
register_bits!(phy_management, register_address, u8, 18, 22);
register_bits!(phy_management, write10, u8, 16, 17);
register_bits!(phy_management, phy_write_read_data, u16, 0, 15);

register!(external_fifo_interface, ExternalFifoInterface, RW, u32);
register_bit!(external_fifo_interface, enable, 0);

register!(spec_add_bottom, SpecAddBottom, RW, u32);
register_bits!(spec_add_bottom, address, u32, 0, 31);

register!(spec_add_top, SpecAddTop, RW, u32);
register_bit!(spec_add_top, filter_type, 16);
register_bits!(spec_add_top, address, u16, 0, 15);

register!(spec_type, SpecType, RW, u32);
register_bit!(spec_type, enable_copy, 31);
register_bits!(spec_type, match_val, u16, 0, 15);

register!(mask_add_bottom, MaskAddBottom, RW, u32);
register_bits!(mask_add_bottom, address_mask, u32, 0, 31);

register!(mask_add_top, MaskAddTop, RW, u32);
register_bits!(mask_add_top, address_mask, u16, 0, 15);

register!(upper_tx_q_base_addr, UpperTxQBaseAddr, RW, u32);
register_bits!(upper_tx_q_base_addr, upper_tx_q_base_addr, u32, 0, 31);

register!(upper_rx_q_base_addr, UpperRxQBaseAddr, RW, u32);
register_bits!(upper_rx_q_base_addr, upper_rx_q_base_addr, u32, 0, 31);
