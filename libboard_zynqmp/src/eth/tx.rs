// Copied from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: be672ab
// Modifications:
// - minor changes for cortex a9 -> a53
// - add dummy descriptor to tie off q1
use super::{regs, Buffer};
use alloc::{vec, vec::Vec};
use core::ops::{Deref, DerefMut};
use libcortex_a53::{
    cache::{dcc_poc, dcc_slice_poc},
    uncached::UncachedSlice,
};
use libregister::*;

/// Descriptor entry
#[repr(C, align(0x10))]
pub struct DescEntry {
    word0: DescWord0,
    word1: DescWord1,
    word2: DescWord2,
    word3: DescWord3,
}

register!(desc_word0, DescWord0, VolatileCell, u32);
register_bits!(desc_word0, address_lsbs, u32, 0, 31);

register!(desc_word1, DescWord1, VolatileCell, u32);
register_bits!(desc_word1, length, u16, 0, 13);
register_bit!(desc_word1, last_buffer, 15);
register_bit!(desc_word1, no_crc_append, 16);
register_bits!(desc_word1, csum_offload_errors, u8, 20, 22);
register_bit!(desc_word1, late_collision_tx_error, 26);
register_bit!(desc_word1, ahb_frame_corruption, 27);
register_bit!(desc_word1, retry_limit_exceeded, 29);
register_bit!(
    desc_word1,
    /// marks last descriptor in list
    wrap,
    30
);
register_bit!(
    desc_word1,
    /// true if owned by software, false if owned by hardware
    used,
    31
);

register!(desc_word2, DescWord2, VolatileCell, u32);
register_bits!(desc_word2, address_msbs, u32, 0, 31);

// unused
register!(desc_word3, DescWord3, VolatileCell, u32);

impl DescEntry {
    pub fn zeroed() -> Self {
        DescEntry {
            word0: DescWord0 {
                inner: VolatileCell::new(0),
            },
            word1: DescWord1 {
                inner: VolatileCell::new(0),
            },
            word2: DescWord2 {
                inner: VolatileCell::new(0),
            },
            word3: DescWord3 {
                inner: VolatileCell::new(0),
            },
        }
    }
}

#[repr(C)]
pub struct DescList {
    list: UncachedSlice<DescEntry>,
    dummy_descriptor: DescEntry,
    buffers: Vec<Buffer>,
    next: usize,
}

impl DescList {
    pub fn new(size: usize) -> Self {
        let mut list = UncachedSlice::new(size, || DescEntry::zeroed()).unwrap();
        let mut buffers = vec![Buffer::new(); size];

        let last = list.len().min(buffers.len()) - 1;
        // Sending seems to not work properly with only one packet
        // buffer (two duplicates get send with every packet), so
        // check that at least 2 are allocated, i.e. that the index of
        // the last one is at least one.
        assert!(last > 0);

        for (i, (entry, buffer)) in list.iter_mut().zip(buffers.iter_mut()).enumerate() {
            let is_last = i == last;
            let buffer_addr = &mut buffer.0[0] as *mut _ as u64;
            let address_lsbs = (buffer_addr & 0xffff_ffff) as u32;
            let address_msbs = (buffer_addr >> 32) as u32;
            entry
                .word0
                .write(DescWord0::zeroed().address_lsbs(address_lsbs));
            entry.word1.write(
                DescWord1::zeroed()
                    .used(true)
                    .wrap(is_last)
                    // every frame contains 1 packet
                    .last_buffer(true),
            );
            entry
                .word2
                .write(DescWord2::zeroed().address_msbs(address_msbs));
        }

        // dummy descriptor to "tie off" q1
        // doesn't need to be uncached as it should never be modified
        let mut dummy_descriptor = DescEntry::zeroed();
        dummy_descriptor
            .word1
            .write(DescWord1::zeroed().used(true).wrap(true).last_buffer(true));
        dcc_poc(&dummy_descriptor);

        DescList {
            list,
            dummy_descriptor,
            buffers,
            next: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.list.len().min(self.buffers.len())
    }

    pub fn list_addr(&self) -> u64 {
        &self.list[0] as *const _ as u64
    }

    pub fn dummy_desc_addr(&self) -> u32 {
        &self.dummy_descriptor as *const _ as u32
    }

    pub fn can_send(&self) -> bool {
        self.list[self.next].word1.read().used()
    }

    pub fn send<'s: 'p, 'p>(
        &'s mut self,
        regs: &'s mut regs::RegisterBlock,
        length: usize,
    ) -> Option<PktRef<'p>> {
        let list_len = self.list.len();
        let entry = &mut self.list[self.next];
        if entry.word1.read().used() {
            let buffer = &mut self.buffers[self.next][0..length];
            entry.word1.write(
                DescWord1::zeroed()
                    .length(length as u16)
                    .last_buffer(true)
                    .wrap(self.next >= list_len - 1)
                    .used(true),
            );

            self.next += 1;
            if self.next >= list_len {
                self.next = 0;
            }

            Some(PktRef {
                entry,
                buffer,
                regs,
            })
        } else {
            // Still in use by HW (sending too fast, ring exceeded)
            None
        }
    }
}

/// Releases a buffer back to the HW upon Drop, and start the TX
/// engine
pub struct PktRef<'a> {
    entry: &'a mut DescEntry,
    buffer: &'a mut [u8],
    regs: &'a mut regs::RegisterBlock,
}

impl<'a> Drop for PktRef<'a> {
    fn drop(&mut self) {
        // Write back all dirty cachelines of this buffer
        dcc_slice_poc(self.buffer);

        self.entry.word1.modify(|_, w| w.used(false));
        // Start the TX engine
        self.regs
            .network_control
            .modify(|_, w| w.tx_start_pclk(true));
    }
}

impl<'a> Deref for PktRef<'a> {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        self.buffer
    }
}

impl<'a> DerefMut for PktRef<'a> {
    fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
        self.buffer
    }
}

/// TxToken for smoltcp support
pub struct Token<'a> {
    pub regs: &'a mut regs::RegisterBlock,
    pub desc_list: &'a mut DescList,
}

impl<'a> smoltcp::phy::TxToken for Token<'a> {
    fn consume<R, F>(self, len: usize, f: F) -> R
    where
        F: FnOnce(&mut [u8]) -> R,
    {
        match self.desc_list.send(self.regs, len) {
            Some(mut pktref) => f(pktref.deref_mut()),
            None => panic!("No tx buffers available"),
        }
    }
}
