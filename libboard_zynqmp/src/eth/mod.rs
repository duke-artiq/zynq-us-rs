//! Copied from https://git.m-labs.hk/M-labs/zynq-rs
//! Commit: be672ab
//! Modifications:
//! - smoltcp -> 0.10.0
//! - additional GEMs (4 total vs. 2)
//! - remove clock/mio setup as that's already done elsewhere
//! - different registers
//! - buffer alignment (64 vs. 32 byte cache lines)
//! - ability to access extended phy register space
use crate::{
    clocks::Clocks,
    slcr::{
        common::Unlocked,
        crl_apb::{self, IoClkSource},
        iou_slcr,
    },
};
use core::{
    convert::TryInto,
    marker::PhantomData,
    ops::{Deref, DerefMut},
};
use libregister::*;
use log::{debug, error, info, warn};

pub mod phy;
use phy::{ExtendedPhyAccess, Phy, PhyAccess, PhyRegister};

pub mod regs;
pub mod rx;
pub mod tx;

/// Size of all the buffers
pub const MTU: usize = 1536;
/// Maximum MDC clock
const MAX_MDC: u32 = 2_500_000;
const TX_10: u32 = 10_000_000;
const TX_100: u32 = 25_000_000;
/// Clock for GbE
const TX_1000: u32 = 125_000_000;

#[allow(unused)]
pub fn test_addrs() {
    let gem = regs::RegisterBlock::gem3();
    assert_eq!(
        &gem.type2_compare_3_word_1 as *const _ as u32 - &gem.network_control as *const _ as u32,
        0x71c
    );
}

#[derive(Clone)]
// align to cache line length (64 bytes)
#[repr(C, align(0x40))]
pub struct Buffer(pub [u8; MTU]);

impl Buffer {
    pub const fn new() -> Self {
        Buffer([0; MTU])
    }
}

impl Deref for Buffer {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
        &mut self.0
    }
}

/// Gigabit Ethernet Peripheral
pub trait Gem {
    fn setup_clock(tx_clock: u32);
    fn regs() -> &'static mut regs::RegisterBlock;
}

/// first Gigabit Ethernet peripheral
pub struct Gem0;

impl Gem for Gem0 {
    fn setup_clock(tx_clock: u32) {
        let (divisor0, divisor1) = calculate_tx_divisors(tx_clock);

        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.gem0_clk_ctrl.write(
                crl_apb::GemClkCtrl::zeroed()
                    .rx_clkact(true)
                    .clkact(true)
                    .divisor1(divisor1)
                    .divisor0(divisor0)
                    .srcsel(IoClkSource::IoPll),
            );
        })
    }

    fn regs() -> &'static mut regs::RegisterBlock {
        regs::RegisterBlock::gem0()
    }
}

/// second Gigabit Ethernet peripheral
pub struct Gem1;

impl Gem for Gem1 {
    fn setup_clock(tx_clock: u32) {
        let (divisor0, divisor1) = calculate_tx_divisors(tx_clock);

        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.gem1_clk_ctrl.write(
                crl_apb::GemClkCtrl::zeroed()
                    .rx_clkact(true)
                    .clkact(true)
                    .divisor1(divisor1)
                    .divisor0(divisor0)
                    .srcsel(IoClkSource::IoPll),
            );
        })
    }

    fn regs() -> &'static mut regs::RegisterBlock {
        regs::RegisterBlock::gem1()
    }
}

/// third Gigabit Ethernet peripheral
pub struct Gem2;

impl Gem for Gem2 {
    fn setup_clock(tx_clock: u32) {
        let (divisor0, divisor1) = calculate_tx_divisors(tx_clock);

        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.gem2_clk_ctrl.write(
                crl_apb::GemClkCtrl::zeroed()
                    .rx_clkact(true)
                    .clkact(true)
                    .divisor1(divisor1)
                    .divisor0(divisor0)
                    .srcsel(IoClkSource::IoPll),
            );
        })
    }

    fn regs() -> &'static mut regs::RegisterBlock {
        regs::RegisterBlock::gem2()
    }
}

/// fourth Gigabit Ethernet peripheral
pub struct Gem3;

impl Gem for Gem3 {
    fn setup_clock(tx_clock: u32) {
        let (divisor0, divisor1) = calculate_tx_divisors(tx_clock);

        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.gem3_clk_ctrl.write(
                crl_apb::GemClkCtrl::zeroed()
                    .rx_clkact(true)
                    .clkact(true)
                    .divisor1(divisor1)
                    .divisor0(divisor0)
                    .srcsel(IoClkSource::IoPll),
            );
        })
    }

    fn regs() -> &'static mut regs::RegisterBlock {
        regs::RegisterBlock::gem3()
    }
}

fn calculate_tx_divisors(tx_clock: u32) -> (u8, u8) {
    let io_pll = Clocks::get().io;
    let target = (tx_clock - 1 + io_pll) / tx_clock;

    let mut best = None;
    let mut best_error = 0;
    for divisor0 in 1..63 {
        for divisor1 in 1..63 {
            let current = (divisor0 as u32) * (divisor1 as u32);
            let error = if current > target {
                current - target
            } else {
                target - current
            };
            if best.is_none() || best_error > error {
                best = Some((divisor0, divisor1));
                best_error = error;
            }
        }
    }
    let result = best.unwrap();
    debug!(
        "Eth TX clock for {}: {} / {} / {} = {}",
        tx_clock,
        io_pll,
        result.0,
        result.1,
        io_pll / result.0 as u32 / result.1 as u32
    );
    result
}

pub struct Eth<GEM: Gem, RX, TX> {
    rx: RX,
    tx: TX,
    inner: EthInner<GEM>,
    phy: Phy,
    /// keep track of RX path occupation to avoid needless `check_link_change()`
    idle: bool,
}

impl Eth<Gem3, (), ()> {
    pub fn eth3(macaddr: [u8; 6]) -> Self {
        let iou_slcr = iou_slcr::RegisterBlock::iou_slcr();
        iou_slcr.gem_clk_ctrl.write(iou_slcr::GemClkCtrl::zeroed());
        iou_slcr.iou_interconnect_route.modify(|_, w| w.gem3(false));
        crl_apb::RegisterBlock::unlocked(|crl_apb| {
            crl_apb.gem_rst_ctrl.modify(|_, w| w.gem3_rst(true));
            crl_apb.gem_rst_ctrl.modify(|_, w| w.gem3_rst(false));
        });
        Self::gem_common(macaddr)
    }
}

impl<GEM: Gem> Eth<GEM, (), ()> {
    fn gem_common(macaddr: [u8; 6]) -> Self {
        GEM::setup_clock(TX_1000);

        let mut inner = EthInner {
            gem: PhantomData,
            link: None,
        };
        inner.init();

        inner.configure(macaddr);

        let phy = Phy::find(&mut inner).expect("phy");
        phy.reset(&mut inner);

        #[cfg(feature = "target_zcu111")]
        {
            // workaround for DP83867 strap quirk
            phy.modify_cfg4(&mut inner, |cfg4| cfg4.set_int_tst_mode_1(false));
            // delays taken from uboot driver
            phy.modify_rgmiictl(&mut inner, |rgmiictl| {
                rgmiictl
                    .set_rgmii_tx_clk_delay(true)
                    .set_rgmii_rx_clk_delay(true)
            });
            phy.modify_rgmiidctl(&mut inner, |rgmiidctl| {
                rgmiidctl
                    .set_rgmii_tx_delay_ctrl(0xa)
                    .set_rgmii_rx_delay_ctrl(0x8)
            });
        }

        phy.restart_autoneg(&mut inner);

        Eth {
            rx: (),
            tx: (),
            inner,
            phy,
            idle: true,
        }
    }
}

impl<GEM: Gem, RX, TX> Eth<GEM, RX, TX> {
    pub fn start_rx(self, rx_size: usize) -> Eth<GEM, rx::DescList, TX> {
        let new_self = Eth {
            rx: rx::DescList::new(rx_size),
            tx: self.tx,
            inner: self.inner,
            phy: self.phy,
            idle: self.idle,
        };
        let list_addr = new_self.rx.list_addr();
        assert!(list_addr & 0b11 == 0);
        let address_lsbs = (list_addr & 0xffff_ffff) as u32;
        let address_msbs = (list_addr >> 32) as u32;
        GEM::regs()
            .receive_q_ptr
            .write(regs::ReceiveQPtr::zeroed().dma_rx_q_ptr(address_lsbs >> 2));
        GEM::regs()
            .upper_rx_q_base_addr
            .write(regs::UpperRxQBaseAddr::zeroed().upper_rx_q_base_addr(address_msbs));
        let dummy_desc_addr = new_self.rx.dummy_desc_addr();
        assert!(dummy_desc_addr & 0b11 == 0);
        // if there is a register for the q1 bd msbs, it's undocumented
        GEM::regs()
            .receive_q1_ptr
            .write(regs::ReceiveQPtr::zeroed().dma_rx_q_ptr(dummy_desc_addr >> 2));
        GEM::regs()
            .network_control
            .modify(|_, w| w.enable_receive(true));
        new_self
    }

    pub fn start_tx(self, tx_size: usize) -> Eth<GEM, RX, tx::DescList> {
        let new_self = Eth {
            rx: self.rx,
            tx: tx::DescList::new(tx_size),
            inner: self.inner,
            phy: self.phy,
            idle: self.idle,
        };
        let list_addr = new_self.tx.list_addr();
        assert!(list_addr & 0b11 == 0);
        let address_lsbs = (list_addr & 0xffff_ffff) as u32;
        let address_msbs = (list_addr >> 32) as u32;
        GEM::regs()
            .transmit_q_ptr
            .write(regs::TransmitQPtr::zeroed().dma_tx_q_ptr(address_lsbs >> 2));
        GEM::regs()
            .upper_tx_q_base_addr
            .write(regs::UpperTxQBaseAddr::zeroed().upper_tx_q_base_addr(address_msbs));
        let dummy_desc_addr = new_self.tx.dummy_desc_addr();
        assert!(dummy_desc_addr & 0b11 == 0);
        // if there is a register for the q1 bd msbs, it's undocumented
        GEM::regs()
            .transmit_q1_ptr
            .write(regs::TransmitQPtr::zeroed().dma_tx_q_ptr(dummy_desc_addr >> 2));
        GEM::regs()
            .network_control
            .modify(|_, w| w.enable_transmit(true));
        new_self
    }
}

impl<GEM: Gem, TX> libasync::smoltcp::LinkCheck for Eth<GEM, rx::DescList, TX> {
    type Link = Option<phy::Link>;

    fn check_link_change(&mut self) -> Option<Option<phy::Link>> {
        self.inner.check_link_change(&self.phy)
    }

    fn is_idle(&self) -> bool {
        self.idle
    }
}

impl<GEM: Gem> smoltcp::phy::Device for Eth<GEM, rx::DescList, tx::DescList> {
    type RxToken<'a> = rx::PktRef<'a> where Self: 'a;
    type TxToken<'a> = tx::Token<'a> where Self: 'a;

    fn capabilities(&self) -> smoltcp::phy::DeviceCapabilities {
        use smoltcp::phy::{Checksum, ChecksumCapabilities, DeviceCapabilities, Medium};

        let mut checksum_caps = ChecksumCapabilities::default();
        checksum_caps.ipv4 = Checksum::Both;
        checksum_caps.tcp = Checksum::Both;
        checksum_caps.udp = Checksum::Both;

        let mut caps = DeviceCapabilities::default();
        caps.max_transmission_unit = MTU;
        caps.max_burst_size = Some(self.rx.len().min(self.tx.len()));
        caps.checksum = checksum_caps;
        caps.medium = Medium::Ethernet;

        caps
    }

    fn receive(
        &mut self,
        _timestamp: smoltcp::time::Instant,
    ) -> Option<(Self::RxToken<'_>, Self::TxToken<'_>)> {
        match self.rx.recv_next() {
            Ok(Some(pktref)) => {
                if self.tx.can_send() {
                    let tx_token = tx::Token {
                        regs: GEM::regs(),
                        desc_list: &mut self.tx,
                    };
                    self.idle = false;
                    Some((pktref, tx_token))
                } else {
                    warn!("Tx buffer exhausted, dropping received packet");
                    None
                }
            }
            Ok(None) => {
                self.idle = true;
                None
            }
            Err(e) => {
                error!("eth recv error: {:?}", e);
                None
            }
        }
    }

    fn transmit(&mut self, _timestamp: smoltcp::time::Instant) -> Option<Self::TxToken<'_>> {
        if self.tx.can_send() {
            Some(tx::Token {
                regs: GEM::regs(),
                desc_list: &mut self.tx,
            })
        } else {
            None
        }
    }
}

struct EthInner<GEM: Gem> {
    gem: PhantomData<GEM>,
    link: Option<phy::Link>,
}

impl<GEM: Gem> EthInner<GEM> {
    /// UG1085 Chapter 34: GEM Ethernet -> Programming Model -> Initialize the Controller
    fn init(&mut self) {
        let regs = GEM::regs();
        regs.network_control.write(regs::NetworkControl::zeroed());
        // clear stats, flush dpram
        regs.network_control.write(
            regs::NetworkControl::zeroed()
                .clear_all_stats_regs(true)
                .flush_rx_pkt_pclk(true),
        );

        // clear status registers
        regs.receive_status.write(
            regs::ReceiveStatus::zeroed()
                .resp_not_ok()
                .receive_overrun()
                .frame_received()
                .buffer_not_available(),
        );
        // note: UG1085 says to write 0xFF in order to clear, but that leaves out resp_not_ok and
        // includes transmit_go, which is listed as RO in UG1087
        regs.transmit_status.write(
            regs::TransmitStatus::zeroed()
                .resp_not_ok()
                .late_collision_occurred()
                .transmit_under_run()
                .transmit_complete()
                .amba_error()
                .transmit_go()
                .retry_limit_exceeded()
                .collision_occurred()
                .used_bit_read(),
        );

        // disable all interrupts
        // note: UG1085 says to write 0x7FF_FEFF, presumably a typo
        regs.int_disable
            .write(regs::int_disable::Write { inner: 0x7fff_feff });
        // clear interrupt status
        regs.int_status
            .modify(|r, _| regs::int_status::Write { inner: r.inner });
        // q1 interrupts
        unsafe {
            regs.int_q1_disable.write(0xffff_ffff);
            regs.int_q1_status.write(0xffff_ffff);
        }

        // clear buffer queues
        regs.receive_q_ptr.write(regs::ReceiveQPtr::zeroed());
        regs.upper_rx_q_base_addr
            .write(regs::UpperRxQBaseAddr::zeroed());
        regs.receive_q1_ptr.write(regs::ReceiveQPtr::zeroed());
        regs.transmit_q_ptr.write(regs::TransmitQPtr::zeroed());
        regs.upper_tx_q_base_addr
            .write(regs::UpperTxQBaseAddr::zeroed());
        regs.transmit_q1_ptr.write(regs::TransmitQPtr::zeroed());
    }

    /// UG1085 Chapter 34: GEM Ethernet -> Programming Model -> Configure the Controller
    fn configure(&mut self, macaddr: [u8; 6]) {
        let regs = GEM::regs();
        let target_div = Clocks::get().lpd_lsbus_ref_clk().div_ceil(MAX_MDC);
        let mdc_clock_div = if target_div <= 8 {
            0b000
        } else if target_div <= 16 {
            0b001
        } else if target_div <= 32 {
            0b010
        } else if target_div <= 48 {
            0b011
        } else if target_div <= 64 {
            0b100
        } else if target_div <= 96 {
            0b101
        } else if target_div <= 128 {
            0b110
        } else if target_div <= 224 {
            0b111
        } else {
            panic!(
                "LPD_LSBUS frequency too high for max MDC frequency {} Hz",
                MAX_MDC
            );
        };
        debug!(
            "Setting MDC clock div to {:#b} (target div: {})",
            mdc_clock_div, target_div
        );
        regs.network_config.write(
            regs::NetworkConfig::zeroed()
                .receive_checksum_offload_enable(true)
                .disable_copy_of_pause_frames(true)
                .data_bus_width(regs::DataBusWidth::X64)
                .mdc_clock_division(mdc_clock_div)
                .fcs_remove(true)
                .length_field_error_frame_discard(true)
                .gigabit_mode_enable(true)
                .receive_1536_byte_frames(true)
                .multicast_hash_enable(true)
                .full_duplex(true),
        );

        let mac_addr_bottom = u32::from_le_bytes(macaddr[0..4].try_into().unwrap());
        let mac_addr_top = u16::from_le_bytes(macaddr[4..].try_into().unwrap());
        // NOTE: bottom must be written first -- writing to bottom deactivates the address,
        // writing to top reactivates it
        regs.spec_add1_bottom
            .write(regs::SpecAddBottom::zeroed().address(mac_addr_bottom));
        regs.spec_add1_top
            .write(regs::SpecAddTop::zeroed().address(mac_addr_top));
        // disable others
        regs.spec_add2_bottom.write(regs::SpecAddBottom::zeroed());
        regs.spec_add3_bottom.write(regs::SpecAddBottom::zeroed());
        regs.spec_add4_bottom.write(regs::SpecAddBottom::zeroed());
        regs.spec_type1.write(regs::SpecType::zeroed());
        regs.spec_type2.write(regs::SpecType::zeroed());
        regs.spec_type3.write(regs::SpecType::zeroed());
        regs.spec_type4.write(regs::SpecType::zeroed());
        regs.mask_add1_bottom.write(regs::MaskAddBottom::zeroed());
        regs.mask_add1_top.write(regs::MaskAddTop::zeroed());
        // just to make sure q1 isn't used
        unsafe {
            regs.screening_type_1_register_0.write(0);
            regs.screening_type_1_register_1.write(0);
            regs.screening_type_1_register_2.write(0);
            regs.screening_type_1_register_3.write(0);
            regs.screening_type_2_register_0.write(0);
            regs.screening_type_2_register_1.write(0);
            regs.screening_type_2_register_2.write(0);
            regs.screening_type_2_register_3.write(0);
        }

        // configure DMA
        regs.dma_config.write(
            regs::DMAConfig::zeroed()
                .dma_addr_bus_width_1(true) // 64-bit address
                .rx_buf_size((MTU >> 6) as u8) // defined in units of 64 bytes
                .tx_pbuf_tcp_en(true) // tx checksum offload
                .tx_pbuf_size(true) // 32 kB
                .rx_pbuf_size(0b11) // 32 kB
                .amba_burst_length(0x10), // size 16 bursts
        );

        // enable MDIO
        regs.network_control
            .write(regs::NetworkControl::zeroed().man_port_en(true));
    }

    fn wait_phy_idle(&self) {
        while !GEM::regs().network_status.read().man_done() {}
    }

    fn check_link_change(&mut self, phy: &Phy) -> Option<Option<phy::Link>> {
        let link = phy.get_link(self);

        // Check link state transition
        if self.link != link {
            match &link {
                Some(link) => {
                    info!("eth: got {:?}", link);

                    use phy::{LinkDuplex::Full, LinkSpeed::*};
                    let txclock = match link.speed {
                        S10 => TX_10,
                        S100 => TX_100,
                        S1000 => TX_1000,
                    };
                    GEM::setup_clock(txclock);
                    GEM::regs().network_config.modify(|_, w| {
                        w.full_duplex(link.duplex == Full)
                            .gigabit_mode_enable(link.speed == S1000)
                            .speed(link.speed != S10)
                    });
                }
                None => {
                    warn!("eth: link lost");
                    phy.modify_control(self, |control| {
                        control.set_autoneg_enable(true).set_restart_autoneg(true)
                    });
                }
            }

            self.link = link;
            Some(link)
        } else {
            None
        }
    }
}

impl<GEM: Gem> PhyAccess for EthInner<GEM> {
    fn read_phy(&mut self, addr: u8, reg: u8) -> u16 {
        self.wait_phy_idle();
        GEM::regs().phy_management.write(
            regs::PhyManagement::zeroed()
                .clause_22(true)
                .operation(regs::PhyOperation::Read)
                .phy_address(addr)
                .register_address(reg)
                .write10(0b10),
        );
        self.wait_phy_idle();
        GEM::regs().phy_management.read().phy_write_read_data()
    }

    fn write_phy(&mut self, addr: u8, reg: u8, data: u16) {
        self.wait_phy_idle();
        GEM::regs().phy_management.write(
            regs::PhyManagement::zeroed()
                .clause_22(true)
                .operation(regs::PhyOperation::Write)
                .phy_address(addr)
                .register_address(reg)
                .write10(0b10)
                .phy_write_read_data(data),
        );
        self.wait_phy_idle();
    }
}

impl<GEM: Gem> ExtendedPhyAccess for EthInner<GEM> {
    fn read_phy_extended(&mut self, addr: u8, devad: u16, reg: u16) -> u16 {
        // write reg address
        let mut regcr = phy::RegCR::from(0)
            .set_devad(devad)
            .set_function(phy::RegCRFunction::Address);
        self.write_phy(addr, phy::RegCR::addr(), regcr.into());
        let addar = phy::AdDaR::from(0).set_addr_or_data(reg);
        self.write_phy(addr, phy::AdDaR::addr(), addar.into());

        // read data
        regcr = regcr.set_function(phy::RegCRFunction::DataNoPostInc);
        self.write_phy(addr, phy::RegCR::addr(), regcr.into());
        self.read_phy(addr, phy::AdDaR::addr())
    }

    fn write_phy_extended(&mut self, addr: u8, devad: u16, reg: u16, data: u16) {
        // write reg address
        let mut regcr = phy::RegCR::from(0)
            .set_devad(devad)
            .set_function(phy::RegCRFunction::Address);
        self.write_phy(addr, phy::RegCR::addr(), regcr.into());
        let mut addar = phy::AdDaR::from(0).set_addr_or_data(reg);
        self.write_phy(addr, phy::AdDaR::addr(), addar.into());

        // write data
        regcr = regcr.set_function(phy::RegCRFunction::DataNoPostInc);
        self.write_phy(addr, phy::RegCR::addr(), regcr.into());
        addar = addar.set_addr_or_data(data);
        self.write_phy(addr, phy::AdDaR::addr(), addar.into());
    }
}
