use super::{PhyRegister, RegCRFunction};
use bit_field::BitField;

#[derive(Clone, Copy, Debug)]
/// Register Control Register
pub struct RegCR(u16);

impl RegCR {
    pub fn devad(&self) -> u16 {
        self.0.get_bits(0..=4)
    }

    pub fn set_devad(mut self, devad: u16) -> Self {
        self.0.set_bits(0..=4, devad);
        self
    }

    pub fn function(&self) -> RegCRFunction {
        match self.0.get_bits(14..=15) {
            0b00 => RegCRFunction::Address,
            0b01 => RegCRFunction::DataNoPostInc,
            0b10 => RegCRFunction::DataPostIncRW,
            0b11 => RegCRFunction::DataPostIncWO,
            _ => unreachable!(),
        }
    }

    pub fn set_function(mut self, function: RegCRFunction) -> Self {
        self.0.set_bits(14..=15, function as u16);
        self
    }
}

impl PhyRegister for RegCR {
    fn addr() -> u8 {
        0xD
    }

    fn page() -> u8 {
        0
    }
}

impl From<u16> for RegCR {
    fn from(value: u16) -> Self {
        RegCR(value)
    }
}

impl Into<u16> for RegCR {
    fn into(self) -> u16 {
        self.0
    }
}
