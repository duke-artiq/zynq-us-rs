use super::ExtendedPhyRegister;
use bit_field::BitField;

#[derive(Clone, Copy, Debug)]
/// RGMII Control Register
pub struct RgmiiCtl(u16);

impl RgmiiCtl {
    pub fn rgmii_en(&self) -> bool {
        self.0.get_bit(7)
    }

    pub fn set_rgmii_en(mut self, en: bool) -> Self {
        self.0.set_bit(7, en);
        self
    }

    pub fn rgmii_tx_clk_delay(&self) -> bool {
        self.0.get_bit(1)
    }

    pub fn set_rgmii_tx_clk_delay(mut self, en: bool) -> Self {
        self.0.set_bit(1, en);
        self
    }

    pub fn rgmii_rx_clk_delay(&self) -> bool {
        self.0.get_bit(0)
    }

    pub fn set_rgmii_rx_clk_delay(mut self, en: bool) -> Self {
        self.0.set_bit(0, en);
        self
    }
}

impl ExtendedPhyRegister for RgmiiCtl {
    fn addr() -> u16 {
        0x32
    }
}

impl From<u16> for RgmiiCtl {
    fn from(value: u16) -> Self {
        RgmiiCtl(value)
    }
}

impl Into<u16> for RgmiiCtl {
    fn into(self) -> u16 {
        self.0
    }
}
