use super::ExtendedPhyRegister;
use bit_field::BitField;

#[derive(Clone, Copy, Debug)]
/// Configuration Register 4
pub struct Cfg4(u16);

impl Cfg4 {
    pub fn int_tst_mode_1(&self) -> bool {
        self.0.get_bit(7)
    }

    pub fn set_int_tst_mode_1(mut self, mode: bool) -> Self {
        self.0.set_bit(7, mode);
        self
    }

    pub fn port_mirror_en(&self) -> bool {
        self.0.get_bit(0)
    }

    pub fn set_port_mirror_en(mut self, en: bool) -> Self {
        self.0.set_bit(0, en);
        self
    }
}

impl ExtendedPhyRegister for Cfg4 {
    fn addr() -> u16 {
        0x31
    }
}

impl From<u16> for Cfg4 {
    fn from(value: u16) -> Self {
        Cfg4(value)
    }
}

impl Into<u16> for Cfg4 {
    fn into(self) -> u16 {
        self.0
    }
}
