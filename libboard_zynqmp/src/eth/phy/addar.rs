use super::PhyRegister;

#[derive(Clone, Copy, Debug)]
pub struct AdDaR(u16);

impl AdDaR {
    pub fn addr_or_data(&self) -> u16 {
        self.0
    }

    pub fn set_addr_or_data(mut self, addr_or_data: u16) -> Self {
        self.0 = addr_or_data;
        self
    }
}

impl PhyRegister for AdDaR {
    fn addr() -> u8 {
        0xE
    }

    fn page() -> u8 {
        0
    }
}

impl From<u16> for AdDaR {
    fn from(value: u16) -> Self {
        AdDaR(value)
    }
}

impl Into<u16> for AdDaR {
    fn into(self) -> u16 {
        self.0
    }
}
