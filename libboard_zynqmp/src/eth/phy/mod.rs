//! Copied from https://git.m-labs.hk/M-labs/zynq-rs
//! Commit: be672ab
//! Modifications:
//! - added extended registers and traits to access
//! - removed LED stuff
pub mod id;
use id::{identify_phy, PhyIdentifier};
mod status;
use log::debug;
pub use status::Status;
mod control;
pub use control::Control;
mod pssr;
pub use pssr::PSSR;
mod regcr;
pub use regcr::RegCR;
mod addar;
pub use addar::AdDaR;
mod cfg4;
pub use cfg4::Cfg4;
mod rgmiictl;
pub use rgmiictl::RgmiiCtl;
mod rgmiidctl;
pub use rgmiidctl::RgmiiDCtl;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Link {
    pub speed: LinkSpeed,
    pub duplex: LinkDuplex,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LinkSpeed {
    S10,
    S100,
    S1000,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LinkDuplex {
    Half,
    Full,
}

#[derive(Clone, Copy, Debug)]
pub enum RegCRFunction {
    Address = 0b00,
    DataNoPostInc = 0b01,
    DataPostIncRW = 0b10,
    DataPostIncWO = 0b11,
}

pub trait PhyAccess {
    fn read_phy(&mut self, addr: u8, reg: u8) -> u16;
    fn write_phy(&mut self, addr: u8, reg: u8, data: u16);
}

pub trait ExtendedPhyAccess: PhyAccess {
    fn read_phy_extended(&mut self, addr: u8, devad: u16, reg: u16) -> u16;
    fn write_phy_extended(&mut self, addr: u8, devad: u16, reg: u16, data: u16);
}

pub trait PhyRegister {
    fn addr() -> u8;
    fn page() -> u8;
}

pub trait ExtendedPhyRegister {
    fn addr() -> u16;
}

#[derive(Clone)]
pub struct Phy {
    pub addr: u8,
}

const OUI_TI: u32 = 0x080028;
const DEVAD_TI: u16 = 0x1F;

impl Phy {
    /// Probe all addresses on MDIO for a known PHY
    pub fn find<PA: PhyAccess>(pa: &mut PA) -> Option<Phy> {
        (0..32)
            .find(|addr| {
                match identify_phy(pa, *addr) {
                    Some(PhyIdentifier {
                        oui: OUI_TI,
                        // TI DP83867
                        model: 35,
                        ..
                    }) => true,
                    _ => false,
                }
            })
            .map(|addr| {
                debug!("found phy at {addr}");
                Phy { addr }
            })
    }

    pub fn read_reg<PA, PR>(&self, pa: &mut PA) -> PR
    where
        PA: PhyAccess,
        PR: PhyRegister + From<u16>,
    {
        pa.read_phy(self.addr, PR::addr()).into()
    }

    pub fn read_reg_extended<EPA, EPR>(&self, epa: &mut EPA) -> EPR
    where
        EPA: ExtendedPhyAccess,
        EPR: ExtendedPhyRegister + From<u16>,
    {
        epa.read_phy_extended(self.addr, DEVAD_TI, EPR::addr())
            .into()
    }

    pub fn modify_reg<PA, PR, F>(&self, pa: &mut PA, mut f: F)
    where
        PA: PhyAccess,
        PR: PhyRegister + From<u16> + Into<u16>,
        F: FnMut(PR) -> PR,
    {
        let reg = pa.read_phy(self.addr, PR::addr()).into();
        let reg = f(reg);
        pa.write_phy(self.addr, PR::addr(), reg.into())
    }

    pub fn modify_reg_extended<EPA, EPR, F>(&self, epa: &mut EPA, mut f: F)
    where
        EPA: ExtendedPhyAccess,
        EPR: ExtendedPhyRegister + From<u16> + Into<u16>,
        F: FnMut(EPR) -> EPR,
    {
        let reg = epa
            .read_phy_extended(self.addr, DEVAD_TI, EPR::addr())
            .into();
        let reg = f(reg);
        epa.write_phy_extended(self.addr, DEVAD_TI, EPR::addr(), reg.into())
    }

    pub fn modify_control<PA, F>(&self, pa: &mut PA, f: F)
    where
        PA: PhyAccess,
        F: FnMut(Control) -> Control,
    {
        self.modify_reg(pa, f)
    }

    pub fn modify_rgmiictl<EPA, F>(&self, epa: &mut EPA, f: F)
    where
        EPA: ExtendedPhyAccess,
        F: FnMut(RgmiiCtl) -> RgmiiCtl,
    {
        self.modify_reg_extended(epa, f)
    }

    pub fn modify_rgmiidctl<EPA, F>(&self, epa: &mut EPA, f: F)
    where
        EPA: ExtendedPhyAccess,
        F: FnMut(RgmiiDCtl) -> RgmiiDCtl,
    {
        self.modify_reg_extended(epa, f)
    }

    pub fn modify_cfg4<EPA, F>(&self, epa: &mut EPA, f: F)
    where
        EPA: ExtendedPhyAccess,
        F: FnMut(Cfg4) -> Cfg4,
    {
        self.modify_reg_extended(epa, f)
    }

    pub fn get_control<PA: PhyAccess>(&self, pa: &mut PA) -> Control {
        self.read_reg(pa)
    }

    pub fn get_status<PA: PhyAccess>(&self, pa: &mut PA) -> Status {
        self.read_reg(pa)
    }

    pub fn get_link<PA: PhyAccess>(&self, pa: &mut PA) -> Option<Link> {
        let status = self.get_status(pa);
        if !status.link_status() {
            None
        } else if status.cap_1000base_t_extended_status() {
            let phy_status: PSSR = self.read_reg(pa);
            phy_status.get_link()
        } else {
            status.get_link()
        }
    }

    pub fn reset<PA: PhyAccess>(&self, pa: &mut PA) {
        self.modify_control(pa, |control| control.set_reset(true));
        while self.get_control(pa).reset() {}
    }

    pub fn restart_autoneg<PA: PhyAccess>(&self, pa: &mut PA) {
        self.modify_control(pa, |control| {
            control.set_autoneg_enable(true).set_restart_autoneg(true)
        });
    }
}
