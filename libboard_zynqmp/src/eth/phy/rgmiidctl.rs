use super::ExtendedPhyRegister;
use bit_field::BitField;

#[derive(Clone, Copy, Debug)]
/// RGMII Control Register
pub struct RgmiiDCtl(u16);

impl RgmiiDCtl {
    pub fn rgmii_tx_delay_ctrl(&self) -> u16 {
        self.0.get_bits(4..=7)
    }

    pub fn set_rgmii_tx_delay_ctrl(mut self, delay: u16) -> Self {
        self.0.set_bits(4..=7, delay);
        self
    }

    pub fn rgmii_rx_delay_ctrl(&self) -> u16 {
        self.0.get_bits(0..=3)
    }

    pub fn set_rgmii_rx_delay_ctrl(mut self, delay: u16) -> Self {
        self.0.set_bits(0..=3, delay);
        self
    }
}

impl ExtendedPhyRegister for RgmiiDCtl {
    fn addr() -> u16 {
        0x86
    }
}

impl From<u16> for RgmiiDCtl {
    fn from(value: u16) -> Self {
        RgmiiDCtl(value)
    }
}

impl Into<u16> for RgmiiDCtl {
    fn into(self) -> u16 {
        self.0
    }
}
