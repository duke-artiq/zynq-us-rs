// Copied from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: be672ab
// Modifications:
// - minor changes for cortex a9 -> a53
// - add dummy descriptor to tie off q1
use super::Buffer;
use alloc::{vec, vec::Vec};
use core::ops::Deref;
use libcortex_a53::{
    asm::dmb_sys,
    cache::{dcc_poc, dcci_slice_poc, dci_slice_poc, LINELEN},
    uncached::UncachedSlice,
};
use libregister::*;

#[derive(Debug)]
pub enum Error {
    HrespNotOk,
    RxOverrun,
    BufferNotAvail,
    Truncated,
}

/// Descriptor entry
#[repr(C, align(0x10))]
pub struct DescEntry {
    word0: DescWord0,
    word1: DescWord1,
    word2: DescWord2,
    word3: DescWord3,
}

impl DescEntry {
    pub fn zeroed() -> Self {
        DescEntry {
            word0: DescWord0 {
                inner: VolatileCell::new(0),
            },
            word1: DescWord1 {
                inner: VolatileCell::new(0),
            },
            word2: DescWord2 {
                inner: VolatileCell::new(0),
            },
            word3: DescWord3 {
                inner: VolatileCell::new(0),
            },
        }
    }
}

register!(desc_word0, DescWord0, VolatileCell, u32);
register_bit!(
    desc_word0,
    /// true if owned by software, false if owned by hardware
    used,
    0
);
register_bit!(
    desc_word0,
    /// mark last desc in list
    wrap,
    1
);
register_bits!(desc_word0, address_lsbs, u32, 2, 31);

register!(desc_word1, DescWord1, VolatileCell, u32);
register_bits!(desc_word1, frame_length_lsbs, u16, 0, 12);
register_bit!(desc_word1, bad_fcs, 13);
register_bit!(desc_word1, start_of_frame, 14);
register_bit!(desc_word1, end_of_frame, 15);
register_bit!(desc_word1, cfi, 16);
register_bits!(desc_word1, vlan_priority, u8, 17, 19);
register_bit!(desc_word1, priority_tag, 20);
register_bit!(desc_word1, vlan_tag, 21);
register_bits!(desc_word1, bits_22_23, u8, 22, 23);
register_bit!(desc_word1, bit_24, 24);
register_bits!(desc_word1, spec_addr_which, u8, 25, 26);
register_bit!(desc_word1, spec_addr_match, 27);
register_bit!(desc_word1, uni_hash_match, 29);
register_bit!(desc_word1, multi_hash_match, 30);
register_bit!(desc_word1, global_broadcast, 31);

register!(desc_word2, DescWord2, VolatileCell, u32);
register_bits!(desc_word2, address_msbs, u32, 0, 31);

// unused
register!(desc_word3, DescWord3, VolatileCell, u32);

#[repr(C)]
pub struct DescList {
    list: UncachedSlice<DescEntry>,
    dummy_descriptor: DescEntry,
    buffers: Vec<Buffer>,
    next: usize,
}

impl DescList {
    pub fn new(size: usize) -> Self {
        let mut list = UncachedSlice::new(size, || DescEntry::zeroed()).unwrap();
        let mut buffers = vec![Buffer::new(); size];

        let last = list.len().min(buffers.len()) - 1;
        for (i, (entry, buffer)) in list.iter_mut().zip(buffers.iter_mut()).enumerate() {
            let is_last = i == last;
            let buffer_addr = &mut buffer.0[0] as *mut _ as u64;
            assert!(buffer_addr & 0b11 == 0);
            let address_lsbs = (buffer_addr & 0xffff_ffff) as u32;
            let address_msbs = (buffer_addr >> 32) as u32;
            entry.word0.write(
                DescWord0::zeroed()
                    .used(false)
                    .wrap(is_last)
                    .address_lsbs(address_lsbs >> 2),
            );
            entry.word1.write(DescWord1::zeroed());
            entry
                .word2
                .write(DescWord2::zeroed().address_msbs(address_msbs));
            dcci_slice_poc(&buffer[..]);
        }

        // dummy descriptor to "tie off" q1
        // doesn't need to be uncached as it should never be modified
        let mut dummy_descriptor = DescEntry::zeroed();
        dummy_descriptor
            .word0
            .write(DescWord0::zeroed().used(true).wrap(true));
        dcc_poc(&dummy_descriptor);

        DescList {
            list,
            dummy_descriptor,
            buffers,
            next: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.list.len().min(self.buffers.len())
    }

    pub fn list_addr(&self) -> u64 {
        &self.list[0] as *const _ as u64
    }

    pub fn dummy_desc_addr(&self) -> u32 {
        &self.dummy_descriptor as *const _ as u32
    }

    pub fn recv_next<'s: 'p, 'p>(&'s mut self) -> Result<Option<PktRef<'p>>, Error> {
        let list_len = self.list.len();
        let entry = &mut self.list[self.next];
        dmb_sys();
        if entry.word0.read().used() {
            let word1 = entry.word1.read();
            let len = word1.frame_length_lsbs().into();
            if len == 0 {
                self.next += 1;
                if self.next >= list_len {
                    self.next = 0;
                }
                entry.word0.modify(|_, w| w.used(false));
                return Ok(None);
            }
            let padding = {
                let diff = len % LINELEN;
                if diff == 0 {
                    0
                } else {
                    LINELEN - diff
                }
            };
            // invalidate the buffer
            // we cannot do it in the drop function, as L2 cache data prefetch would prefetch
            // the data, and there is no way for us to prevent that unless changing MMU table.
            dci_slice_poc(&mut self.buffers[self.next][0..len + padding]);
            let buffer = &mut self.buffers[self.next][0..len];

            self.next += 1;
            if self.next >= list_len {
                self.next = 0;
            }

            let pkt = PktRef { entry, buffer };
            if word1.start_of_frame() && word1.end_of_frame() {
                Ok(Some(pkt))
            } else {
                Err(Error::Truncated)
            }
        } else {
            Ok(None)
        }
    }
}

/// Releases a buffer back to the HW upon Drop
pub struct PktRef<'a> {
    entry: &'a mut DescEntry,
    buffer: &'a mut [u8],
}

impl<'a> Drop for PktRef<'a> {
    fn drop(&mut self) {
        self.entry.word0.modify(|_, w| w.used(false));
        dmb_sys();
    }
}

impl<'a> Deref for PktRef<'a> {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        self.buffer
    }
}

impl<'a> smoltcp::phy::RxToken for PktRef<'a> {
    fn consume<R, F>(self, f: F) -> R
    where
        F: FnOnce(&mut [u8]) -> R,
    {
        f(self.buffer)
    }
}
