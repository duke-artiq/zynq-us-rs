//! APU Interrupts - GIC400

use libregister::{
    register, register_at, register_bit, register_bits, register_bits_typed, RegisterRW, RegisterW,
};
use volatile_register::{RO, RW, WO};

/// GIC distributor registers
#[repr(C)]
pub struct GICD {
    pub ctrl: GICDCtlr,
    pub type_r: GICDTypeR,
    pub iid: RO<u32>,
    unused0: [u32; 29],
    pub group: [RW<u32>; 6],
    unused1: [u32; 26],
    pub set_enable: [RW<u32>; 6],
    unused2: [u32; 26],
    pub clear_enable: [RW<u32>; 6],
    unused3: [u32; 26],
    pub set_pending: [RW<u32>; 6],
    unused4: [u32; 26],
    pub clear_pending: [RW<u32>; 6],
    unused5: [u32; 26],
    pub set_active: [RW<u32>; 6],
    unused6: [u32; 26],
    pub clear_active: [RW<u32>; 6],
    unused7: [u32; 26],
    pub priority: [GICDIPriorityRn; 48],
    unused8: [u32; 208],
    // GICv2 spec: "In a multiprocessor implementation, GICD_ITARGETSR0 to GICD_ITARGETSR7 are
    // banked for each connected processor. These registers hold the CPU targets fields for
    // interrupts 0-31"
    pub target_0_7: [RO<u32>; 8],
    pub target_8_47: [RW<u32>; 40],
    unused9: [u32; 208],
    pub config_sgi: RO<u32>,
    pub config_ppi: RO<u32>,
    pub config_spi: [RW<u32>; 10],
    unused10: [u32; 52],
    pub ppi_status: RO<u32>,
    pub spi_status: [RO<u32>; 5],
    unused11: [u32; 122],
    pub sgi: SGI,
    unused12: [u32; 3],
    pub sgi_clear_pending: [RW<u32>; 4],
    pub sgi_set_pending: [RW<u32>; 4],
    unused13: [u32; 40],
    pub pid4: RO<u32>,
    pub pid5: RO<u32>,
    pub pid6: RO<u32>,
    pub pid7: RO<u32>,
    pub pid0: RO<u32>,
    pub pid1: RO<u32>,
    pub pid2: RO<u32>,
    pub pid3: RO<u32>,
    pub cid: [RO<u32>; 4],
}

register_at!(GICD, 0xF901_0000, gicd);

register!(gicd_ctlr, GICDCtlr, RW, u32);
// secure bit assignments
register_bit!(gicd_ctlr, enable_grp1, 1);
register_bit!(gicd_ctlr, enable_grp0, 0);

register!(gicd_typer, GICDTypeR, RO, u32);
register_bits!(gicd_typer, lspi, u8, 11, 15);
register_bit!(gicd_typer, security_extn, 10);
register_bits!(gicd_typer, cpu_number, u8, 5, 7);
register_bits!(gicd_typer, it_lines_number, u8, 0, 4);

register!(gicd_ipriorityrn, GICDIPriorityRn, RW, u32);
// not specifying each byte to simplify `set_priority` logic
register_bits!(gicd_ipriorityrn, bytes, u32, 0, 31);

#[repr(u8)]
pub enum SGITargetListFilter {
    /// Forward to CPUs specified by cpu_target_list
    UseTargetList = 0b00,
    /// Forward to all CPUs except the one that requested the interrupt
    AllButRequester = 0b01,
    /// Forward to CPU that requested the interrupt
    OnlyRequester = 0b10,
    _Reserved = 0b11,
}

register!(sgi, SGI, WO, u32);
register_bits_typed!(sgi, target_list_filter, u8, SGITargetListFilter, 24, 25);
// one-hot encoding
register_bits!(sgi, cpu_target_list, u8, 16, 23);
register_bit!(sgi, nsatt, 15);
register_bits!(sgi, sgi_int_id, u8, 0, 3);

/// GIC CPU interface registers
#[repr(C)]
pub struct GICC {
    pub ctrl: GICCCtlr,
    pub prio_mask: GICCPMR,
    pub binary_point: RW<u32>,
    pub interrupt_ack: GICCIAR,
    pub end_of_interrupt: GICCEOIR,
    pub running_prio: RO<u32>,
    pub highest_prio_pending: RO<u32>,
    pub aliased_binary_point: RW<u32>,
    pub aliased_interrupt_ack: RO<u32>,
    pub aliased_end_of_interrupt: WO<u32>,
    pub aliased_highest_prio_pending: RO<u32>,
    unused0: [u32; 41],
    pub active_prio: RW<u32>,
    unused1: [u32; 3],
    pub nonsecure_active_prio: RW<u32>,
    unused2: [u32; 6],
    pub iid: RO<u32>,
}

register_at!(GICC, 0xF902_0000, gicc);

register!(gicc_ctlr, GICCCtlr, RW, u32);
// secure bit assignments
register_bit!(gicc_ctlr, eoi_mode_ns, 10);
register_bit!(gicc_ctlr, eoi_mode_s, 9);
register_bit!(gicc_ctlr, irq_byp_dis_grp1, 8);
register_bit!(gicc_ctlr, fiq_byp_dis_grp1, 7);
register_bit!(gicc_ctlr, irq_byp_dis_grp0, 6);
register_bit!(gicc_ctlr, fiq_byp_dis_grp0, 5);
register_bit!(gicc_ctlr, cbpr, 4);
register_bit!(gicc_ctlr, fiq_en, 3);
// GIC spec:
// "ARM deprecates use of GICC_CTLR.AckCtl, and strongly recommends
// using a software model where GICC_CTLR.AckCtl is set to 0."
register_bit!(gicc_ctlr, ack_ctl, 2);
register_bit!(gicc_ctlr, enable_grp1, 1);
register_bit!(gicc_ctlr, enable_grp0, 0);

register!(gicc_pmr, GICCPMR, RW, u32);
register_bits!(gicc_pmr, priority, u8, 0, 7);

register!(gicc_iar, GICCIAR, RO, u32);
register_bits!(gicc_iar, cpu_id, u8, 10, 12);
register_bits!(gicc_iar, interrupt_id, u16, 0, 9);

register!(gicc_eoir, GICCEOIR, WO, u32);
register_bits!(gicc_eoir, cpu_id, u8, 10, 12);
register_bits!(gicc_eoir, interrupt_id, u16, 0, 9);

pub struct GIC {
    pub gicd: &'static mut GICD,
    pub gicc: &'static mut GICC,
}

// Not sure if documented in UG1085. Figured out the value by writing 0xff to a priority register and reading
// it back (unused priority bits are RES0).
pub const NUM_PRIORITY_BITS: u8 = 5;
/// The maximum *value* a priority field can have, which is the *lowest* priority.
pub const MAX_PRIORITY: u8 = 31;

// TODO: type safe implementation of SGI targets as well as support for the other target list
// filter values.
// TODO: protect GICD registers/operations with a mutex. GICC registers are banked per core so those are fine as is.
impl GIC {
    pub fn gic() -> Self {
        GIC {
            gicd: GICD::gicd(),
            gicc: GICC::gicc(),
        }
    }

    /// Clear all pending interrupts.
    ///
    /// Writes to GICD only.
    pub fn clear_pending(&mut self) {
        for icenabler in &self.gicd.clear_enable {
            unsafe {
                icenabler.write(0xffff_ffff);
            }
        }
        for icpendr in &self.gicd.clear_pending {
            unsafe {
                icpendr.write(0xffff_ffff);
            }
        }
        for icactiver in &self.gicd.clear_active {
            unsafe {
                icactiver.write(0xffff_ffff);
            }
        }
        for cpendsgir in &self.gicd.sgi_clear_pending {
            unsafe {
                cpendsgir.write(0xffff_ffff);
            }
        }
    }

    /// Enable interrupts for the current core (writes GICC only).
    pub fn enable_interrupts(&mut self) {
        self.gicc
            .ctrl
            .modify(|_, w| w.enable_grp1(true).enable_grp0(true));
    }

    /// Enable interrupts to all cores (writes GICD only).
    pub fn global_enable_interrupts(&mut self) {
        self.gicd
            .ctrl
            .modify(|_, w| w.enable_grp1(true).enable_grp0(true));
    }

    /// Disable interrupts for the current core (writes GICC only).
    pub fn disable_interrupts(&mut self) {
        self.gicc
            .ctrl
            .modify(|_, w| w.enable_grp1(false).enable_grp0(false));
    }

    /// Disable interrupts to all cores (writes GICD only).
    pub fn global_disable_interrupts(&mut self) {
        self.gicd
            .ctrl
            .modify(|_, w| w.enable_grp1(false).enable_grp0(false));
    }

    /// Send a software-generated interrupt.
    pub fn send_sgi(&mut self, targets: u8, iid: u8) {
        assert!(iid < 16, "Interrupt ID for SGIs must be in the range [0, 15]");
        self.gicd.sgi.write(
            SGI::zeroed()
                .target_list_filter(SGITargetListFilter::UseTargetList)
                .cpu_target_list(targets)
                .sgi_int_id(iid),
        );
    }

    /// Set the priority `priority` of interrupt with ID `iid`.
    ///
    /// Applies to all cores (writes GICD).
    ///
    /// **Note that lower values correspond to higher priorities -
    /// i.e. 0 is the highest priority and 31 is the lowest.**
    pub fn set_priority(&mut self, iid: u16, priority: u8) {
        assert!(
            priority <= MAX_PRIORITY,
            "Priority must be in the range [0, {}]",
            MAX_PRIORITY
        );
        let n = iid / 4;
        let byte_offset = 8 * (iid % 4) as u8;
        let byte_mask = 0xff << byte_offset;
        self.gicd.priority[n as usize].modify(|r, w| {
            w.bytes(
                (r.bytes() & !byte_mask)
                    | ((priority as u32) << (byte_offset + (8 - NUM_PRIORITY_BITS))),
            )
        });
    }

    /// Set the minimum priority for interrupts forwarded to the current core.
    ///
    /// Writes to GICC only.
    ///
    /// **Note that the same principle governing priorities applies to the priority mask - a mask
    /// of 31 allows interrupts with priority 0-30 and a mask of 0 effectively disables all
    /// interrupts.**
    pub fn set_priority_mask(&mut self, priority: u8) {
        assert!(
            priority <= MAX_PRIORITY,
            "Priority mask must be in the range [0, {}]",
            MAX_PRIORITY
        );
        self.gicc
            .prio_mask
            .write(GICCPMR::zeroed().priority(priority << (8 - NUM_PRIORITY_BITS)));
    }
}
