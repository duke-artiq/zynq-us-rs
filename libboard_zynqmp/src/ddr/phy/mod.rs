use libm::ceilf;
use libregister::{RegisterR, RegisterRW, RegisterW};

use regs::{
    pll_cr0, DX8SLnDQSCTL, DX8SLnDXCTL2, DX8SLnIOCR, DX8SLnOSC, DXnGCR0, DXnGCR1, DXnGCR2, DXnGCR3,
    DXnGCR4, DXnGCR5, DXnGCR6, MRn, ModeCtl, RegisterBlock, ZQnOR0, ZQnOR1, ZQnPR0, ACBDLR1,
    ACBDLR2, ACBDLR6, ACBDLR7, ACBDLR8, ACBDLR9, ACIOCR0, ACIOCR2, ACIOCR3, ACIOCR4, BISTLSR,
    CATR0, DCR, DQSDR0, DSGCR, DTAR0, DTAR1, DTAR2, DTCR0, DTCR1, DTDR0, DTDR1, DTPR0, DTPR1,
    DTPR2, DTPR3, DTPR4, DTPR5, DTPR6, DX8_BYTES, DX8_PLLS, GPR1, IOVCR0, PGCR0, PGCR2, PGCR3,
    PGCR5, PIR, PLLCR0, PTR0, PTR1, RDIMMCR0, RDIMMCR1, RDIMMCR2,
    RDIMMCR3, RDIMMCR4, RDIMMGCR0, RDIMMGCR1, RIOCR5, VTCR0, VTCR1, ZQCR,
};

use super::spd::{DeviceType, FineGranularityRefMode, GeneralConfig, ModuleConfig};

pub mod regs;

pub struct DdrPhy {
    pub regs: &'static mut RegisterBlock,
}

pub const DEFAULT_TIMEOUT: u32 = 1_000_000; // arbitrary

impl DdrPhy {
    pub fn ddr_phy() -> Self {
        let self_ = DdrPhy {
            regs: RegisterBlock::ddr_phy(),
        };
        self_
    }

    /// Settings for AC and data PLLs
    fn pll_cr0(ctl_clock_mhz: u32) -> pll_cr0::Write {
        let frq_sel: u8;
        let cppc: u8;
        let cpic: u8;
        // values from UG1087 (e.g. DDR_PHY::PLLCR0)
        if ctl_clock_mhz > 668 {
            frq_sel = 0b1000;
            cppc = 0b000101;
            cpic = 0b0000;
        } else if ctl_clock_mhz > 560 {
            frq_sel = 0b0000;
            cppc = 0b000111;
            cpic = 0b0000;
        } else if ctl_clock_mhz > 471 {
            frq_sel = 0b0001;
            cppc = 0b001000;
            cpic = 0b0000;
        } else if ctl_clock_mhz > 396 {
            frq_sel = 0b0010;
            cppc = 0b001001;
            cpic = 0b0000;
        } else if ctl_clock_mhz > 332 {
            frq_sel = 0b0011;
            cppc = 0b001010;
            cpic = 0b0000;
        } else if ctl_clock_mhz > 279 {
            frq_sel = 0b0100;
            cppc = 0b000110;
            cpic = 0b0001;
        } else if ctl_clock_mhz > 235 {
            frq_sel = 0b0101;
            cppc = 0b001000;
            cpic = 0b0001;
        } else if ctl_clock_mhz > 197 {
            frq_sel = 0b0110;
            cppc = 0b001001;
            cpic = 0b0010
        } else if ctl_clock_mhz >= 166 {
            frq_sel = 0b0111;
            cppc = 0b001010;
            cpic = 0b0011;
        } else {
            panic!("Control clock frequency must be at least 166 MHz")
        }

        PLLCR0::zeroed().frq_sel(frq_sel).cppc(cppc).cpic(cpic)
    }

    /// Data lane and PLL configuration
    fn datx8_config(&mut self, config: &GeneralConfig) {
        // DDRC programming has already ensured the data bus width is valid
        let en_lanes = config.bus_width as usize / 8;
        let dxn_en: [bool; DX8_BYTES] =
            core::array::from_fn(|i| i < en_lanes || (config.ecc_en && i == DX8_BYTES - 1));

        for i in 0..DX8_BYTES {
            let dxn_regs = &mut self.regs.dxn_regs[i];
            if dxn_en[i] {
                let dqsg_pdr = i == DX8_BYTES - 1; // true for ecc byte
                dxn_regs.dxn_gcr0.write(
                    DXnGCR0::zeroed()
                        .mdl_en(true)
                        .rd_dly(0x8)
                        .rtt_oh(0x3)
                        .dqsg_pdr(dqsg_pdr)
                        .dqsg_oe(true),
                );

                dxn_regs.dxn_gcr1.write(
                    DXnGCR1::zeroed()
                        .qsn_sel(true)
                        .qs_sel(true)
                        .oe_en(true)
                        .pdr_en(true)
                        .te_en(true)
                        .ds_en(true)
                        .dm_en(config.dm_en || config.rd_dbi_en || config.wr_dbi_en)
                        .dq_en(0xff),
                );

                dxn_regs.dxn_gcr2.write(DXnGCR2::zeroed());

                let dm_oe_mode: ModeCtl;
                let dm_te_mode: ModeCtl;
                let dm_pdr_mode: ModeCtl;
                if config.dm_en || config.rd_dbi_en || config.wr_dbi_en {
                    dm_oe_mode = ModeCtl::Dynamic;
                    dm_te_mode = ModeCtl::Dynamic;
                    dm_pdr_mode = ModeCtl::Dynamic;
                } else {
                    dm_oe_mode = ModeCtl::AlwaysOff;
                    dm_te_mode = ModeCtl::AlwaysOff;
                    dm_pdr_mode = ModeCtl::AlwaysOn;
                }
                dxn_regs.dxn_gcr3.write(
                    DXnGCR3::zeroed()
                        .rd_bvt(true)
                        .wd_bvt(true)
                        .rg_lvt(true)
                        .wd_lvt(true)
                        .rd_lvt(true)
                        .wd_lvt(true)
                        .wl_lvt(true)
                        .dm_oe_mode(dm_oe_mode)
                        .dm_te_mode(dm_te_mode)
                        .dm_pdr_mode(dm_pdr_mode)
                        .ds_pdr_mode(ModeCtl::AlwaysOff),
                );

                let refi_en = match i {
                    4 | 6 | 8 => {
                        if config.rank_addr_bits() == 1 {
                            0x3
                        } else {
                            0x1
                        }
                    }
                    _ => 0xf,
                };
                dxn_regs.dxn_gcr4.write(
                    DXnGCR4::zeroed()
                        .dx_refe_en(0x3)
                        .dx_refs_en(true)
                        .dx_refs_sel_range(true)
                        .dx_refs_sel(0x30)
                        .dx_refi_en(refi_en),
                );
            } else {
                dxn_regs.dxn_gcr0.write(
                    DXnGCR0::zeroed()
                        .cal_byp(true)
                        .rd_dly(0x8)
                        .dqsnse_pdr(true)
                        .dqsse_pdr(true)
                        .rtt_oh(0x3)
                        .dqsr_pd(true)
                        .dqsg_pdr(true),
                );

                dxn_regs.dxn_gcr1.write(
                    DXnGCR1::zeroed()
                        .dx_pdr_mode(0x5555)
                        .qsn_sel(true)
                        .qs_sel(true),
                );

                dxn_regs
                    .dxn_gcr2
                    .write(DXnGCR2::zeroed().dx_oe_mode(0xaaaa).dx_te_mode(0xaaaa));

                dxn_regs.dxn_gcr3.write(
                    DXnGCR3::zeroed()
                        .dsn_oe_mode(ModeCtl::AlwaysOff)
                        .dsn_te_mode(ModeCtl::AlwaysOff)
                        .dsn_pdr_mode(ModeCtl::AlwaysOn)
                        .dm_oe_mode(ModeCtl::AlwaysOff)
                        .dm_te_mode(ModeCtl::AlwaysOff)
                        .dm_pdr_mode(ModeCtl::AlwaysOn)
                        .ds_oe_mode(ModeCtl::AlwaysOff)
                        .ds_te_mode(ModeCtl::AlwaysOff)
                        .ds_pdr_mode(ModeCtl::AlwaysOn),
                );

                dxn_regs.dxn_gcr4.write(
                    DXnGCR4::zeroed()
                        .dx_refe_en(0x3)
                        .dx_refs_sel_range(true)
                        .dx_refs_sel(0x30),
                );
            }
        }

        // data PLLs
        let en_plls = en_lanes / 2;
        let dx8_pll_en: [bool; DX8_PLLS] =
            core::array::from_fn(|i| i < en_plls || (config.ecc_en && i == DX8_PLLS - 1));

        let ctl_clock_mhz = config.ctl_clock_mhz();
        let pos_oex = 0b000; // ddr4 postamble is always 1/2 t_ck
        let pre_oex = if config.wr_2ck_preamble { 0b11 } else { 0b01 };
        for i in 0..DX8_PLLS {
            let dx8_sln_regs = &mut self.regs.dx8_sln_regs[i];
            let en = dx8_pll_en[i];

            let gate_clk = if en {
                ModeCtl::AlwaysOff
            } else {
                ModeCtl::AlwaysOn
            };
            dx8_sln_regs.dx8_sln_osc.write(
                DX8SLnOSC::zeroed()
                    .gate_dx_rd_clk(gate_clk)
                    .gate_dx_ddr_clk(gate_clk)
                    .gate_dx_ctl_clk(gate_clk)
                    .phy_hrst(true)
                    .phy_frst(true)
                    .res_11_12(0x3)
                    .osc_wd_dl(0x3)
                    .res_7_8(0x3)
                    .osc_w_dl(0x3)
                    .osc_div(0xf),
            );

            dx8_sln_regs
                .dx8_sln_pll_cr0
                .write(Self::pll_cr0(ctl_clock_mhz).pll_pd(!en));

            dx8_sln_regs.dx8_sln_dqs_ctl.write(
                DX8SLnDQSCTL::zeroed()
                    .rrr_mode(true)
                    .wrr_mode(true)
                    .lp_pll_pd(true)
                    .lp_io_pd(true)
                    .qs_cnt_en(true)
                    .udq_iom(!en)
                    .dx_sr(0x3),
            );

            dx8_sln_regs.dx8_sln_dx_ctl2.write(
                DX8SLnDXCTL2::zeroed()
                    .pos_oex(pos_oex)
                    .pre_oex(pre_oex)
                    .lp_wakeup_thrsh(0xc)
                    .rdbi(config.rd_dbi_en)
                    .wdbi(config.wr_dbi_en),
            );

            let dx_iom = if en {
                match config.device_type {
                    DeviceType::Ddr3 => 0b000,
                    DeviceType::LpDdr3 | DeviceType::Ddr4 => 0b010,
                    DeviceType::LpDdr4 => 0b100,
                }
            } else {
                0b001
            };

            dx8_sln_regs
                .dx8_sln_io_cr
                .write(DX8SLnIOCR::zeroed().dx_dac_range(0x7).dx_iom(dx_iom));
        }
    }

    /// Address/command bus and PLL configuration
    fn ac_config(&mut self, config: &GeneralConfig) {
        self.regs
            .pgcr0
            .write(PGCR0::zeroed().phy_frst(true).osc_acdl(0x3).osc_div(0xf));

        let t_refi_nck = config.ps_to_nck(config.t_refi_ps());
        let t_ref_prd = t_refi_nck
            / (match config.fine_granularity_ref_mode {
                FineGranularityRefMode::X1 => 1,
                FineGranularityRefMode::X2 => 2,
                FineGranularityRefMode::X4 => 4,
            })
            - (match config.device_type {
                DeviceType::Ddr4 | DeviceType::LpDdr4 => 800,
                DeviceType::Ddr3 | DeviceType::LpDdr3 => 400,
            });
        self.regs
            .pgcr2
            .write(PGCR2::zeroed().dtpm_xtmr(0xf).t_ref_prd(t_ref_prd));

        self.regs.pgcr3.write(
            PGCR3::zeroed()
                .ckn_en(0x55)
                .ck_en(0xaa)
                .gate_ac_rd_clk(ModeCtl::AlwaysOff)
                .gate_ac_ddr_clk(ModeCtl::AlwaysOff)
                .gate_ac_ctl_clk(ModeCtl::AlwaysOff)
                .ddl_bypmode(0b10),
        );

        self.regs.pgcr5.write(
            PGCR5::zeroed()
                .frq_bt(0x1)
                .frq_at(0x1)
                .vref_rbctrl(0xf)
                .dx_refi_sel_range(true),
        );

        // 1 us in ctl clock cycles
        let ctl_clock_period_ns = config.ctl_clock_period_ns();
        let t_pll_pd = ceilf(1_000.0 / ctl_clock_period_ns) as u16;
        // 4 us
        let t_pll_gs = ceilf(4_000.0 / ctl_clock_period_ns) as u16;
        self.regs.ptr0.write(
            PTR0::zeroed()
                .t_pll_pd(t_pll_pd)
                .t_pll_gs(t_pll_gs)
                .t_phy_rst(0x10),
        );

        // 100 us
        let t_pll_lock = ceilf(100_000.0 / ctl_clock_period_ns) as u16;
        // 9 us
        let t_pll_rst = ceilf(9_000.0 / ctl_clock_period_ns) as u16;
        self.regs
            .ptr1
            .write(PTR1::zeroed().t_pll_lock(t_pll_lock).t_pll_rst(t_pll_rst));

        self.regs
            .pll_cr0
            .write(Self::pll_cr0(config.ctl_clock_mhz()));
    }

    fn dram_config(&mut self, config: &GeneralConfig) {
        let puad = ceilf(10.0 / config.ctl_clock_period_ns()) as u8;
        self.regs.ds_gcr.write(
            DSGCR::zeroed()
                .rdbi_cl(0x2)
                .phy_zu_en(true)
                .rst_oe(true)
                .dto_pdr(true)
                .puad(puad)
                .cua_en(true)
                .pur_en(true),
        );

        self.regs.gpr1.write(GPR1::zeroed());

        let udimm = match config.module_config {
            ModuleConfig::Unbuffered(mod_config) => mod_config.rank_1_mirrored,
            _ => false,
        };
        let ddr_8bnk = config.bank_groups() * config.banks_per_group() == 8;
        let ddr_md = match config.device_type {
            DeviceType::LpDdr3 => 0b001,
            DeviceType::Ddr3 => 0b011,
            DeviceType::Ddr4 => 0b100,
            DeviceType::LpDdr4 => 0b101,
        };
        self.regs.dcr.write(
            DCR::zeroed()
                .udimm(udimm)
                .no_sra(true)
                .bytemask(0x1)
                .ddr_8bnk(ddr_8bnk)
                .ddr_md(ddr_md),
        );

        let t_rrd_nck = config.ps_to_nck(config.t_rrd_l_min_ps) as u8;
        let t_ras_nck = config.ps_to_nck(config.t_ras_min_ps) as u8;
        let t_rp_nck = config.ps_to_nck(config.t_rp_min_ps) as u8;
        self.regs.dtpr0.write(
            DTPR0::zeroed()
                .t_rrd(t_rrd_nck)
                .t_ras(t_ras_nck)
                .t_rp(t_rp_nck)
                .t_rtp(config.t_rtp_min_nck() as u8),
        );

        let t_faw_nck = config.ps_to_nck(config.t_faw_min_ps) as u8;
        let t_mod_nck = config.t_mod_min_nck();
        let t_mod = if t_mod_nck >= 24 && t_mod_nck <= 30 {
            t_mod_nck - 24
        } else {
            6
        } as u8;
        let t_mrd = (config.t_mrd_min_nck()
            - match config.device_type {
                DeviceType::Ddr3 => 4,
                DeviceType::LpDdr3 | DeviceType::Ddr4 => 8,
                DeviceType::LpDdr4 => 0,
            }) as u8;
        self.regs.dtpr1.write(
            DTPR1::zeroed()
                .t_wlm_rd(config.t_wlm_rd_min_nck() as u8)
                .t_faw(t_faw_nck)
                .t_mod(t_mod)
                .t_mrd(t_mrd),
        );

        let t_xs = (config.ns_to_nck(config.t_xs_min_ns()) as u32)
            .max(config.t_xs_dll_min_nck())
            .min(0x3ff) as u16;
        self.regs.dtpr2.write(
            DTPR2::zeroed()
                .t_cke(config.t_cke_min_nck() as u8)
                .t_xs(t_xs),
        );

        let t_dllk = (config.t_dllk_min_nck() - 128).min(0x3ff) as u16;
        self.regs
            .dtpr3
            .write(DTPR3::zeroed().t_ofd_x(0x4).t_dllk(t_dllk).t_dqsck_max(0x8));

        self.regs.dtpr4.write(
            DTPR4::zeroed()
                .t_rfc(config.ps_to_nck(config.t_rfc_min_ps()) as u16)
                .t_wlo(0x2b)
                .t_xp(config.t_xp_nck() as u8),
        );

        self.regs.dtpr5.write(
            DTPR5::zeroed()
                .t_rc(config.ps_to_nck(config.t_rc_min_ps) as u8)
                .t_rcd(config.ps_to_nck(config.t_rcd_min_ps) as u8)
                .t_wtr(config.t_wtr_l_min_nck() as u8),
        );

        self.regs.dtpr6.write(
            DTPR6::zeroed()
                .pub_wl(config.write_latency_nck() as u8)
                .pub_rl(config.read_latency_nck() as u8),
        );
    }

    fn rdimm_config(&mut self, config: &GeneralConfig) {
        let rnk_mrr_en = match config.module_config {
            ModuleConfig::Unbuffered(mod_config) => {
                if mod_config.rank_1_mirrored {
                    0b10
                } else {
                    0b00
                }
            }
            _ => 0b00,
        };
        let lrdimm = matches!(config.module_config, ModuleConfig::LoadReduced(_));
        let rdimm = matches!(config.module_config, ModuleConfig::Registered(_));
        self.regs.rdimm_gcr0.write(
            RDIMMGCR0::zeroed()
                .rdimm_iom(true)
                .err_out_iom(true)
                .err_out_odt(config.crc_en)
                .lrdimm(lrdimm)
                .rnk_mrr_en(rnk_mrr_en)
                .rdimm(rdimm),
        );

        let a17_bid = config.parity_en && rdimm;
        // default t_bc_stab
        self.regs
            .rdimm_gcr1
            .write(RDIMMGCR1::zeroed().a17_bid(a17_bid).t_bc_stab(0xc80));

        // TODO: actual values if RDIMM
        self.regs.rdimm_cr0.write(RDIMMCR0::zeroed());
        self.regs.rdimm_cr1.write(RDIMMCR1::zeroed().rc10(0x4));
        self.regs.rdimm_cr2.write(RDIMMCR2::zeroed());
        self.regs.rdimm_cr3.write(RDIMMCR3::zeroed());
        self.regs.rdimm_cr4.write(RDIMMCR4::zeroed());
    }

    fn write_mode_registers(&mut self, config: &GeneralConfig) {
        self.regs.mr0.write(MRn::zeroed().value(config.mr0()));
        self.regs.mr1.write(MRn::zeroed().value(config.mr1()));
        self.regs.mr2.write(MRn::zeroed().value(config.mr2()));
        self.regs.mr3.write(MRn::zeroed().value(config.mr3()));
        self.regs.mr4.write(MRn::zeroed().value(config.mr4()));
        self.regs.mr5.write(MRn::zeroed().value(config.mr5()));
        self.regs.mr6.write(MRn::zeroed().value(config.mr6()));
        // defaults
        self.regs.mr11.write(MRn::zeroed());
        self.regs.mr12.write(MRn::zeroed().value(0x4d));
        self.regs.mr13.write(MRn::zeroed().value(0x8));
        self.regs.mr14.write(MRn::zeroed().value(0x4d));
        self.regs.mr22.write(MRn::zeroed());
    }

    fn data_training_config(&mut self, config: &GeneralConfig) {
        self.regs.dt_cr0.write(
            DTCR0::zeroed()
                .rfsh_dt(0x8)
                .dt_drs(config.rank_addr_bits())
                .dt_rdbi_tr(0x2)
                .dt_wbd_dm(true)
                .rfsh_en(0x1)
                .dtc_mpd(true)
                .dt_mpr(true)
                .dt_rptn(0x7),
        );

        self.regs.dt_cr1.write(
            DTCR1::zeroed()
                .rank_en(0x1)
                .rd_lvl_gdiff(0x2)
                .rd_lvl_gs(0x3)
                .rd_prm_vl_trn(true)
                .rd_lvl_en(true),
        );

        self.regs.ca_tr0.write(
            CATR0::zeroed()
                .ca_cd(0x14)
                .ca_adr(0x10)
                .ca1_byte1(0x5)
                .ca1_byte0(0x4),
        );

        self.regs
            .dqs_dr0
            .write(DQSDR0::zeroed().dft_b2b_rd(0x8).dft_idl_rd(0x8));

        // defaults
        self.regs.dt_ar0.write(DTAR0::zeroed().dt_bg_bk1(0x4));
        self.regs.dt_ar1.write(DTAR1::zeroed().dt_col1(0x1));
        self.regs
            .dt_ar2
            .write(DTAR2::zeroed().dt_col3(0x3).dt_col2(0x2));
        self.regs.dt_dr0.write(
            DTDR0::zeroed()
                .dt_byte3(0xdd)
                .dt_byte2(0x22)
                .dt_byte1(0xee)
                .dt_byte0(0x11),
        );
        self.regs.dt_dr1.write(
            DTDR1::zeroed()
                .dt_byte7(0x77)
                .dt_byte6(0x88)
                .dt_byte5(0xbb)
                .dt_byte4(0x44),
        );

        self.regs
            .bist_lsr
            .write(BISTLSR::zeroed().seed(0x12340000 | config.module_capacity_megabytes()));
    }

    fn io_config(&mut self, config: &GeneralConfig) {
        self.regs.rio_cr5.write(RIOCR5::zeroed().odt_oe_mode(0x5));

        self.regs.acio_cr0.write(
            ACIOCR0::zeroed()
                .rst_iom(true)
                .rst_pdr(true)
                .ac_pdr_mode(ModeCtl::AlwaysOff)
                .ac_odt_mode(ModeCtl::AlwaysOff),
        );

        let ck_clk_gate = (1 - config.rank_addr_bits()) * 2;
        self.regs.acio_cr2.write(
            ACIOCR2::zeroed()
                .ckn_clk_gate0(ck_clk_gate)
                .ck_clk_gate0(ck_clk_gate),
        );

        let ck_oe_mode = (1 - config.rank_addr_bits()) * 4 + 5;
        self.regs
            .acio_cr3
            .write(ACIOCR3::zeroed().ck_oe_mode(ck_oe_mode));

        self.regs.acio_cr4.write(
            ACIOCR4::zeroed()
                .ckn_clk_gate1(ck_clk_gate)
                .ck_clk_gate1(ck_clk_gate),
        );

        // let acx_iom = match config.device_type {
        //     DeviceType::Ddr3 => 0b000,
        //     DeviceType::Ddr4 | DeviceType::LpDdr3 => 0b010,
        //     DeviceType::LpDdr4 => 0b100,
        // };
        // self.regs.acio_cr5.write(ACIOCR5::zeroed().acx_iom(acx_iom));

        self.regs.io_vcr0.write(
            IOVCR0::zeroed()
                .ac_refs_en(true)
                .ac_refi_en(true)
                .ac_refs_sel_range(true)
                .ac_refs_sel(0x30)
                .ac_vrefi_sel_range(true)
                .ac_vrefi_sel(0x4e),
        );

        self.regs.vt_cr0.write(
            VTCR0::zeroed()
                .t_vref(0x7)
                .dv_en(true)
                .pda_en(true)
                .vw_cr(0x4)
                .dv_max(0x32)
                .dv_init(0x19),
        );

        self.regs.vt_cr1.write(
            VTCR1::zeroed()
                .hv_max(0x7f)
                .sh_r_en(true)
                .t_vref_io(0x7)
                .hv_en(true)
                .hv_io(true),
        );

        self.regs.ac_bdlr1.write(ACBDLR1::zeroed());
        self.regs.ac_bdlr2.write(ACBDLR2::zeroed());
        self.regs.ac_bdlr6.write(ACBDLR6::zeroed());
        self.regs.ac_bdlr7.write(ACBDLR7::zeroed());
        self.regs.ac_bdlr8.write(ACBDLR8::zeroed());
        self.regs.ac_bdlr9.write(ACBDLR9::zeroed());

        for i in 0..DX8_BYTES {
            let dxn_regs = &mut self.regs.dxn_regs[i];
            dxn_regs.dxn_gcr5.write(
                DXnGCR5::zeroed()
                    .res_24_30(0x9)
                    .res_16_22(0x9)
                    .dx_refi_sel_r1(0x55)
                    .dx_refi_sel_r0(0x55),
            );

            dxn_regs.dxn_gcr6.write(
                DXnGCR6::zeroed()
                    .res_24_29(0x9)
                    .res_16_21(0x9)
                    .dx_dq_vref_r1(0x2b)
                    .dx_dq_vref_r0(0x2b),
            );
        }
    }

    fn zq_config(&mut self, config: &GeneralConfig) {
        // 40 ns in ctl clock cycles
        let pg_wait = ceilf(40.0 / config.ctl_clock_period_ns()) as u8;
        let odt_mode = match config.device_type {
            DeviceType::Ddr3 => 0b00,
            DeviceType::LpDdr3 | DeviceType::Ddr4 => 0b01,
            DeviceType::LpDdr4 => 0b10,
        };
        self.regs.zq_cr.write(
            ZQCR::zeroed()
                .pg_wait_frqb(0x11)
                .pg_wait_frqa(pg_wait)
                .zq_refi_en(true)
                .odt_mode(odt_mode)
                .iod_lmt(0x2)
                .avg_en(true)
                .avg_max(0x2),
        );

        self.regs.zq0_pr0.write(
            ZQnPR0::zeroed()
                .zprog_dram_odt(0x7)
                .zprog_host_odt(0x9)
                .zprog_asym_drv_pd(0xd)
                .zprog_asym_drv_pu(0xd),
        );

        self.regs.zq0_or0.write(
            ZQnOR0::zeroed()
                .zdata_pu_drv_ovrd(0x1e1)
                .zdata_pd_drv_ovrd(0x210),
        );
        self.regs
            .zq0_or1
            .write(ZQnOR1::zeroed().zdata_pu_odt_ovrd(0x1e1));

        let zq1_zprog_host_odt = if config.rank_addr_bits() == 1 {
            0x9
        } else {
            0xb
        };
        self.regs.zq1_pr0.write(
            ZQnPR0::zeroed()
                .pd_drv_adjust(0x1)
                .zprog_dram_odt(0x7)
                .zprog_host_odt(zq1_zprog_host_odt)
                .zprog_asym_drv_pd(0xd)
                .zprog_asym_drv_pu(0xd),
        );
    }

    pub fn configure(&mut self, config: &GeneralConfig) {
        self.datx8_config(&config);
        self.ac_config(&config);
        self.dram_config(&config);
        self.rdimm_config(&config);
        self.write_mode_registers(&config);
        self.data_training_config(&config);
        self.io_config(&config);
        self.zq_config(&config);
    }

    pub fn clear_training_status(&mut self) {
        self.regs.pgcr2.modify(|_, w| w.clr_tstat());
    }

    pub fn reset(&mut self) {
        self.regs.pir.write(PIR::zeroed().phy_rst(true));
        self.regs.pir.write(PIR::zeroed());
    }

    /// Initialize and calibrate DDR PLLs
    /// UG1085 Figure 17-16
    pub fn pll_init(&mut self, config: &GeneralConfig) {
        let mut pll_retry: u32 = 10;
        let mut pll_locked: bool = false;
        while pll_retry > 0 {
            self.regs
                .pir
                .write(PIR::zeroed().ctrl_dram_init(true).pll_init(true));
            self.regs.pir.modify(|_, w| w.init());

            while !self.regs.pgsr0.read().i_done() {}

            pll_locked = self.regs.pgsr0.read().ap_lock()
                && self.regs.dxn_regs[0].dxn_gsr0.read().dp_lock()
                && self.regs.dxn_regs[2].dxn_gsr0.read().dp_lock();

            if config.bus_width == 64 {
                pll_locked &= self.regs.dxn_regs[4].dxn_gsr0.read().dp_lock()
                    && self.regs.dxn_regs[6].dxn_gsr0.read().dp_lock();
            }

            if config.ecc_en {
                pll_locked &= self.regs.dxn_regs[8].dxn_gsr0.read().dp_lock();
            }

            pll_retry -= 1;
        }

        self.regs
            .gpr1
            .modify(|r, w| w.gpr1(r.gpr1() | (pll_retry << 16)));
        assert!(pll_locked, "One or more PLLs failed to lock");
    }

    /// Execute impedance and digital delay line calibrations in parallel
    pub fn execute_zcal_dcal(&mut self) {
        // trigger calibrations
        self.regs.pir.write(
            PIR::zeroed()
                .ctrl_dram_init(true)
                .phy_rst(true)
                .dcal(true)
                .zcal(true),
        );
        self.regs.pir.modify(|_, w| w.init());

        // poll until finished
        let mut pgsr0_read = self.regs.pgsr0.read();
        while !(pgsr0_read.zc_done()
            && pgsr0_read.dc_done()
            && pgsr0_read.pl_done()
            && pgsr0_read.i_done())
        {
            pgsr0_read = self.regs.pgsr0.read();
        }

        // re-trigger to skip phy DRAM init now that zcal and dcal are done
        self.regs.pir.modify(|_, w| w.init());
        // poll until finished
        pgsr0_read = self.regs.pgsr0.read();
        while !(pgsr0_read.di_done()
            && pgsr0_read.zc_done()
            && pgsr0_read.dc_done()
            && pgsr0_read.pl_done()
            && pgsr0_read.i_done())
        {
            pgsr0_read = self.regs.pgsr0.read();
        }
    }

    /// Execute training sequences
    /// UG1085 Figure 17-17
    pub fn execute_training(&mut self) {
        self.regs.pgcr1.modify(|_, w| w.pub_mode(true));
        self.regs.pir.write(
            PIR::zeroed()
                .ctrl_dram_init(true)
                .wr_eye(true)
                .rd_eye(true)
                .wr_deskew(true)
                .rd_deskew(true)
                .wr_lev_adj(true)
                .qs_gate(true)
                .wr_lev(true), // .init(),
        );
        self.regs.pir.modify(|_, w| w.init());

        let mut pgsr0_read = self.regs.pgsr0.read();
        let mut timeout = DEFAULT_TIMEOUT;
        while timeout > 0
            && !(pgsr0_read.ap_lock()
                && pgsr0_read.we_done()
                && pgsr0_read.re_done()
                && pgsr0_read.wd_done()
                && pgsr0_read.rd_done()
                && pgsr0_read.wla_done()
                && pgsr0_read.qsg_done()
                && pgsr0_read.wl_done()
                && pgsr0_read.di_done()
                && pgsr0_read.zc_done()
                && pgsr0_read.dc_done()
                && pgsr0_read.pl_done()
                && pgsr0_read.i_done())
        {
            pgsr0_read = self.regs.pgsr0.read();
            timeout -= 1;
        }

        assert_ne!(
            timeout, 0,
            "Timed out waiting for data training completion.\nLast read value of PGSR0: {:#X}",
            pgsr0_read.inner
        );
        assert_eq!(
            (pgsr0_read.inner >> 18) & 0xfff,
            0,
            "Error(s) occurred during data training.\nLast read value of PGSR0: {:#X}",
            pgsr0_read.inner
        );

        // vref training
        let rfsh_dt = if self.regs.vt_cr0.read().pda_en() {
            0x1
        } else {
            0x0
        };
        self.regs.dt_cr0.modify(|_, w| w.rfsh_dt(rfsh_dt));
        self.regs
            .pgcr2
            .modify(|r, w| w.t_ref_prd(r.t_ref_prd() - 400));

        // set read modes to loopback
        self.regs.pgcr3.modify(|_, w| w.rd_mode(0x3));
        for i in 0..DX8_PLLS {
            self.regs.dx8_sln_regs[i]
                .dx8_sln_dx_ctl2
                .modify(|_, w| w.rd_mode(0x3));
        }

        // trigger vref training
        self.regs
            .pir
            .write(PIR::zeroed().ctrl_dram_init(true).vref(true));
        self.regs.pir.modify(|_, w| w.init());

        // wait for completion
        pgsr0_read = self.regs.pgsr0.read();
        while !(pgsr0_read.ap_lock() && pgsr0_read.v_done() && pgsr0_read.i_done()) {
            pgsr0_read = self.regs.pgsr0.read();
        }

        // restore registers modified for vref training
        self.regs.pgcr3.modify(|_, w| w.rd_mode(0x0));
        for i in 0..DX8_PLLS {
            self.regs.dx8_sln_regs[i]
                .dx8_sln_dx_ctl2
                .modify(|_, w| w.rd_mode(0x0));
        }
        self.regs.dt_cr0.modify(|_, w| w.rfsh_dt(0x8));
        self.regs
            .pgcr2
            .modify(|r, w| w.t_ref_prd(r.t_ref_prd() + 400));

        // re-run read/write eye training
        self.regs.pir.write(PIR::zeroed().wr_eye(true).rd_eye(true));
        self.regs.pir.modify(|_, w| w.init());

        pgsr0_read = self.regs.pgsr0.read();
        while !(pgsr0_read.ap_lock()
            && pgsr0_read.we_done()
            && pgsr0_read.re_done()
            && pgsr0_read.i_done())
        {
            pgsr0_read = self.regs.pgsr0.read();
        }
    }
}
