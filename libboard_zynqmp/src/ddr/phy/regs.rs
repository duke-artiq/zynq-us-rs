use volatile_register::{RO, RW};

use libregister::{register, register_at, register_bit, register_bits, register_bits_typed};

#[allow(unused)]
#[derive(Clone, Copy)]
#[repr(u8)]
pub enum DataBusWidth {
    Width32bit = 0b00,
    Width16bit = 0b01,
}

#[derive(Debug, Clone, PartialEq)]
#[repr(u8)]
pub enum ControllerStatus {
    Init = 0,
    Normal = 1,
    Powerdown = 2,
    SelfRefresh = 3,
    Powerdown1 = 4,
    Powerdown2 = 5,
    Powerdown3 = 6,
    Powerdown4 = 7,
}

#[repr(u8)]
pub enum ZCalStatus {
    Success = 0b00,
    Overflow = 0b01,
    Underflow = 0b10,
    InProgress = 0b11,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum ModeCtl {
    Dynamic = 0b00,
    AlwaysOn = 0b01,
    AlwaysOff = 0b10,
    // Reserved = 0b11
}

#[repr(C)]
pub struct DXnRegs {
    pub dxn_gcr0: DXnGCR0,
    pub dxn_gcr1: DXnGCR1,
    pub dxn_gcr2: DXnGCR2,
    pub dxn_gcr3: DXnGCR3,
    pub dxn_gcr4: DXnGCR4,
    pub dxn_gcr5: DXnGCR5,
    pub dxn_gcr6: DXnGCR6,
    unused0: [u32; 9],
    pub dxn_bdlr0: RW<u32>,
    pub dxn_bdlr1: RW<u32>,
    pub dxn_bdlr2: RW<u32>,
    unused1: [u32; 1],
    pub dxn_bdlr3: RW<u32>,
    pub dxn_bdlr4: RW<u32>,
    pub dxn_bdlr5: RW<u32>,
    unused2: [u32; 1],
    pub dxn_bdlr6: RW<u32>,
    unused3: [u32; 7],
    pub dxn_lcdlr0: RW<u32>,
    pub dxn_lcdlr1: RW<u32>,
    pub dxn_lcdlr2: RW<u32>,
    pub dxn_lcdlr3: RW<u32>,
    pub dxn_lcdlr4: RW<u32>,
    pub dxn_lcdlr5: RW<u32>, // 0x794
    unused4: [u32; 2],
    pub dxn_mdlr0: RW<u32>, // 0x7a0
    pub dxn_mdlr1: RW<u32>,
    unused5: [u32; 6],
    pub dxn_gtr0: DXnGTR0, // 0x7c0
    unused6: [u32; 4],
    pub dxn_rsr1: RW<u32>,
    pub dxn_rsr2: RW<u32>,
    pub dxn_rsr3: RW<u32>,
    pub dxn_gsr0: DXnGSR0,
    pub dxn_gsr1: RW<u32>,
    pub dxn_gsr2: RW<u32>,
    pub dxn_gsr3: RW<u32>,
    unused7: [u32; 4],
}

#[repr(C)]
pub struct DX8SLnRegs {
    pub dx8_sln_osc: DX8SLnOSC,
    pub dx8_sln_pll_cr0: PLLCR0,
    pub dx8_sln_pll_cr1: DX8SLnPLLCR1,
    pub dx8_sln_pll_cr2: PLLCR2,
    pub dx8_sln_pll_cr3: PLLCR3,
    pub dx8_sln_pll_cr4: PLLCR4,
    pub dx8_sln_pll_cr5: PLLCR5,
    pub dx8_sln_dqs_ctl: DX8SLnDQSCTL,
    pub dx8_sln_trn_ctl: RW<u32>,
    pub dx8_sln_ddl_ctl: RW<u32>,
    pub dx8_sln_dx_ctl1: RW<u32>,
    pub dx8_sln_dx_ctl2: DX8SLnDXCTL2,
    pub dx8_sln_io_cr: DX8SLnIOCR,
    unused0: [u32; 3],
}

// max of 8 data bytes + 1 ECC
pub const DX8_BYTES: usize = 9;
// 4 data PLLs + 1 ECC
pub const DX8_PLLS: usize = 5;

#[repr(C)]
pub struct RegisterBlock {
    unused0: [u32; 1],
    pub pir: PIR,
    unused1: [u32; 2],
    pub pgcr0: PGCR0,
    pub pgcr1: PGCR1, // undocumented
    pub pgcr2: PGCR2,
    pub pgcr3: PGCR3,
    pub pgcr4: PGCR4,
    pub pgcr5: PGCR5,
    pub pgcr6: PGCR6,
    pub pgcr7: PGCR7,
    pub pgsr0: PGSR0,
    pub pgsr1: PGSR1,
    pub pgsr2: PGSR2,
    unused2: [u32; 1],
    pub ptr0: PTR0,
    pub ptr1: PTR1,
    pub ptr2: PTR2,
    pub ptr3: PTR3,
    pub ptr4: PTR4,
    pub ptr5: PTR5,
    pub ptr6: PTR6,
    unused3: [u32; 3],
    pub pll_cr0: PLLCR0,
    pub pll_cr1: PLLCR1,
    pub pll_cr2: PLLCR2,
    pub pll_cr3: PLLCR3,
    pub pll_cr4: PLLCR4,
    pub pll_cr5: PLLCR5,
    unused4: [u32; 2],
    pub dx_ccr: DXCCR,
    unused5: [u32; 1],
    pub ds_gcr: DSGCR,
    unused6: [u32; 1],
    pub odt_cr: ODTCR,
    unused7: [u32; 1],
    pub aa_cr: AACR,
    unused8: [u32; 7],
    pub gpr0: GPR0,
    pub gpr1: GPR1,
    unused9: [u32; 14],
    pub dcr: DCR,
    unused10: [u32; 3],
    pub dtpr0: DTPR0,
    pub dtpr1: DTPR1,
    pub dtpr2: DTPR2,
    pub dtpr3: DTPR3,
    pub dtpr4: DTPR4,
    pub dtpr5: DTPR5,
    pub dtpr6: DTPR6,
    unused11: [u32; 5],
    pub rdimm_gcr0: RDIMMGCR0,
    pub rdimm_gcr1: RDIMMGCR1,
    pub rdimm_gcr2: RDIMMGCR2,
    unused12: [u32; 1],
    pub rdimm_cr0: RDIMMCR0,
    pub rdimm_cr1: RDIMMCR1,
    pub rdimm_cr2: RDIMMCR2,
    pub rdimm_cr3: RDIMMCR3,
    pub rdimm_cr4: RDIMMCR4,
    unused13: [u32; 1],
    pub sch_cr0: RW<u32>,
    pub sch_cr1: RW<u32>,
    unused14: [u32; 4],
    pub mr0: MRn,
    pub mr1: MRn,
    pub mr2: MRn,
    pub mr3: MRn,
    pub mr4: MRn,
    pub mr5: MRn,
    pub mr6: MRn,
    pub mr7: MRn,
    unused15: [u32; 3],
    pub mr11: MRn,
    pub mr12: MRn,
    pub mr13: MRn,
    pub mr14: MRn,
    unused16: [u32; 7],
    pub mr22: MRn,
    unused17: [u32; 9],
    pub dt_cr0: DTCR0,
    pub dt_cr1: DTCR1,
    pub dt_ar0: DTAR0,
    pub dt_ar1: DTAR1,
    pub dt_ar2: DTAR2,
    unused18: [u32; 1],
    pub dt_dr0: DTDR0,
    pub dt_dr1: DTDR1,
    unused19: [u32; 4],
    pub dt_edr0: RO<u32>,
    pub dt_edr1: RO<u32>,
    pub dt_edr2: RO<u32>,
    pub vt_dr: RO<u32>,
    pub ca_tr0: CATR0,
    pub ca_tr1: RW<u32>,
    unused20: [u32; 2],
    pub dqs_dr0: DQSDR0,
    pub dqs_dr1: RW<u32>,
    pub dqs_dr2: RW<u32>,
    pub dt_edr3: RO<u32>, // 0x25c
    unused21: [u32; 40],
    pub dcu_ar: RW<u32>, // 0x300
    pub dcu_dr: RW<u32>,
    pub dcu_rr: RW<u32>,
    pub dcu_lr: RW<u32>,
    pub dcu_gcr: RW<u32>,
    pub dcu_tpr: RW<u32>,
    pub dcu_sr0: RO<u32>,
    pub dcu_sr1: RO<u32>, // 0x31c
    unused22: [u32; 61],
    pub bist_lsr: BISTLSR, // 0x414, undocumented
    unused23: [u32; 49],
    pub rank_idr: RW<u32>, // 0x4dc
    pub rio_cr0: RW<u32>,
    pub rio_cr1: RW<u32>,
    pub rio_cr2: RW<u32>,
    pub rio_cr3: RW<u32>,
    pub rio_cr4: RW<u32>,
    pub rio_cr5: RIOCR5, // 0x4f4
    unused24: [u32; 2],
    pub acio_cr0: ACIOCR0,
    pub acio_cr1: ACIOCR1,
    pub acio_cr2: ACIOCR2,
    pub acio_cr3: ACIOCR3,
    pub acio_cr4: ACIOCR4,
    pub acio_cr5: ACIOCR5,
    unused25: [u32; 2],
    pub io_vcr0: IOVCR0,
    pub io_vcr1: RW<u32>,
    pub vt_cr0: VTCR0,
    pub vt_cr1: VTCR1,
    unused26: [u32; 4],
    pub ac_bdlr0: ACBDLR0,
    pub ac_bdlr1: ACBDLR1,
    pub ac_bdlr2: ACBDLR2,
    pub ac_bdlr3: ACBDLR3,
    pub ac_bdlr4: ACBDLR4,
    pub ac_bdlr5: ACBDLR5,
    pub ac_bdlr6: ACBDLR6,
    pub ac_bdlr7: ACBDLR7,
    pub ac_bdlr8: ACBDLR8,
    pub ac_bdlr9: ACBDLR9,
    unused27: [u32; 5],
    pub ac_bdlr15: ACBDLR15,
    pub ac_bdlr16: ACBDLR16,
    pub ac_lcdlr: RW<u32>,
    unused28: [u32; 6],
    pub ac_mdlr0: RW<u32>,
    pub ac_mdlr1: RW<u32>,
    unused29: [u32; 54],
    pub zq_cr: ZQCR,
    pub zq0_pr0: ZQnPR0,
    pub zq0_pr1: ZQnPR1,
    pub zq0_dr0: ZQnDR0,
    pub zq0_dr1: ZQnDR1,
    pub zq0_or0: ZQnOR0,
    pub zq0_or1: ZQnOR1,
    pub zq0_sr: ZQnSR, // 0x69c
    unused30: [u32; 1],
    pub zq1_pr0: ZQnPR0,
    pub zq1_pr1: ZQnPR1,
    pub zq1_dr0: ZQnDR0,
    pub zq1_dr1: ZQnDR1,
    pub zq1_or0: ZQnOR0,
    pub zq1_or1: ZQnOR1,
    pub zq1_sr: ZQnSR, // 0x6bc
    unused31: [u32; 16],
    pub dxn_regs: [DXnRegs; DX8_BYTES],
    unused32: [u32; 256],
    pub dx8_sln_regs: [DX8SLnRegs; DX8_PLLS],
    unused33: [u32; 160],
    pub dx8_slb_osc: DX8SLnOSC,
    pub dx8_slb_pll_cr0: PLLCR0,
    pub dx8_slb_pll_cr1: DX8SLnPLLCR1,
    pub dx8_slb_pll_cr2: PLLCR2,
    pub dx8_slb_pll_cr3: PLLCR3,
    pub dx8_slb_pll_cr4: PLLCR4,
    pub dx8_slb_pll_cr5: PLLCR5,
    pub dx8_slb_dqs_ctl: DX8SLnDQSCTL,
    pub dx8_slb_trn_ctl: RW<u32>,
    pub dx8_slb_ddl_ctl: RW<u32>,
    pub dx8_slb_dx_ctl1: RW<u32>,
    pub dx8_slb_dx_ctl2: DX8SLnDXCTL2,
    pub dx8_slb_io_cr: DX8SLnIOCR,
}

register_at!(RegisterBlock, 0xFD08_0000, ddr_phy);

// PIR
register!(pir, PIR, RW, u32);
register_bit!(pir, zcal_bypass, 30, WTC);
register_bit!(pir, delay_cal_pause, 29, WTC);
register_bit!(pir, dqs2dq, 20);
register_bit!(pir, rdimm_init, 19);
register_bit!(pir, ctrl_dram_init, 18);
register_bit!(pir, vref, 17);
register_bit!(pir, wr_eye, 15);
register_bit!(pir, rd_eye, 14);
register_bit!(pir, wr_deskew, 13);
register_bit!(pir, rd_deskew, 12);
register_bit!(pir, wr_lev_adj, 11);
register_bit!(pir, qs_gate, 10);
register_bit!(pir, wr_lev, 9);
register_bit!(pir, dram_init, 8);
register_bit!(pir, dram_rst, 7);
register_bit!(pir, phy_rst, 6);
register_bit!(pir, dcal, 5);
register_bit!(pir, pll_init, 4);
register_bit!(pir, lpddr3_ca, 2);
register_bit!(pir, zcal, 1);
register_bit!(pir, init, 0, WTC);

register!(pgcr0, PGCR0, RW, u32);
register_bit!(pgcr0, adcp, 31);
register_bit!(pgcr0, phy_frst, 26);
register_bits!(pgcr0, osc_acdl, u8, 24, 25);
register_bits!(pgcr0, dto_sel, u8, 14, 18);
register_bits!(pgcr0, osc_div, u8, 9, 12);
register_bit!(pgcr0, osc_en, 8);

register!(pgcr1, PGCR1, RW, u32);
register_bit!(pgcr1, pub_mode, 6);

register!(pgcr2, PGCR2, RW, u32);
register_bit!(pgcr2, clr_tstat, 31, WTC);
register_bit!(pgcr2, clr_zcal, 30, WTC);
register_bit!(pgcr2, clr_perr, 29, WTC);
register_bit!(pgcr2, icpc, 28);
register_bits!(pgcr2, dtpm_xtmr, u8, 20, 27);
register_bit!(pgcr2, init_fsmbyp, 19);
register_bit!(pgcr2, pll_fsmbyp, 18);
register_bits!(pgcr2, t_ref_prd, u32, 0, 17);

register!(pgcr3, PGCR3, RW, u32);
register_bits!(pgcr3, ckn_en, u8, 24, 31);
register_bits!(pgcr3, ck_en, u8, 16, 23);
register_bits_typed!(pgcr3, gate_ac_rd_clk, u8, ModeCtl, 13, 14);
register_bits_typed!(pgcr3, gate_ac_ddr_clk, u8, ModeCtl, 11, 12);
register_bits_typed!(pgcr3, gate_ac_ctl_clk, u8, ModeCtl, 9, 10);
register_bits!(pgcr3, ddl_bypmode, u8, 6, 7);
register_bit!(pgcr3, io_lb, 5);
register_bits!(pgcr3, rd_mode, u8, 3, 4);
register_bit!(pgcr3, dis_rst, 2);
register_bits!(pgcr3, clk_level, u8, 0, 1);

register!(pgcr4, PGCR4, RW, u32);
register_bit!(pgcr4, ac_ddl_ld, 29);
register_bits!(pgcr4, ac_ddl_byp, u8, 24, 28);
register_bit!(pgcr4, oe_ddl_byp, 23);
register_bit!(pgcr4, te_ddl_byp, 22);
register_bit!(pgcr4, pdr_ddl_byp, 21);
register_bit!(pgcr4, rrr_mode, 20);
register_bit!(pgcr4, wrr_mode, 19);
register_bits!(pgcr4, lp_wakeup_thrsh, u8, 4, 7);
register_bit!(pgcr4, lp_pll_pd, 1);
register_bit!(pgcr4, lp_io_pd, 0);

register!(pgcr5, PGCR5, RW, u32);
register_bits!(pgcr5, frq_bt, u8, 24, 31);
register_bits!(pgcr5, frq_at, u8, 16, 23);
register_bits!(pgcr5, discn_period, u8, 8, 15);
register_bits!(pgcr5, vref_rbctrl, u8, 4, 7);
register_bit!(pgcr5, dx_refi_sel_range, 2);
register_bit!(pgcr5, ddl_pg_act, 1);
register_bit!(pgcr5, ddl_pg_rw, 0);

register!(pgcr6, PGCR6, RW, u32);
register_bits!(pgcr6, dld_lmt, u8, 16, 23);
register_bit!(pgcr6, ac_dlvt, 13);
register_bit!(pgcr6, ac_bvt, 12);
register_bit!(pgcr6, odt_bvt, 11);
register_bit!(pgcr6, cke_bvt, 10);
register_bit!(pgcr6, csn_bvt, 9);
register_bit!(pgcr6, ck_bvt, 8);
register_bit!(pgcr6, in_hvt, 0);

register!(pgcr7, PGCR7, RW, u32);
register_bit!(pgcr7, ac_cal_clk, 5);
register_bit!(pgcr7, ac_rclk_md, 4);
register_bit!(pgcr7, ac_dldt, 3);
register_bit!(pgcr7, ac_dto_sel, 1);
register_bit!(pgcr7, ac_tmode, 0);

register!(pgsr0, PGSR0, RO, u32);
register_bit!(pgsr0, ap_lock, 31);
register_bit!(pgsr0, ca_wrn, 29);
register_bit!(pgsr0, ca_err, 28);
register_bit!(pgsr0, we_err, 27);
register_bit!(pgsr0, re_err, 26);
register_bit!(pgsr0, wd_err, 25);
register_bit!(pgsr0, rd_err, 24);
register_bit!(pgsr0, wla_err, 23);
register_bit!(pgsr0, qsg_err, 22);
register_bit!(pgsr0, wl_err, 21);
register_bit!(pgsr0, zc_err, 20);
register_bit!(pgsr0, v_err, 19);
register_bit!(pgsr0, dqs2dq_err, 18);
register_bit!(pgsr0, dqs2dq_done, 15);
register_bit!(pgsr0, v_done, 14);
register_bit!(pgsr0, ca_done, 12);
register_bit!(pgsr0, we_done, 11);
register_bit!(pgsr0, re_done, 10);
register_bit!(pgsr0, wd_done, 9);
register_bit!(pgsr0, rd_done, 8);
register_bit!(pgsr0, wla_done, 7);
register_bit!(pgsr0, qsg_done, 6);
register_bit!(pgsr0, wl_done, 5);
register_bit!(pgsr0, di_done, 4);
register_bit!(pgsr0, zc_done, 3);
register_bit!(pgsr0, dc_done, 2);
register_bit!(pgsr0, pl_done, 1);
// Wait at least 32 ctl_clk cycles after this is first observed to be 1b1
// before starting/resuming traffic to DRAM
// or triggering new PIR.INIT
register_bit!(pgsr0, i_done, 0);

register!(pgsr1, PGSR1, RO, u32);
register_bit!(pgsr1, par_err, 31);
register_bit!(pgsr1, vt_stop, 30);
register_bits!(pgsr1, dlt_code, u32, 1, 24);
register_bit!(pgsr1, dlt_done, 0);

register!(pgsr2, PGSR2, RO, u32);
register_bits!(pgsr2, dlt_code, u32, 1, 24);
register_bit!(pgsr2, dlt_done, 0);

register!(ptr0, PTR0, RW, u32);
register_bits!(ptr0, t_pll_pd, u16, 21, 31);
register_bits!(ptr0, t_pll_gs, u16, 6, 20);
register_bits!(ptr0, t_phy_rst, u8, 0, 5);

register!(ptr1, PTR1, RW, u32);
register_bits!(ptr1, t_pll_lock, u16, 16, 31);
register_bits!(ptr1, t_pll_rst, u16, 0, 12);

register!(ptr2, PTR2, RW, u32);
register_bits!(ptr2, t_wl_dly_s, u8, 15, 19);
register_bits!(ptr2, t_cal_h, u8, 10, 14);
register_bits!(ptr2, t_cal_s, u8, 5, 9);
register_bits!(ptr2, t_cal_on, u8, 0, 4);

register!(ptr3, PTR3, RW, u32);
register_bits!(ptr3, t_dinit0, u32, 0, 22);

register!(ptr4, PTR4, RW, u32);
register_bits!(ptr4, t_dinit1, u16, 0, 12);

register!(ptr5, PTR5, RW, u32);
register_bits!(ptr5, t_dinit2, u32, 0, 18);

register!(ptr6, PTR6, RW, u32);
register_bits!(ptr6, t_dinit4, u8, 20, 26);
register_bits!(ptr6, t_dinit3, u16, 0, 11);

register!(pll_cr0, PLLCR0, RW, u32);
register_bit!(pll_cr0, pll_rst, 30);
register_bit!(pll_cr0, pll_pd, 29);
register_bit!(pll_cr0, r_stop_m, 28);
register_bits!(pll_cr0, frq_sel, u8, 24, 27);
register_bit!(pll_cr0, r_lock_m, 23);
register_bits!(pll_cr0, cppc, u8, 17, 21);
register_bits!(pll_cr0, cpic, u8, 13, 16);
register_bit!(pll_cr0, gshift, 12);

register!(pll_cr1, PLLCR1, RW, u32);
register_bits!(pll_cr1, pll_prog, u16, 16, 31);
register_bit!(pll_cr1, byp_vreg_cp, 5);
register_bit!(pll_cr1, byp_vreg_dig, 4);
register_bit!(pll_cr1, lock_ps, 2);
register_bit!(pll_cr1, lock_cs, 1);
register_bit!(pll_cr1, lock_ds, 0);

register!(pll_cr2, PLLCR2, RW, u32);
register_bits!(pll_cr2, pll_ctrl_31_0, u32, 0, 31);

register!(pll_cr3, PLLCR3, RW, u32);
register_bits!(pll_cr3, pll_ctrl_63_32, u32, 0, 31);

register!(pll_cr4, PLLCR4, RW, u32);
register_bits!(pll_cr4, pll_ctrl_95_64, u32, 0, 31);

register!(pll_cr5, PLLCR5, RW, u32);
register_bits!(pll_cr5, pll_ctrl_103_96, u8, 0, 7);

register!(dx_ccr, DXCCR, RW, u32);
register_bit!(dx_ccr, rk_loop, 29);
register_bits!(dx_ccr, dqs2dq_mper, u8, 3, 6);

register!(ds_gcr, DSGCR, RW, u32);
register_bit!(ds_gcr, rdbi_cl_sel, 27);
register_bits!(ds_gcr, rdbi_cl, u8, 24, 26);
register_bit!(ds_gcr, phy_zu_en, 23);
register_bit!(ds_gcr, rst_oe, 21);
register_bits!(ds_gcr, sdr_mode, u8, 19, 20);
register_bit!(ds_gcr, ato_ae, 17);
register_bit!(ds_gcr, dto_oe, 16);
register_bit!(ds_gcr, dto_iom, 15);
register_bit!(ds_gcr, dto_pdr, 14);
register_bit!(ds_gcr, dto_odt, 12);
register_bits!(ds_gcr, puad, u8, 6, 11);
register_bit!(ds_gcr, cua_en, 5);
register_bit!(ds_gcr, ctl_zu_en, 2);
register_bit!(ds_gcr, pur_en, 0);

register!(odt_cr, ODTCR, RW, u32);
register_bits!(odt_cr, wr_odt, u8, 16, 17);
register_bits!(odt_cr, rd_odt, u8, 0, 1);

register!(aa_cr, AACR, RW, u32);
register_bit!(aa_cr, aa_oen_c, 31);
register_bit!(aa_cr, aa_en_c, 30);
register_bits!(aa_cr, aa_tr, u32, 0, 29);

register!(gpr0, GPR0, RW, u32);
register_bits!(gpr0, gpr0, u32, 1, 31);
register_bit!(gpr0, wdqs_ext, 0);

register!(gpr1, GPR1, RW, u32);
register_bits!(gpr1, gpr1, u32, 0, 31);

register!(dcr, DCR, RW, u32);
register_bit!(dcr, geardn, 31);
register_bit!(dcr, ubg, 30);
register_bit!(dcr, udimm, 29);
register_bit!(dcr, ddr_2t, 28);
register_bit!(dcr, no_sra, 27);
register_bits!(dcr, bytemask, u8, 10, 17);
register_bits!(dcr, ddr_type, u8, 8, 9);
register_bit!(dcr, mpr_dq, 7);
register_bits!(dcr, pdq, u8, 4, 6);
register_bit!(dcr, ddr_8bnk, 3);
register_bits!(dcr, ddr_md, u8, 0, 2);

register!(dtpr0, DTPR0, RW, u32);
register_bits!(dtpr0, t_rrd, u8, 24, 28);
register_bits!(dtpr0, t_ras, u8, 16, 22);
register_bits!(dtpr0, t_rp, u8, 8, 14);
register_bits!(dtpr0, t_rtp, u8, 0, 4);

register!(dtpr1, DTPR1, RW, u32);
register_bits!(dtpr1, t_wlm_rd, u8, 24, 30);
register_bits!(dtpr1, t_faw, u8, 16, 22);
register_bits!(dtpr1, t_mod, u8, 8, 10);
register_bits!(dtpr1, t_mrd, u8, 0, 4);

register!(dtpr2, DTPR2, RW, u32);
register_bit!(dtpr2, t_rtw, 28);
register_bit!(dtpr2, t_rt_odt, 24);
register_bits!(dtpr2, t_cke, u8, 16, 19);
register_bits!(dtpr2, t_xs, u16, 0, 9);

register!(dtpr3, DTPR3, RW, u32);
register_bits!(dtpr3, t_ofd_x, u8, 29, 31);
register_bits!(dtpr3, t_ccd, u8, 26, 28);
register_bits!(dtpr3, t_dllk, u16, 16, 25);
register_bits!(dtpr3, t_dqsck_max, u8, 8, 11);
register_bits!(dtpr3, t_dqsck, u8, 0, 2);

register!(dtpr4, DTPR4, RW, u32);
register_bits!(dtpr4, t_aond_aofd, u8, 28, 29);
register_bits!(dtpr4, t_rfc, u16, 16, 25);
register_bits!(dtpr4, t_wlo, u8, 8, 13);
register_bits!(dtpr4, t_xp, u8, 0, 4);

register!(dtpr5, DTPR5, RW, u32);
register_bits!(dtpr5, t_rc, u8, 16, 23);
register_bits!(dtpr5, t_rcd, u8, 8, 14);
register_bits!(dtpr5, t_wtr, u8, 0, 4);

register!(dtpr6, DTPR6, RW, u32);
register_bit!(dtpr6, pub_wl_en, 31);
register_bit!(dtpr6, pub_rl_en, 30);
register_bits!(dtpr6, pub_wl, u8, 8, 13);
register_bits!(dtpr6, pub_rl, u8, 0, 5);

register!(rdimm_gcr0, RDIMMGCR0, RW, u32);
register_bit!(rdimm_gcr0, qcs_en, 30);
register_bit!(rdimm_gcr0, rdimm_iom, 27);
register_bit!(rdimm_gcr0, err_out_oe, 23);
register_bit!(rdimm_gcr0, err_out_iom, 22);
register_bit!(rdimm_gcr0, err_out_pdr, 21);
register_bit!(rdimm_gcr0, err_out_odt, 19);
register_bit!(rdimm_gcr0, lrdimm, 18);
register_bit!(rdimm_gcr0, par_in_iom, 17);
register_bits!(rdimm_gcr0, rnk_mrr_en, u8, 4, 5);
register_bit!(rdimm_gcr0, so_perr, 2);
register_bit!(rdimm_gcr0, err_no_reg, 1);
register_bit!(rdimm_gcr0, rdimm, 0);

register!(rdimm_gcr1, RDIMMGCR1, RW, u32);
register_bit!(rdimm_gcr1, a17_bid, 28);
register_bits!(rdimm_gcr1, t_bc_mrd_l2, u8, 24, 26);
register_bits!(rdimm_gcr1, t_bc_mrd_l, u8, 20, 22);
register_bits!(rdimm_gcr1, t_bc_mrd, u8, 16, 18);
register_bits!(rdimm_gcr1, t_bc_stab, u16, 0, 13);

register!(rdimm_gcr2, RDIMMGCR2, RW, u32);
register_bits!(rdimm_gcr2, cr_init, u32, 0, 31);

register!(rdimm_cr0, RDIMMCR0, RW, u32);
register_bits!(rdimm_cr0, rc7, u8, 28, 31);
register_bits!(rdimm_cr0, rc6, u8, 24, 27);
register_bits!(rdimm_cr0, rc5, u8, 20, 23);
register_bits!(rdimm_cr0, rc4, u8, 16, 19);
register_bits!(rdimm_cr0, rc3, u8, 12, 15);
register_bits!(rdimm_cr0, rc2, u8, 8, 11);
register_bits!(rdimm_cr0, rc1, u8, 4, 7);
register_bits!(rdimm_cr0, rc0, u8, 0, 3);

register!(rdimm_cr1, RDIMMCR1, RW, u32);
register_bits!(rdimm_cr1, rc15, u8, 28, 31);
register_bits!(rdimm_cr1, rc14, u8, 24, 27);
register_bits!(rdimm_cr1, rc13, u8, 20, 23);
register_bits!(rdimm_cr1, rc12, u8, 16, 19);
register_bits!(rdimm_cr1, rc11, u8, 12, 15);
register_bits!(rdimm_cr1, rc10, u8, 8, 11);
register_bits!(rdimm_cr1, rc9, u8, 4, 7);
register_bits!(rdimm_cr1, rc8, u8, 0, 3);

register!(rdimm_cr2, RDIMMCR2, RW, u32);
register_bits!(rdimm_cr2, rc4x, u8, 24, 31);
register_bits!(rdimm_cr2, rc3x, u8, 16, 23);
register_bits!(rdimm_cr2, rc2x, u8, 8, 15);
register_bits!(rdimm_cr2, rc1x, u8, 0, 7);

register!(rdimm_cr3, RDIMMCR3, RW, u32);
register_bits!(rdimm_cr3, rc8x, u8, 24, 31);
register_bits!(rdimm_cr3, rc7x, u8, 16, 23);
register_bits!(rdimm_cr3, rc6x, u8, 8, 15);
register_bits!(rdimm_cr3, rc5x, u8, 0, 7);

register!(rdimm_cr4, RDIMMCR4, RW, u32);
register_bits!(rdimm_cr4, rcxx, u8, 24, 31);
register_bits!(rdimm_cr4, rcbx, u8, 16, 23);
register_bits!(rdimm_cr4, rcax, u8, 8, 15);
register_bits!(rdimm_cr4, rc9x, u8, 0, 7);

// documentation for the mode registers is all screwy, but I'm fairly sure
// the mode register value is just the lower 16 bits of each reg
register!(mrn, MRn, RW, u32);
register_bits!(mrn, value, u16, 0, 15);

register!(dt_cr0, DTCR0, RW, u32);
register_bits!(dt_cr0, rfsh_dt, u8, 28, 31);
register_bits!(dt_cr0, dt_drs, u8, 24, 25);
register_bit!(dt_cr0, dt_exg, 23);
register_bit!(dt_cr0, dt_exd, 22);
register_bit!(dt_cr0, dt_dstp, 21, WTC);
register_bit!(dt_cr0, dt_den, 20);
register_bits!(dt_cr0, dt_dbs, u8, 16, 19);
register_bits!(dt_cr0, dt_rdbi_tr, u8, 14, 15);
register_bit!(dt_cr0, dt_wbd_dm, 12);
register_bits!(dt_cr0, rfsh_en, u8, 8, 11);
register_bit!(dt_cr0, dtc_mpd, 7);
register_bit!(dt_cr0, dt_mpr, 6);
register_bit!(dt_cr0, inc_weye, 4);
register_bits!(dt_cr0, dt_rptn, u8, 0, 3);

register!(dt_cr1, DTCR1, RW, u32);
register_bits!(dt_cr1, rank_en, u8, 16, 17);
register_bits!(dt_cr1, dt_rank, u8, 12, 13);
register_bits!(dt_cr1, rd_lvl_gdiff, u8, 8, 10);
register_bits!(dt_cr1, rd_lvl_gs, u8, 4, 6);
register_bit!(dt_cr1, rd_prm_vl_trn, 2);
register_bit!(dt_cr1, rd_lvl_en, 1);
register_bit!(dt_cr1, bst_en, 0);

register!(dt_ar0, DTAR0, RW, u32);
register_bits!(dt_ar0, mpr_loc, u8, 28, 29);
register_bits!(dt_ar0, dt_bg_bk1, u8, 24, 27);
register_bits!(dt_ar0, dt_bg_bk0, u8, 20, 23);
register_bits!(dt_ar0, dt_row, u32, 0, 17);

register!(dt_ar1, DTAR1, RW, u32);
register_bits!(dt_ar1, dt_col1, u16, 16, 24);
register_bits!(dt_ar1, dt_col0, u16, 0, 8);

register!(dt_ar2, DTAR2, RW, u32);
register_bits!(dt_ar2, dt_col3, u16, 16, 24);
register_bits!(dt_ar2, dt_col2, u16, 0, 8);

register!(dt_dr0, DTDR0, RW, u32);
register_bits!(dt_dr0, dt_byte3, u8, 24, 31);
register_bits!(dt_dr0, dt_byte2, u8, 16, 23);
register_bits!(dt_dr0, dt_byte1, u8, 8, 15);
register_bits!(dt_dr0, dt_byte0, u8, 0, 7);

register!(dt_dr1, DTDR1, RW, u32);
register_bits!(dt_dr1, dt_byte7, u8, 24, 31);
register_bits!(dt_dr1, dt_byte6, u8, 16, 23);
register_bits!(dt_dr1, dt_byte5, u8, 8, 15);
register_bits!(dt_dr1, dt_byte4, u8, 0, 7);

register!(ca_tr0, CATR0, RW, u32);
register_bits!(ca_tr0, ca_cd, u8, 16, 20);
register_bits!(ca_tr0, ca_adr, u8, 8, 12);
register_bits!(ca_tr0, ca1_byte1, u8, 4, 7);
register_bits!(ca_tr0, ca1_byte0, u8, 0, 3);

register!(dqs_dr0, DQSDR0, RW, u32);
register_bits!(dqs_dr0, dft_dly, u8, 28, 31);
register_bit!(dqs_dr0, dft_zq_up, 27);
register_bit!(dqs_dr0, dft_ddl_up, 26);
register_bits!(dqs_dr0, dft_rd_spc, u8, 20, 21);
register_bits!(dqs_dr0, dft_b2b_rd, u8, 16, 19);
register_bits!(dqs_dr0, dft_idl_rd, u8, 12, 15);
register_bits!(dqs_dr0, dft_gpulse, u8, 4, 7);
register_bits!(dqs_dr0, dft_up_mode, u8, 2, 3);
register_bit!(dqs_dr0, dft_dt_mode, 1);
register_bit!(dqs_dr0, dft_dt_en, 0);

register!(bist_lsr, BISTLSR, RW, u32);
register_bits!(bist_lsr, seed, u32, 0, 31);

register!(rio_cr5, RIOCR5, RW, u32);
register_bits!(rio_cr5, odt_oe_mode, u8, 0, 3);

register!(acio_cr0, ACIOCR0, RW, u32);
register_bits!(acio_cr0, acsr, u8, 30, 31);
register_bit!(acio_cr0, rst_iom, 29);
register_bit!(acio_cr0, rst_pdr, 28);
register_bit!(acio_cr0, rst_odt, 26);
register_bits!(acio_cr0, ck_dcc, u8, 6, 9);
register_bits_typed!(acio_cr0, ac_pdr_mode, u8, ModeCtl, 4, 5);
register_bits_typed!(acio_cr0, ac_odt_mode, u8, ModeCtl, 2, 3);
register_bit!(acio_cr0, ac_rank_clk_sel, 0);

register!(acio_cr1, ACIOCR1, RW, u32);
register_bits!(acio_cr1, aoe_mode, u32, 0, 31);

register!(acio_cr2, ACIOCR2, RW, u32);
register_bit!(acio_cr2, clk_gen_clk_gate, 31);
register_bit!(acio_cr2, ac_oe_clk_gate0, 30);
register_bit!(acio_cr2, ac_pdr_clk_gate0, 29);
register_bit!(acio_cr2, ac_te_clk_gate0, 28);
register_bits!(acio_cr2, ckn_clk_gate0, u8, 26, 27);
register_bits!(acio_cr2, ck_clk_gate0, u8, 24, 25);
register_bits!(acio_cr2, ac_clk_gate0, u32, 0, 23);

register!(acio_cr3, ACIOCR3, RW, u32);
register_bits_typed!(acio_cr3, par_oe_mode, u8, ModeCtl, 30, 31);
register_bits_typed!(acio_cr3, bg_oe_mode, u8, ModeCtl, 26, 29);
register_bits_typed!(acio_cr3, ba_oe_mode, u8, ModeCtl, 22, 25);
register_bits_typed!(acio_cr3, a17_oe_mode, u8, ModeCtl, 20, 21);
register_bits_typed!(acio_cr3, a16_oe_mode, u8, ModeCtl, 18, 19);
register_bits_typed!(acio_cr3, act_oe_mode, u8, ModeCtl, 16, 17);
register_bits!(acio_cr3, ck_oe_mode, u8, 0, 3);

register!(acio_cr4, ACIOCR4, RW, u32);
register_bit!(acio_cr4, lb_clk_gate, 31);
register_bit!(acio_cr4, ac_oe_clk_gate1, 30);
register_bit!(acio_cr4, ac_pdr_clk_gate1, 29);
register_bit!(acio_cr4, ac_te_clk_gate1, 28);
register_bits!(acio_cr4, ckn_clk_gate1, u8, 26, 27);
register_bits!(acio_cr4, ck_clk_gate1, u8, 24, 25);
register_bits!(acio_cr4, ac_clk_gate1, u32, 0, 23);

register!(acio_cr5, ACIOCR5, RW, u32);
register_bits!(acio_cr5, acx_iom, u8, 22, 24);

register!(io_vcr0, IOVCR0, RW, u32);
register_bits!(io_vcr0, ac_ref_iom, u8, 29, 31);
register_bit!(io_vcr0, ac_refp_en, 28);
register_bit!(io_vcr0, ac_refs_en, 25);
register_bit!(io_vcr0, ac_refi_en, 24);
register_bit!(io_vcr0, ac_refe_sel_range, 23);
register_bits!(io_vcr0, ac_refe_sel, u8, 16, 22);
register_bit!(io_vcr0, ac_refs_sel_range, 15);
register_bits!(io_vcr0, ac_refs_sel, u8, 8, 14);
register_bit!(io_vcr0, ac_vrefi_sel_range, 7);
register_bits!(io_vcr0, ac_vrefi_sel, u8, 0, 6);

register!(vt_cr0, VTCR0, RW, u32);
register_bits!(vt_cr0, t_vref, u8, 29, 31);
register_bit!(vt_cr0, dv_en, 28);
register_bit!(vt_cr0, pda_en, 27);
register_bits!(vt_cr0, vw_cr, u8, 22, 25);
register_bits!(vt_cr0, dv_ss, u8, 18, 21);
register_bits!(vt_cr0, dv_max, u8, 12, 17);
register_bits!(vt_cr0, dv_min, u8, 6, 11);
register_bits!(vt_cr0, dv_init, u8, 0, 5);

register!(vt_cr1, VTCR1, RW, u32);
register_bits!(vt_cr1, hv_ss, u8, 28, 31);
register_bits!(vt_cr1, hv_max, u8, 20, 26);
register_bits!(vt_cr1, hv_min, u8, 12, 18);
register_bits!(vt_cr1, sh_rnk, u8, 9, 10);
register_bit!(vt_cr1, sh_r_en, 8);
register_bits!(vt_cr1, t_vref_io, u8, 5, 7);
register_bit!(vt_cr1, hv_en, 1);
register_bit!(vt_cr1, hv_io, 0);

register!(ac_bdlr0, ACBDLR0, RW, u32);
register_bits!(ac_bdlr0, ck1bd, u8, 8, 13);
register_bits!(ac_bdlr0, ck0bd, u8, 0, 5);

register!(ac_bdlr1, ACBDLR1, RW, u32);
register_bits!(ac_bdlr1, parbd, u8, 24, 29);
register_bits!(ac_bdlr1, a16bd, u8, 16, 21);
register_bits!(ac_bdlr1, a17bd, u8, 8, 13);
register_bits!(ac_bdlr1, actbd, u8, 0, 5);

register!(ac_bdlr2, ACBDLR2, RW, u32);
register_bits!(ac_bdlr2, bg1bd, u8, 24, 29);
register_bits!(ac_bdlr2, bg0bd, u8, 16, 21);
register_bits!(ac_bdlr2, ba1bd, u8, 8, 13);
register_bits!(ac_bdlr2, ba0bd, u8, 0, 5);

register!(ac_bdlr3, ACBDLR3, RW, u32);
register_bits!(ac_bdlr3, cs1bd, u8, 8, 13);
register_bits!(ac_bdlr3, cs0bd, u8, 0, 5);

register!(ac_bdlr4, ACBDLR4, RW, u32);
register_bits!(ac_bdlr4, odt1bd, u8, 8, 13);
register_bits!(ac_bdlr4, odt0bd, u8, 0, 5);

register!(ac_bdlr5, ACBDLR5, RW, u32);
register_bits!(ac_bdlr5, cke1bd, u8, 8, 13);
register_bits!(ac_bdlr5, cke0bd, u8, 0, 5);

register!(ac_bdlr6, ACBDLR6, RW, u32);
register_bits!(ac_bdlr6, a03bd, u8, 24, 29);
register_bits!(ac_bdlr6, a02bd, u8, 16, 21);
register_bits!(ac_bdlr6, a01bd, u8, 8, 13);
register_bits!(ac_bdlr6, a00bd, u8, 0, 5);

register!(ac_bdlr7, ACBDLR7, RW, u32);
register_bits!(ac_bdlr7, a07bd, u8, 24, 29);
register_bits!(ac_bdlr7, a06bd, u8, 16, 21);
register_bits!(ac_bdlr7, a05bd, u8, 8, 13);
register_bits!(ac_bdlr7, a04bd, u8, 0, 5);

register!(ac_bdlr8, ACBDLR8, RW, u32);
register_bits!(ac_bdlr8, a11bd, u8, 24, 29);
register_bits!(ac_bdlr8, a10bd, u8, 16, 21);
register_bits!(ac_bdlr8, a09bd, u8, 8, 13);
register_bits!(ac_bdlr8, a08bd, u8, 0, 5);

register!(ac_bdlr9, ACBDLR9, RW, u32);
register_bits!(ac_bdlr9, a15bd, u8, 24, 29);
register_bits!(ac_bdlr9, a14bd, u8, 16, 21);
register_bits!(ac_bdlr9, a13bd, u8, 8, 13);
register_bits!(ac_bdlr9, a12bd, u8, 0, 5);

register!(ac_bdlr15, ACBDLR15, RW, u32);
register_bits!(ac_bdlr15, oebd, u8, 16, 21);
register_bits!(ac_bdlr15, tebd, u8, 8, 13);
register_bits!(ac_bdlr15, pdrbd, u8, 0, 5);

register!(ac_bdlr16, ACBDLR16, RW, u32);
register_bits!(ac_bdlr16, ckn1bd, u8, 8, 13);
register_bits!(ac_bdlr16, ckn0bd, u8, 0, 5);

register!(zq_cr, ZQCR, RW, u32);
register_bit!(zq_cr, zq_refi_sel_range, 25);
register_bits!(zq_cr, pg_wait_frqb, u8, 19, 24);
register_bits!(zq_cr, pg_wait_frqa, u8, 13, 18);
register_bit!(zq_cr, zq_refp_en, 12);
register_bit!(zq_cr, zq_refi_en, 11);
register_bits!(zq_cr, odt_mode, u8, 9, 10);
register_bit!(zq_cr, force_zcal_vt_update, 8);
register_bits!(zq_cr, iod_lmt, u8, 5, 7);
register_bit!(zq_cr, avg_en, 4);
register_bits!(zq_cr, avg_max, u8, 2, 3);
register_bit!(zq_cr, zcalt, 1);
register_bit!(zq_cr, zq_pd, 0);

register!(zqn_pr0, ZQnPR0, RW, u32);
register_bit!(zqn_pr0, pd_drv_zden, 31);
register_bit!(zqn_pr0, pu_drv_zden, 30);
register_bit!(zqn_pr0, pd_odt_zden, 29);
register_bit!(zqn_pr0, pu_odt_zden, 28);
register_bit!(zqn_pr0, zseg_byp, 27);
register_bits!(zqn_pr0, zle_mode, u8, 25, 26);
register_bits!(zqn_pr0, odt_adjust, u8, 22, 24);
register_bits!(zqn_pr0, pd_drv_adjust, u8, 19, 21);
register_bits!(zqn_pr0, pu_drv_adjust, u8, 16, 18);
register_bits!(zqn_pr0, zprog_dram_odt, u8, 12, 15);
register_bits!(zqn_pr0, zprog_host_odt, u8, 8, 11);
register_bits!(zqn_pr0, zprog_asym_drv_pd, u8, 4, 7);
register_bits!(zqn_pr0, zprog_asym_drv_pu, u8, 0, 3);

register!(zqn_pr1, ZQnPR1, RW, u32);
register_bits!(zqn_pr1, pu_ref_sel, u8, 8, 14);
register_bits!(zqn_pr1, pd_ref_sel, u8, 0, 6);

register!(zqn_dr0, ZQnDR0, RO, u32);
register_bits!(zqn_dr0, zdata_pu_drv_result, u16, 16, 25);
register_bits!(zqn_dr0, zdata_pd_drv_result, u16, 0, 9);

register!(zqn_dr1, ZQnDR1, RO, u32);
register_bits!(zqn_dr1, zdata_pu_odt_result, u16, 16, 25);
register_bits!(zqn_dr1, zdata_pd_odt_result, u16, 0, 9);

register!(zqn_or0, ZQnOR0, RW, u32);
register_bits!(zqn_or0, zdata_pu_drv_ovrd, u16, 16, 25);
register_bits!(zqn_or0, zdata_pd_drv_ovrd, u16, 0, 9);

register!(zqn_or1, ZQnOR1, RW, u32);
register_bits!(zqn_or1, zdata_pu_odt_ovrd, u16, 16, 25);
register_bits!(zqn_or1, zdata_pd_odt_ovrd, u16, 0, 9);

register!(zqn_sr, ZQnSR, RO, u32);
register_bit!(zqn_sr, pd_odt_sat, 13);
register_bit!(zqn_sr, pu_odt_sat, 12);
register_bit!(zqn_sr, pd_drv_sat, 11);
register_bit!(zqn_sr, pu_drv_sat, 10);
register_bit!(zqn_sr, z_done, 9);
register_bit!(zqn_sr, z_err, 8);
register_bits_typed!(zqn_sr, opu, u8, ZCalStatus, 6, 7);
register_bits_typed!(zqn_sr, opd, u8, ZCalStatus, 4, 5);
register_bits_typed!(zqn_sr, zpu, u8, ZCalStatus, 2, 3);
register_bits_typed!(zqn_sr, zpd, u8, ZCalStatus, 2, 3);

register!(dxn_gcr0, DXnGCR0, RW, u32);
register_bit!(dxn_gcr0, cal_byp, 31);
register_bit!(dxn_gcr0, mdl_en, 30);
register_bits!(dxn_gcr0, c_odt_shft, u8, 28, 29);
register_bits!(dxn_gcr0, rd_dly, u8, 20, 23);
register_bit!(dxn_gcr0, dqsnse_pdr, 13);
register_bit!(dxn_gcr0, dqsse_pdr, 12);
register_bit!(dxn_gcr0, rtt_oal, 11);
register_bits!(dxn_gcr0, rtt_oh, u8, 9, 10);
register_bits!(dxn_gcr0, cpdr_shft, u8, 7, 8);
register_bit!(dxn_gcr0, dqsr_pd, 6);
register_bit!(dxn_gcr0, dqsg_pdr, 5);
register_bit!(dxn_gcr0, dqsg_odt, 3);
register_bit!(dxn_gcr0, dqsg_oe, 2);

register!(dxn_gcr1, DXnGCR1, RW, u32);
register_bits!(dxn_gcr1, dx_pdr_mode, u16, 16, 31);
register_bit!(dxn_gcr1, qsn_sel, 14);
register_bit!(dxn_gcr1, qs_sel, 13);
register_bit!(dxn_gcr1, oe_en, 12);
register_bit!(dxn_gcr1, pdr_en, 11);
register_bit!(dxn_gcr1, te_en, 10);
register_bit!(dxn_gcr1, ds_en, 9);
register_bit!(dxn_gcr1, dm_en, 8);
register_bits!(dxn_gcr1, dq_en, u8, 0, 7);

register!(dxn_gcr2, DXnGCR2, RW, u32);
register_bits!(dxn_gcr2, dx_oe_mode, u16, 16, 31);
register_bits!(dxn_gcr2, dx_te_mode, u16, 0, 15);

register!(dxn_gcr3, DXnGCR3, RW, u32);
register_bit!(dxn_gcr3, rd_bvt, 29);
register_bit!(dxn_gcr3, wd_bvt, 28);
register_bit!(dxn_gcr3, rg_lvt, 27);
register_bit!(dxn_gcr3, rd_lvt, 26);
register_bit!(dxn_gcr3, wd_lvt, 25);
register_bit!(dxn_gcr3, wl_lvt, 24);
register_bits_typed!(dxn_gcr3, dsn_oe_mode, u8, ModeCtl, 20, 21);
register_bits_typed!(dxn_gcr3, dsn_te_mode, u8, ModeCtl, 18, 19);
register_bits_typed!(dxn_gcr3, dsn_pdr_mode, u8, ModeCtl, 16, 17);
register_bits_typed!(dxn_gcr3, dm_oe_mode, u8, ModeCtl, 14, 15);
register_bits_typed!(dxn_gcr3, dm_te_mode, u8, ModeCtl, 12, 13);
register_bits_typed!(dxn_gcr3, dm_pdr_mode, u8, ModeCtl, 10, 11);
register_bits_typed!(dxn_gcr3, ds_oe_mode, u8, ModeCtl, 6, 7);
register_bits_typed!(dxn_gcr3, ds_te_mode, u8, ModeCtl, 4, 5);
register_bits_typed!(dxn_gcr3, ds_pdr_mode, u8, ModeCtl, 2, 3);

register!(dxn_gcr4, DXnGCR4, RW, u32);
register_bit!(dxn_gcr4, dx_refp_en, 28);
register_bits!(dxn_gcr4, dx_refe_en, u8, 26, 27);
register_bit!(dxn_gcr4, dx_refs_en, 25);
register_bit!(dxn_gcr4, dx_refe_sel_range, 23);
register_bits!(dxn_gcr4, dx_refe_sel, u8, 16, 22);
register_bit!(dxn_gcr4, dx_refs_sel_range, 15);
register_bits!(dxn_gcr4, dx_refs_sel, u8, 8, 14);
register_bits!(dxn_gcr4, dx_refi_en, u8, 2, 5);
register_bits!(dxn_gcr4, dx_refi_mon, u8, 0, 1);

register!(dxn_gcr5, DXnGCR5, RW, u32);
register_bits!(dxn_gcr5, res_24_30, u8, 24, 30);
register_bits!(dxn_gcr5, res_16_22, u8, 16, 22);
register_bits!(dxn_gcr5, dx_refi_sel_r1, u8, 8, 14);
register_bits!(dxn_gcr5, dx_refi_sel_r0, u8, 0, 6);

register!(dxn_gcr6, DXnGCR6, RW, u32);
register_bits!(dxn_gcr6, res_24_29, u8, 24, 29);
register_bits!(dxn_gcr6, res_16_21, u8, 16, 21);
register_bits!(dxn_gcr6, dx_dq_vref_r1, u8, 8, 13);
register_bits!(dxn_gcr6, dx_dq_vref_r0, u8, 0, 5);

register!(dxn_gtr0, DXnGTR0, RW, u32);
register_bits!(dxn_gtr0, wdq_sl, u8, 24, 26);
register_bits!(dxn_gtr0, wl_sl, u8, 16, 19);
register_bits!(dxn_gtr0, dg_sl, u8, 0, 4);

// even values of n
register!(dxn_gsr0, DXnGSR0, RO, u32);
register_bit!(dxn_gsr0, dp_lock, 16);
// other bits excluded for now

register!(dx8_sln_osc, DX8SLnOSC, RW, u32);
register_bits_typed!(dx8_sln_osc, gate_dx_rd_clk, u8, ModeCtl, 28, 29);
register_bits_typed!(dx8_sln_osc, gate_dx_ddr_clk, u8, ModeCtl, 26, 27);
register_bits_typed!(dx8_sln_osc, gate_dx_ctl_clk, u8, ModeCtl, 24, 25);
register_bits!(dx8_sln_osc, clk_level, u8, 22, 23);
register_bit!(dx8_sln_osc, lb_mode, 21);
register_bit!(dx8_sln_osc, lb_gsdqs, 20, WTC);
register_bits!(dx8_sln_osc, lb_gdqs, u8, 18, 19);
register_bit!(dx8_sln_osc, lb_dqss, 17);
register_bit!(dx8_sln_osc, phy_hrst, 16);
register_bit!(dx8_sln_osc, phy_frst, 15);
register_bit!(dx8_sln_osc, dlt_st, 14);
register_bit!(dx8_sln_osc, dlt_mode, 13);
// "reserved" but written to because why the fuck not
register_bits!(dx8_sln_osc, res_11_12, u8, 11, 12);
register_bits!(dx8_sln_osc, osc_wd_dl, u8, 9, 10);
register_bits!(dx8_sln_osc, res_7_8, u8, 7, 8);
register_bits!(dx8_sln_osc, osc_w_dl, u8, 5, 6);
register_bits!(dx8_sln_osc, osc_div, u8, 1, 4);
register_bit!(dx8_sln_osc, osc_en, 0);

// all other pll_crx regs same as above
register!(dx8_sln_pll_cr1, DX8SLnPLLCR1, RW, u32);
register_bits!(dx8_sln_pll_cr1, pll_prog, u16, 6, 21);
register_bit!(dx8_sln_pll_cr1, byp_vreg_cp, 5);
register_bit!(dx8_sln_pll_cr1, byp_vreg_dig, 4);
register_bit!(dx8_sln_pll_cr1, lock_ps, 2);
register_bit!(dx8_sln_pll_cr1, lock_cs, 1);
register_bit!(dx8_sln_pll_cr1, lock_ds, 0);

register!(dx8_sln_dqsctl, DX8SLnDQSCTL, RW, u32);
register_bit!(dx8_sln_dqsctl, rrr_mode, 24);
register_bit!(dx8_sln_dqsctl, wrr_mode, 21);
register_bits!(dx8_sln_dqsctl, dqs_gx, u8, 19, 20);
register_bit!(dx8_sln_dqsctl, lp_pll_pd, 18);
register_bit!(dx8_sln_dqsctl, lp_io_pd, 17);
register_bit!(dx8_sln_dqsctl, qs_cnt_en, 14);
register_bit!(dx8_sln_dqsctl, udq_iom, 13);
register_bits!(dx8_sln_dqsctl, dx_sr, u8, 8, 9);
register_bits!(dx8_sln_dqsctl, dqs_nres, u8, 4, 7);
register_bits!(dx8_sln_dqsctl, dqs_res, u8, 0, 3);

register!(dx8_sln_dxctl2, DX8SLnDXCTL2, RW, u32);
register_bit!(dx8_sln_dxctl2, crd_en, 23);
register_bits!(dx8_sln_dxctl2, pos_oex, u8, 20, 22);
register_bits!(dx8_sln_dxctl2, pre_oex, u8, 18, 19);
register_bit!(dx8_sln_dxctl2, io_ag, 16);
register_bit!(dx8_sln_dxctl2, io_lb, 15);
register_bits!(dx8_sln_dxctl2, lp_wakeup_thrsh, u8, 9, 12);
register_bit!(dx8_sln_dxctl2, rdbi, 8);
register_bit!(dx8_sln_dxctl2, wdbi, 7);
register_bit!(dx8_sln_dxctl2, prf_byp, 6);
register_bits!(dx8_sln_dxctl2, rd_mode, u8, 4, 5);
register_bit!(dx8_sln_dxctl2, dis_rst, 3);
register_bits!(dx8_sln_dxctl2, dqs_glb, u8, 1, 2);

register!(dx8_sln_iocr, DX8SLnIOCR, RW, u32);
register_bits!(dx8_sln_iocr, dx_dac_range, u8, 28, 30);
register_bits!(dx8_sln_iocr, dx_vref_iom, u8, 25, 27);
register_bits!(dx8_sln_iocr, dx_iom, u8, 22, 24);
register_bits!(dx8_sln_iocr, dx_txm, u16, 11, 21);
register_bits!(dx8_sln_iocr, dx_rxm, u16, 0, 10);
