use libregister::{register, register_at, register_bit, register_bits, register_bits_typed};
use volatile_register::{RO, RW};

#[allow(unused)]
#[repr(u8)]
pub enum DeviceConfig {
    Reserved = 0b00,
    X8 = 0b01,
    X16 = 0b10,
    X32 = 0b11,
}

#[allow(unused)]
#[repr(u8)]
pub enum BurstRdwr {
    Len4 = 0b0010,
    Len8 = 0b0100,
    Len16 = 0b1000,
}

#[allow(unused)]
#[repr(u8)]
pub enum DataBusWidth {
    Full = 0b00,
    Half = 0b01,
    Quarter = 0b10,
}

#[repr(u8)]
pub enum RdTrafficClass {
    LPR = 0,
    VPR = 1,
    HPR = 2,
}

#[repr(u8)]
pub enum WrTrafficClass {
    NPW = 0,
    VPW = 1,
}

#[repr(C)]
pub struct RegisterBlock {
    pub mstr: Mstr,
    pub stat: Stat,
    unused1: [u32; 2],
    pub mr_ctrl0: MRCtrl0,
    pub mr_ctrl1: RW<u32>,
    pub mr_stat: RO<u32>,
    pub mr_ctrl2: RW<u32>,
    pub derate_en: DerateEn,
    pub derate_int: DerateInt,
    unused2: [u32; 2],
    pub pwr_ctl: PwrCtl,
    pub pwr_tmg: PwrTmg,
    pub hw_lp_ctl: RW<u32>,
    unused3: [u32; 5],
    pub rfsh_ctl0: RfshCtl0,
    pub rfsh_ctl1: RfshCtl1,
    unused4: [u32; 2],
    pub rfsh_ctl3: RfshCtl3,
    pub rfsh_tmg: RfshTmg,
    unused5: [u32; 2],
    pub ecc_cfg0: EccCfg0,
    pub ecc_cfg1: RW<u32>,
    pub ecc_stat: RO<u32>,
    pub ecc_clr: RW<u32>,
    pub ecc_err_cnt: RO<u32>,
    pub ecc_caddr0: RO<u32>,
    pub ecc_caddr1: RO<u32>,
    pub ecc_csyn0: RO<u32>,
    pub ecc_csyn1: RO<u32>,
    pub ecc_csyn2: RO<u32>,
    pub ecc_bitmask0: RO<u32>,
    pub ecc_bitmask1: RO<u32>,
    pub ecc_bitmask2: RO<u32>,
    pub ecc_uaddr0: RO<u32>,
    pub ecc_uaddr1: RO<u32>,
    pub ecc_usyn0: RO<u32>,
    pub ecc_usyn1: RO<u32>,
    pub ecc_usyn2: RO<u32>,
    pub ecc_poison_addr0: RW<u32>,
    pub ecc_poison_addr1: RW<u32>,
    pub crc_par_ctl0: RW<u32>,
    pub crc_par_ctl1: CrcParCtl1,
    pub crc_par_ctl2: CrcParCtl2,
    pub crc_par_stat: RO<u32>,
    pub init0: Init0,
    pub init1: Init1,
    pub init2: Init2,
    pub init3: Init3,
    pub init4: Init4,
    pub init5: Init5,
    pub init6: Init6,
    pub init7: Init7,
    pub dimm_ctl: DimmCtl,
    pub rank_ctl: RankCtl,
    unused6: [u32; 2],
    pub dram_tmg0: DramTmg0,
    pub dram_tmg1: DramTmg1,
    pub dram_tmg2: DramTmg2,
    pub dram_tmg3: DramTmg3,
    pub dram_tmg4: DramTmg4,
    pub dram_tmg5: DramTmg5,
    pub dram_tmg6: DramTmg6,
    pub dram_tmg7: DramTmg7,
    pub dram_tmg8: DramTmg8,
    pub dram_tmg9: DramTmg9,
    pub dram_tmg10: RW<u32>,
    pub dram_tmg11: DramTmg11,
    pub dram_tmg12: DramTmg12,
    pub dram_tmg13: DramTmg13,
    pub dram_tmg14: DramTmg14,
    unused7: [u32; 17],
    pub zq_ctl0: ZQCtl0,
    pub zq_ctl1: ZQCtl1,
    pub zq_ctl2: ZQCtl2,
    pub zq_stat: ZQStat,
    pub dfi_tmg0: DfiTmg0,
    pub dfi_tmg1: DfiTmg1,
    pub dfi_lp_cfg0: DfiLpCfg0,
    pub dfi_lp_cfg1: DfiLpCfg1,
    pub dfi_upd0: DfiUpd0,
    pub dfi_upd1: DfiUpd1,
    pub dfi_upd2: DfiUpd2,
    unused8: [u32; 1],
    pub dfi_misc: DfiMisc,
    pub dfi_tmg2: DfiTmg2,
    unused9: [u32; 2],
    pub dbi_ctl: DbiCtl,
    unused10: [u32; 15],
    pub addr_map0: AddrMap0,
    pub addr_map1: AddrMap1,
    pub addr_map2: AddrMap2,
    pub addr_map3: AddrMap3,
    pub addr_map4: AddrMap4,
    pub addr_map5: AddrMap5,
    pub addr_map6: AddrMap6,
    pub addr_map7: AddrMap7,
    pub addr_map8: AddrMap8,
    pub addr_map9: AddrMap9,
    pub addr_map10: AddrMap10,
    pub addr_map11: AddrMap11,
    unused11: [u32; 4],
    pub odt_cfg: OdtCfg,
    pub odt_map: OdtMap,
    unused12: [u32; 2],
    pub sched: Sched,
    pub sched1: Sched1,
    unused13: [u32; 1],
    pub perf_hpr1: PerfHPR1,
    unused14: [u32; 1],
    pub perf_lpr1: PerfLPR1,
    unused15: [u32; 1],
    pub perf_wr1: PerfWR1,
    unused16: [u32; 1],
    pub perf_vpr1: PerfVPR1,
    pub perf_vpw1: PerfVPW1,
    unused17: [u32; 1],
    pub dq_map0: DqMap0,
    pub dq_map1: DqMap1,
    pub dq_map2: DqMap2,
    pub dq_map3: DqMap3,
    pub dq_map4: DqMap4,
    pub dq_map5: DqMap5,
    unused18: [u32; 26],
    pub dbg0: Dbg0,
    pub dbg1: Dbg1,
    pub dbg_cam: RO<u32>,
    pub dbg_cmd: DbgCmd,
    pub dbg_status: RO<u32>,
    unused19: [u32; 3],
    pub sw_ctl: SwCtl,
    pub sw_stat: SwStat,
    unused20: [u32; 17],
    pub poison_cfg: RW<u32>,
    pub poison_status: RO<u32>,
    unused21: [u32; 34],
    pub p_stat: PStat,
    pub pc_cfg: PCCfg,
    pub port0_cfg_r: PortCfgR,
    pub port0_cfg_w: PortCfgW,
    unused22: [u32; 33],
    pub port0_ctrl: PortCtrl,
    pub port0_rqos_cfg0: PortRqosCfg0,
    pub port0_rqos_cfg1: PortRqosCfg1,
    pub port0_wqos_cfg0: PortWqosCfg0,
    pub port0_wqos_cfg1: PortWqosCfg1,
    unused23: [u32; 4],
    pub port1_cfg_r: PortCfgR,
    pub port1_cfg_w: PortCfgW,
    unused24: [u32; 33],
    pub port1_ctrl: PortCtrl,
    pub port1_rqos_cfg0: Port12RqosCfg0,
    pub port1_rqos_cfg1: PortRqosCfg1,
    pub port1_wqos_cfg0: PortWqosCfg0,
    pub port1_wqos_cfg1: PortWqosCfg1,
    unused25: [u32; 4],
    pub port2_cfg_r: PortCfgR,
    pub port2_cfg_w: PortCfgW,
    unused26: [u32; 33],
    pub port2_ctrl: PortCtrl,
    pub port2_rqos_cfg0: Port12RqosCfg0,
    pub port2_rqos_cfg1: PortRqosCfg1,
    pub port2_wqos_cfg0: PortWqosCfg0,
    pub port2_wqos_cfg1: PortWqosCfg1,
    unused27: [u32; 4],
    pub port3_cfg_r: PortCfgR,
    pub port3_cfg_w: PortCfgW,
    unused28: [u32; 33],
    pub port3_ctrl: PortCtrl,
    pub port3_rqos_cfg0: PortRqosCfg0,
    pub port3_rqos_cfg1: PortRqosCfg1,
    pub port3_wqos_cfg0: PortWqosCfg0,
    pub port3_wqos_cfg1: PortWqosCfg1,
    unused29: [u32; 4],
    pub port4_cfg_r: PortCfgR,
    pub port4_cfg_w: PortCfgW,
    unused30: [u32; 33],
    pub port4_ctrl: PortCtrl,
    pub port4_rqos_cfg0: PortRqosCfg0,
    pub port4_rqos_cfg1: PortRqosCfg1,
    pub port4_wqos_cfg0: PortWqosCfg0,
    pub port4_wqos_cfg1: PortWqosCfg1,
    unused31: [u32; 4],
    pub port5_cfg_r: PortCfgR,
    pub port5_cfg_w: PortCfgW,
    unused32: [u32; 33],
    pub port5_ctrl: PortCtrl,
    pub port5_rqos_cfg0: PortRqosCfg0,
    pub port5_rqos_cfg1: PortRqosCfg1,
    pub port5_wqos_cfg0: PortWqosCfg0,
    pub port5_wqos_cfg1: PortWqosCfg1,
    unused33: [u32; 444],
    pub sar_base0: SarBase,
    pub sar_size0: SarSize,
    pub sar_base1: SarBase,
    pub sar_size1: SarSize,
    unused34: [u32; 1092],
    pub derate_int_shadow: RW<u32>,
    unused35: [u32; 10],
    pub rfsh_ctl0_shadow: RW<u32>,
    unused36: [u32; 4],
    pub rfsh_tmg_shadow: RW<u32>,
    unused37: [u32; 29],
    pub init3_shadow: RW<u32>,
    pub init4_shadow: RW<u32>,
    unused38: [u32; 1],
    pub init6_shadow: RW<u32>,
    pub init7_shadow: RW<u32>,
    unused39: [u32; 4],
    pub dram_tmg0_shadow: RW<u32>,
    pub dram_tmg1_shadow: RW<u32>,
    pub dram_tmg2_shadow: RW<u32>,
    pub dram_tmg3_shadow: RW<u32>,
    pub dram_tmg4_shadow: RW<u32>,
    pub dram_tmg5_shadow: RW<u32>,
    pub dram_tmg6_shadow: RW<u32>,
    pub dram_tmg7_shadow: RW<u32>,
    pub dram_tmg8_shadow: RW<u32>,
    pub dram_tmg9_shadow: RW<u32>,
    pub dram_tmg10_shadow: RW<u32>,
    pub dram_tmg11_shadow: RW<u32>,
    pub dram_tmg12_shadow: RW<u32>,
    pub dram_tmg13_shadow: RW<u32>,
    pub dram_tmg14_shadow: RW<u32>,
    unused40: [u32; 17],
    pub zq_ctl0_shadow: RW<u32>,
    unused41: [u32; 3],
    pub dfi_tmg0_shadow: DfiTmg0,
    pub dfi_tmg1_shadow: RW<u32>,
    unused42: [u32; 7],
    pub dfi_tmg2_shadow: RW<u32>,
    unused43: [u32; 34],
    pub odt_cfg_shadow: RW<u32>,
}
register_at!(RegisterBlock, 0xFD07_0000, ddrc);

register!(mstr, Mstr, RW, u32);
register_bits_typed!(mstr, device_config, u8, DeviceConfig, 30, 31);
register_bit!(mstr, freq_mode, 29);
register_bits!(mstr, active_ranks, u8, 24, 25);
register_bits_typed!(mstr, burst_rdwr, u8, BurstRdwr, 16, 19);
register_bit!(mstr, dll_off_mode, 15);
register_bits_typed!(mstr, data_bus_width, u8, DataBusWidth, 12, 13);
register_bit!(mstr, en_2t_timing_mode, 10);
register_bit!(mstr, lpddr4, 5);
register_bit!(mstr, ddr4, 4);
register_bit!(mstr, lpddr3, 3);
register_bit!(mstr, ddr3, 0);

register!(stat, Stat, RO, u32);
register_bits!(stat, selfref_state, u8, 8, 9);
register_bits!(stat, selfref_type, u8, 4, 5);
register_bits!(stat, operating_mode, u8, 0, 2);

register!(mr_ctrl0, MRCtrl0, RW, u32);
register_bit!(mr_ctrl0, mr_wr, 31);
register_bits!(mr_ctrl0, mr_addr, u8, 12, 15);
register_bits!(mr_ctrl0, mr_rank, u8, 4, 5);
register_bit!(mr_ctrl0, sw_init_int, 3);
register_bit!(mr_ctrl0, pda_en, 2);
register_bit!(mr_ctrl0, mpr_en, 1);
register_bit!(mr_ctrl0, mr_type, 0);

register!(derate_en, DerateEn, RW, u32);
register_bits!(derate_en, rc_derate_value, u8, 8, 9);
register_bits!(derate_en, derate_byte, u8, 4, 7);
register_bit!(derate_en, derate_value, 1);
register_bit!(derate_en, derate_enable, 0);

register!(derate_interval, DerateInt, RW, u32);
register_bits!(derate_interval, mr4_read_interval, u32, 0, 31);

register!(power_ctl, PwrCtl, RW, u32);
register_bit!(power_ctl, stay_in_self_ref, 6);
register_bit!(power_ctl, self_ref_sw, 5);
register_bit!(power_ctl, mpsm_en, 4);
register_bit!(power_ctl, en_dfi_dram_clk_disable, 3);
register_bit!(power_ctl, deep_powerdown_en, 2);
register_bit!(power_ctl, powerdown_en, 1);
register_bit!(power_ctl, self_ref_en, 0);

register!(pwr_tmg, PwrTmg, RW, u32);
register_bits!(pwr_tmg, self_ref_to_x32, u8, 16, 23);
register_bits!(pwr_tmg, t_dpd_x4096, u8, 8, 15);
register_bits!(pwr_tmg, powerdown_to_x32, u8, 0, 4);

register!(rfsh_ctl0, RfshCtl0, RW, u32);
register_bits!(rfsh_ctl0, refresh_margin, u8, 20, 23);
register_bits!(rfsh_ctl0, refresh_to_x32, u8, 12, 16);
register_bits!(rfsh_ctl0, refresh_burst, u8, 4, 8);
register_bit!(rfsh_ctl0, per_bank_refresh, 2);

register!(rfsh_ctl1, RfshCtl1, RW, u32);
register_bits!(rfsh_ctl1, timer1_start_value_x32, u16, 16, 27);
register_bits!(rfsh_ctl1, timer0_start_value_x32, u16, 0, 11);

register!(rfsh_ctl3, RfshCtl3, RW, u32);
register_bits!(rfsh_ctl3, refresh_mode, u8, 4, 6);
register_bit!(rfsh_ctl3, refresh_update_level, 1);
register_bit!(rfsh_ctl3, disable_auto_refresh, 0);

register!(rfsh_tmg, RfshTmg, RW, u32);
register_bits!(rfsh_tmg, t_rfc_nom_x32, u16, 16, 27);
register_bit!(rfsh_tmg, lpddr3_trefbw_en, 15);
register_bits!(rfsh_tmg, t_rfc_min, u16, 0, 9);

register!(ecc_cfg0, EccCfg0, RW, u32);
register_bit!(ecc_cfg0, dis_scrub, 4);
register_bits!(ecc_cfg0, ecc_mode, u8, 0, 2);

register!(crc_par_ctl1, CrcParCtl1, RW, u32);
register_bits!(crc_par_ctl1, dfi_t_phy_rdlat, u8, 24, 29);
register_bit!(crc_par_ctl1, alert_wait_for_sw, 9);
register_bit!(crc_par_ctl1, crc_parity_retry_enable, 8);
register_bit!(crc_par_ctl1, crc_inc_dm, 7);
register_bit!(crc_par_ctl1, crc_enable, 4);
register_bit!(crc_par_ctl1, parity_enable, 0);

register!(crc_par_ctl2, CrcParCtl2, RW, u32);
register_bits!(crc_par_ctl2, t_par_alert_pw_max, u16, 16, 24);
register_bits!(crc_par_ctl2, t_crc_alert_pw_max, u8, 8, 12);
register_bits!(crc_par_ctl2, retry_fifo_max_hold_timer_x4, u8, 0, 5);

register!(init0, Init0, RW, u32);
register_bits!(init0, skip_dram_init, u8, 30, 31);
register_bits!(init0, post_cke_x1024, u16, 16, 25);
register_bits!(init0, pre_cke_x1024, u16, 0, 11);

register!(init1, Init1, RW, u32);
register_bits!(init1, dram_rstn_x1024, u16, 16, 24);
register_bits!(init1, final_wait_x32, u8, 8, 14);
register_bits!(init1, pre_ocd_x32, u8, 0, 3);

register!(init2, Init2, RW, u32);
register_bits!(init2, idle_after_reset_x32, u8, 8, 15);
register_bits!(init2, min_stable_clock_x1, u8, 0, 3);

register!(init3, Init3, RW, u32);
// DDR3/4: MR0
// LPDDR3/4: MR1
register_bits!(init3, mr, u16, 16, 31);
// DDR3/4: MR1
// Set bit 7 to 0.
// LPDDR3/4: MR2
register_bits!(init3, emr, u16, 0, 15);

register!(init4, Init4, RW, u32);
// DDR3/4: MR2
// LPDDR3/4: MR3
register_bits!(init4, emr2, u16, 16, 31);
// DDR3/4: MR3
// LPDDR4: MR13
register_bits!(init4, emr3, u16, 0, 15);

register!(init5, Init5, RW, u32);
register_bits!(init5, dev_zqinit_x32, u8, 16, 23);
register_bits!(init5, max_auto_init_x1024, u16, 0, 9);

register!(init6, Init6, RW, u32);
register_bits!(init6, mr4, u16, 16, 31);
register_bits!(init6, mr5, u16, 0, 15);

register!(init7, Init7, RW, u32);
register_bits!(init7, mr6, u16, 16, 31);

register!(dimm_ctl, DimmCtl, RW, u32);
register_bit!(dimm_ctl, dimm_dis_bg_mirroring, 5);
register_bit!(dimm_ctl, mrs_bg1_en, 4);
register_bit!(dimm_ctl, mrs_a17_en, 3);
register_bit!(dimm_ctl, dimm_output_inv_en, 2);
register_bit!(dimm_ctl, dimm_addr_mirr_en, 1);
register_bit!(dimm_ctl, dimm_stagger_cs_en, 0);

register!(rank_ctl, RankCtl, RW, u32);
register_bits!(rank_ctl, diff_rank_wr_gap, u8, 8, 11);
register_bits!(rank_ctl, diff_rank_rd_gap, u8, 4, 7);
register_bits!(rank_ctl, max_rank_rd, u8, 0, 3);

register!(dram_tmg0, DramTmg0, RW, u32);
register_bits!(dram_tmg0, wr2pre, u8, 24, 30);
register_bits!(dram_tmg0, t_faw, u8, 16, 21);
register_bits!(dram_tmg0, t_ras_max, u8, 8, 14);
register_bits!(dram_tmg0, t_ras_min, u8, 0, 5);

register!(dram_tmg1, DramTmg1, RW, u32);
register_bits!(dram_tmg1, t_xp, u8, 16, 20);
register_bits!(dram_tmg1, rd2pre, u8, 8, 12);
register_bits!(dram_tmg1, t_rc, u8, 0, 6);

register!(dram_tmg2, DramTmg2, RW, u32);
register_bits!(dram_tmg2, write_latency, u8, 24, 29);
register_bits!(dram_tmg2, read_latency, u8, 16, 21);
register_bits!(dram_tmg2, rd2wr, u8, 8, 13);
register_bits!(dram_tmg2, wr2rd, u8, 0, 5);

register!(dram_tmg3, DramTmg3, RW, u32);
register_bits!(dram_tmg3, t_mrw, u16, 20, 29);
register_bits!(dram_tmg3, t_mrd, u8, 12, 17);
register_bits!(dram_tmg3, t_mod, u16, 0, 9);

register!(dram_tmg4, DramTmg4, RW, u32);
register_bits!(dram_tmg4, t_rcd, u8, 24, 28);
register_bits!(dram_tmg4, t_ccd, u8, 16, 19);
register_bits!(dram_tmg4, t_rrd, u8, 8, 11);
register_bits!(dram_tmg4, t_rp, u8, 0, 4);

register!(dram_tmg5, DramTmg5, RW, u32);
register_bits!(dram_tmg5, t_cksrx, u8, 24, 27);
register_bits!(dram_tmg5, t_cksre, u8, 16, 19);
register_bits!(dram_tmg5, t_ckesr, u8, 8, 13);
register_bits!(dram_tmg5, t_cke, u8, 0, 4);

register!(dram_tmg6, DramTmg6, RW, u32);
register_bits!(dram_tmg6, t_ckdpde, u8, 24, 27);
register_bits!(dram_tmg6, t_ckdpdx, u8, 16, 19);
register_bits!(dram_tmg6, t_ckcsx, u8, 0, 3);

register!(dram_tmg7, DramTmg7, RW, u32);
register_bits!(dram_tmg7, t_ckpde, u8, 8, 11);
register_bits!(dram_tmg7, t_ckpdx, u8, 0, 3);

register!(dram_tmg8, DramTmg8, RW, u32);
register_bits!(dram_tmg8, t_xs_fast_x32, u8, 24, 30);
register_bits!(dram_tmg8, t_xs_abort_x32, u8, 16, 22);
register_bits!(dram_tmg8, t_xs_dll_x32, u8, 8, 14);
register_bits!(dram_tmg8, t_xs_x32, u8, 0, 6);

register!(dram_tmg9, DramTmg9, RW, u32);
register_bit!(dram_tmg9, ddr4_wr_preamble, 30);
register_bits!(dram_tmg9, t_ccd_s, u8, 16, 18);
register_bits!(dram_tmg9, t_rrd_s, u8, 8, 11);
register_bits!(dram_tmg9, wr2rd_s, u8, 0, 5);

register!(dram_tmg11, DramTmg11, RW, u32);
register_bits!(dram_tmg11, post_mpsm_gap_x32, u8, 24, 30);
register_bits!(dram_tmg11, t_mpx_lh, u8, 16, 20);
register_bits!(dram_tmg11, t_mpx_s, u8, 8, 9);
register_bits!(dram_tmg11, t_ckmpe, u8, 0, 4);

register!(dram_tmg12, DramTmg12, RW, u32);
register_bits!(dram_tmg12, t_cmdcke, u8, 16, 17);
register_bits!(dram_tmg12, t_ckehcmd, u8, 8, 11);
register_bits!(dram_tmg12, t_mrd_pda, u8, 0, 4);

register!(dram_tmg13, DramTmg13, RW, u32);
register_bits!(dram_tmg13, odtloff, u8, 24, 30);
register_bits!(dram_tmg13, t_ccd_mw, u8, 16, 21);
register_bits!(dram_tmg13, t_ppd, u8, 0, 2);

register!(dram_tmg14, DramTmg14, RW, u32);
register_bits!(dram_tmg14, t_xsr, u16, 0, 11);

register!(zq_ctl0, ZQCtl0, RW, u32);
register_bit!(zq_ctl0, dis_auto_zq, 31);
register_bit!(zq_ctl0, dis_srx_zqcl, 30);
register_bit!(zq_ctl0, zq_resistor_shared, 29);
register_bit!(zq_ctl0, dis_mpsmx_zqcl, 28);
register_bits!(zq_ctl0, t_zq_long_nop, u16, 16, 26);
register_bits!(zq_ctl0, t_zq_short_nop, u16, 0, 9);

register!(zq_ctl1, ZQCtl1, RW, u32);
register_bits!(zq_ctl1, t_zq_reset_nop, u16, 20, 29);
register_bits!(zq_ctl1, t_zq_short_interval_x1024, u32, 0, 19);

register!(zq_ctl2, ZQCtl2, RW, u32);
// self-clearing
register_bit!(zq_ctl2, zq_reset, 0);

register!(zq_stat, ZQStat, RO, u32);
register_bit!(zq_stat, zq_reset_busy, 0);

register!(dfi_tmg0, DfiTmg0, RW, u32);
register_bits!(dfi_tmg0, dfi_t_ctrl_delay, u8, 24, 28);
register_bit!(dfi_tmg0, dfi_rddata_use_sdr, 23);
register_bits!(dfi_tmg0, dfi_t_rddata_en, u8, 16, 21);
register_bit!(dfi_tmg0, dfi_wrdata_use_sdr, 15);
register_bits!(dfi_tmg0, dfi_tphy_wrdata, u8, 8, 13);
register_bits!(dfi_tmg0, dfi_tphy_wrlat, u8, 0, 5);

register!(dfi_tmg1, DfiTmg1, RW, u32);
register_bits!(dfi_tmg1, dfi_t_cmd_lat, u8, 28, 31);
register_bits!(dfi_tmg1, dfi_t_parin_lat, u8, 24, 25);
register_bits!(dfi_tmg1, dfi_t_wrdata_delay, u8, 16, 20);
register_bits!(dfi_tmg1, dfi_t_dram_clk_disable, u8, 8, 11);
register_bits!(dfi_tmg1, dfi_t_dram_clk_enable, u8, 0, 3);

register!(dfi_lp_cfg0, DfiLpCfg0, RW, u32);
register_bits!(dfi_lp_cfg0, dfi_tlp_resp, u8, 24, 27);
register_bits!(dfi_lp_cfg0, dfi_lp_wakeup_dpd, u8, 20, 23);
register_bit!(dfi_lp_cfg0, dfi_lp_en_dpd, 16);
register_bits!(dfi_lp_cfg0, dfi_lp_wakeup_sr, u8, 12, 15);
register_bit!(dfi_lp_cfg0, dfi_lp_en_sr, 8);
register_bits!(dfi_lp_cfg0, dfi_lp_wakeup_pd, u8, 4, 7);
register_bit!(dfi_lp_cfg0, dfi_lp_en_pd, 0);

register!(dfi_lp_cfg1, DfiLpCfg1, RW, u32);
register_bits!(dfi_lp_cfg1, dfi_lp_wakeup_mpsm, u8, 4, 7);
register_bit!(dfi_lp_cfg1, dfi_lp_en_mpsm, 0);

register!(dfi_upd0, DfiUpd0, RW, u32);
register_bit!(dfi_upd0, dis_auto_ctrlupd, 31);
register_bit!(dfi_upd0, dis_auto_ctrlupd_srx, 30);
register_bits!(dfi_upd0, dfi_t_ctrlup_max, u16, 16, 25);
register_bits!(dfi_upd0, dfi_t_ctrlup_min, u16, 0, 9);

register!(dfi_upd1, DfiUpd1, RW, u32);
register_bits!(dfi_upd1, dfi_t_ctrlupd_interval_min_x1024, u8, 16, 23);
register_bits!(dfi_upd1, dfi_t_ctrlupd_interval_max_x1024, u8, 0, 7);

register!(dfi_upd2, DfiUpd2, RW, u32);
register_bit!(dfi_upd2, dfi_phyupd_en, 31);

register!(dfi_misc, DfiMisc, RW, u32);
register_bit!(dfi_misc, dfi_data_cs_polarity, 2);
register_bit!(dfi_misc, phy_dbi_mode, 1);
register_bit!(dfi_misc, dfi_init_complete_en, 0);

register!(dfi_tmg2, DfiTmg2, RW, u32);
register_bits!(dfi_tmg2, dfi_tphy_rdcslat, u8, 8, 13);
register_bits!(dfi_tmg2, dfi_tphy_wrcslat, u8, 0, 5);

register!(dbi_ctl, DbiCtl, RW, u32);
register_bit!(dbi_ctl, rd_dbi_en, 2);
register_bit!(dbi_ctl, wr_dbi_en, 1);
register_bit!(dbi_ctl, dm_en, 0);

register!(addr_map0, AddrMap0, RW, u32);
register_bits!(addr_map0, addrmap_cs_bit0, u8, 0, 4);

register!(addr_map1, AddrMap1, RW, u32);
register_bits!(addr_map1, addrmap_bank_b2, u8, 16, 20);
register_bits!(addr_map1, addrmap_bank_b1, u8, 8, 12);
register_bits!(addr_map1, addrmap_bank_b0, u8, 0, 4);

register!(addr_map2, AddrMap2, RW, u32);
register_bits!(addr_map2, addrmap_col_b5, u8, 24, 27);
register_bits!(addr_map2, addrmap_col_b4, u8, 16, 19);
register_bits!(addr_map2, addrmap_col_b3, u8, 8, 11);
register_bits!(addr_map2, addrmap_col_b2, u8, 0, 3);

register!(addr_map3, AddrMap3, RW, u32);
register_bits!(addr_map3, addrmap_col_b9, u8, 24, 27);
register_bits!(addr_map3, addrmap_col_b8, u8, 16, 19);
register_bits!(addr_map3, addrmap_col_b7, u8, 8, 11);
register_bits!(addr_map3, addrmap_col_b6, u8, 0, 3);

register!(addr_map4, AddrMap4, RW, u32);
register_bits!(addr_map4, addrmap_col_b11, u8, 8, 11);
register_bits!(addr_map4, addrmap_col_b10, u8, 0, 3);

register!(addr_map5, AddrMap5, RW, u32);
register_bits!(addr_map5, addrmap_row_b11, u8, 24, 27);
register_bits!(addr_map5, addrmap_row_b2_10, u8, 16, 19);
register_bits!(addr_map5, addrmap_row_b1, u8, 8, 11);
register_bits!(addr_map5, addrmap_row_b0, u8, 0, 3);

register!(addr_map6, AddrMap6, RW, u32);
register_bit!(addr_map6, lpddr3_6gb_12gb, 31);
register_bits!(addr_map6, addrmap_row_b15, u8, 24, 27);
register_bits!(addr_map6, addrmap_row_b14, u8, 16, 19);
register_bits!(addr_map6, addrmap_row_b13, u8, 8, 11);
register_bits!(addr_map6, addrmap_row_b12, u8, 0, 3);

register!(addr_map7, AddrMap7, RW, u32);
register_bits!(addr_map7, addrmap_row_b17, u8, 8, 11);
register_bits!(addr_map7, addrmap_row_b16, u8, 0, 3);

register!(addr_map8, AddrMap8, RW, u32);
register_bits!(addr_map8, addrmap_bg_b1, u8, 8, 12);
register_bits!(addr_map8, addrmap_bg_b0, u8, 0, 4);

register!(addr_map9, AddrMap9, RW, u32);
register_bits!(addr_map9, addrmap_row_b5, u8, 24, 27);
register_bits!(addr_map9, addrmap_row_b4, u8, 16, 19);
register_bits!(addr_map9, addrmap_row_b3, u8, 8, 11);
register_bits!(addr_map9, addrmap_row_b2, u8, 0, 3);

register!(addr_map10, AddrMap10, RW, u32);
register_bits!(addr_map10, addrmap_row_b9, u8, 24, 27);
register_bits!(addr_map10, addrmap_row_b8, u8, 16, 19);
register_bits!(addr_map10, addrmap_row_b7, u8, 8, 11);
register_bits!(addr_map10, addrmap_row_b6, u8, 0, 3);

register!(addr_map11, AddrMap11, RW, u32);
register_bits!(addr_map11, addrmap_row_b10, u8, 0, 3);

register!(odt_cfg, OdtCfg, RW, u32);
register_bits!(odt_cfg, wr_odt_hold, u8, 24, 27);
register_bits!(odt_cfg, wr_odt_delay, u8, 16, 20);
register_bits!(odt_cfg, rd_odt_hold, u8, 8, 11);
register_bits!(odt_cfg, rd_odt_delay, u8, 2, 6);

register!(odt_map, OdtMap, RW, u32);
register_bits!(odt_map, rank1_rd_odt, u8, 12, 13);
register_bits!(odt_map, rank1_wr_odt, u8, 8, 9);
register_bits!(odt_map, rank0_rd_odt, u8, 4, 5);
register_bits!(odt_map, rank0_wr_odt, u8, 0, 1);

register!(sched, Sched, RW, u32);
register_bits!(sched, rdwr_idle_gap, u8, 24, 30);
register_bits!(sched, go2critical_hysteresis, u8, 16, 23);
register_bits!(sched, lpr_num_entries, u8, 8, 13);
register_bit!(sched, page_close, 2);
register_bit!(sched, prefer_write, 1);
register_bit!(sched, force_low_pri_n, 0);

register!(sched1, Sched1, RW, u32);
register_bits!(sched1, page_close_timer, u8, 0, 7);

register!(perf_hpr1, PerfHPR1, RW, u32);
register_bits!(perf_hpr1, hpr_xact_run_length, u8, 24, 31);
register_bits!(perf_hpr1, hpr_max_starve, u16, 0, 15);

register!(perf_lpr1, PerfLPR1, RW, u32);
register_bits!(perf_lpr1, lpr_xact_run_length, u8, 24, 31);
register_bits!(perf_lpr1, lpr_max_starve, u16, 0, 15);

register!(perf_wr1, PerfWR1, RW, u32);
register_bits!(perf_wr1, w_xact_run_length, u8, 24, 31);
register_bits!(perf_wr1, w_max_starve, u16, 0, 15);

register!(perf_vpr1, PerfVPR1, RW, u32);
register_bits!(perf_vpr1, vpr_timeout_range, u16, 0, 10);

register!(perf_vpw1, PerfVPW1, RW, u32);
register_bits!(perf_vpw1, vpw_timeout_range, u16, 0, 10);

register!(dq_map0, DqMap0, RW, u32);
register_bits!(dq_map0, dq_nibble_map_12_15, u8, 24, 31);
register_bits!(dq_map0, dq_nibble_map_8_11, u8, 16, 23);
register_bits!(dq_map0, dq_nibble_map_4_7, u8, 8, 15);
register_bits!(dq_map0, dq_nibble_map_0_3, u8, 0, 7);

register!(dq_map1, DqMap1, RW, u32);
register_bits!(dq_map1, dq_nibble_map_28_31, u8, 24, 31);
register_bits!(dq_map1, dq_nibble_map_24_27, u8, 16, 23);
register_bits!(dq_map1, dq_nibble_map_20_23, u8, 8, 15);
register_bits!(dq_map1, dq_nibble_map_16_19, u8, 0, 7);

register!(dq_map2, DqMap2, RW, u32);
register_bits!(dq_map2, dq_nibble_map_44_47, u8, 24, 31);
register_bits!(dq_map2, dq_nibble_map_40_43, u8, 16, 23);
register_bits!(dq_map2, dq_nibble_map_36_39, u8, 8, 15);
register_bits!(dq_map2, dq_nibble_map_32_35, u8, 0, 7);

register!(dq_map3, DqMap3, RW, u32);
register_bits!(dq_map3, dq_nibble_map_60_63, u8, 24, 31);
register_bits!(dq_map3, dq_nibble_map_56_59, u8, 16, 23);
register_bits!(dq_map3, dq_nibble_map_52_55, u8, 8, 15);
register_bits!(dq_map3, dq_nibble_map_48_51, u8, 0, 7);

register!(dq_map4, DqMap4, RW, u32);
register_bits!(dq_map4, dq_nibble_map_cb_4_7, u8, 8, 15);
register_bits!(dq_map4, dq_nibble_map_cb_0_3, u8, 0, 7);

register!(dq_map5, DqMap5, RW, u32);
register_bit!(dq_map5, dis_dq_rank_swap, 0);

register!(dbg0, Dbg0, RW, u32);
register_bit!(dbg0, dis_collision_page_opt, 4);
register_bit!(dbg0, dis_wc, 0);

register!(dbg1, Dbg1, RW, u32);
register_bit!(dbg1, dis_hif, 1);
register_bit!(dbg1, dis_dq, 0);

register!(dbg_cmd, DbgCmd, RW, u32);
register_bit!(dbg_cmd, hw_ref_zq_en, 31);
register_bit!(dbg_cmd, ctrl_upd, 5);
register_bit!(dbg_cmd, zq_calib_short, 4);
register_bit!(dbg_cmd, rank1_refresh, 1);
register_bit!(dbg_cmd, rank0_refresh, 0);

register!(sw_ctl, SwCtl, RW, u32);
register_bit!(sw_ctl, sw_done, 0);

register!(sw_stat, SwStat, RW, u32);
register_bit!(sw_stat, sw_done_ack, 0);

register!(port_status, PStat, RO, u32);
register_bit!(port_status, port5_wr_busy, 21);
register_bit!(port_status, port4_wr_busy, 20);
register_bit!(port_status, port3_wr_busy, 19);
register_bit!(port_status, port2_wr_busy, 18);
register_bit!(port_status, port1_wr_busy, 17);
register_bit!(port_status, port0_wr_busy, 16);
register_bit!(port_status, port5_rd_busy, 5);
register_bit!(port_status, port4_rd_busy, 4);
register_bit!(port_status, port3_rd_busy, 3);
register_bit!(port_status, port2_rd_busy, 2);
register_bit!(port_status, port1_rd_busy, 1);
register_bit!(port_status, port0_rd_busy, 0);

register!(port_common_cfg, PCCfg, RW, u32);
register_bit!(port_common_cfg, bl_exp_mode, 8);
register_bit!(port_common_cfg, pagematch_limit, 4);
register_bit!(port_common_cfg, go2critical_en, 0);

register!(port_cfg_r, PortCfgR, RW, u32);
register_bit!(port_cfg_r, rd_port_pagematch_en, 14);
register_bit!(port_cfg_r, rd_port_urgent_en, 13);
register_bit!(port_cfg_r, rd_port_aging_en, 12);
register_bits!(port_cfg_r, rd_port_priority, u16, 0, 9);

register!(port_cfg_w, PortCfgW, RW, u32);
register_bit!(port_cfg_w, wr_port_pagematch_en, 14);
register_bit!(port_cfg_w, wr_port_urgent_en, 13);
register_bit!(port_cfg_w, wr_port_aging_en, 12);
register_bits!(port_cfg_w, wr_port_priority, u16, 0, 9);

register!(port_ctrl, PortCtrl, RW, u32);
register_bit!(port_ctrl, port_en, 0);

register!(port_rqos_cfg0, PortRqosCfg0, RW, u32);
register_bits_typed!(port_rqos_cfg0, rqos_map_region1, u8, RdTrafficClass, 20, 21);
register_bits_typed!(port_rqos_cfg0, rqos_map_region0, u8, RdTrafficClass, 16, 17);
register_bits!(port_rqos_cfg0, rqos_map_level1, u8, 0, 3);

// ports 1 & 2 have an extra region, apparently
register!(port12_rqos_cfg0, Port12RqosCfg0, RW, u32);
register_bits_typed!(port12_rqos_cfg0, rqos_map_region2, u8, RdTrafficClass, 24, 25);
register_bits_typed!(port12_rqos_cfg0, rqos_map_region1, u8, RdTrafficClass, 20, 21);
register_bits_typed!(port12_rqos_cfg0, rqos_map_region0, u8, RdTrafficClass, 16, 17);
register_bits!(port12_rqos_cfg0, rqos_map_level2, u8, 8, 11);
register_bits!(port12_rqos_cfg0, rqos_map_level1, u8, 0, 3);

register!(port_rqos_cfg1, PortRqosCfg1, RW, u32);
register_bits!(port_rqos_cfg1, rqos_map_timeoutr, u16, 16, 26);
register_bits!(port_rqos_cfg1, rqos_map_timeoutb, u16, 0, 10);

register!(port_wqos_cfg0, PortWqosCfg0, RW, u32);
register_bits_typed!(port_wqos_cfg0, wqos_map_region1, u8, WrTrafficClass, 20, 21);
register_bits_typed!(port_wqos_cfg0, wqos_map_region0, u8, WrTrafficClass, 16, 17);
register_bits!(port_wqos_cfg0, wqos_map_level1, u8, 0, 3);

register!(port_wqos_cfg1, PortWqosCfg1, RW, u32);
register_bits!(port_wqos_cfg1, wqos_map_timeout, u16, 0, 10);

register!(sar_base, SarBase, RW, u32);
register_bits!(sar_base, base_addr, u16, 0, 8);

register!(sar_size, SarSize, RW, u32);
register_bits!(sar_size, nblocks, u8, 0, 7);
