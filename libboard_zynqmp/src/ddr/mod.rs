//! DDR initialization and configuration
use libcortex_a53::mmu;
use libcortex_a53::regs::Shareability;
use libm::ceilf;
use libregister::{RegisterR, RegisterRW, RegisterW};

use crate::ddr::regs::{
    Dbg0, DbgCmd, DqMap5, DramTmg6, DramTmg7, Init2, PCCfg, PerfLPR1, PerfWR1, Sched,
};

use self::phy::DdrPhy;
use self::regs::{
    AddrMap0, AddrMap1, AddrMap10, AddrMap11, AddrMap2, AddrMap3, AddrMap4, AddrMap5, AddrMap6,
    AddrMap7, AddrMap8, AddrMap9, BurstRdwr, CrcParCtl1, CrcParCtl2, DataBusWidth, DbiCtl,
    DerateEn, DerateInt, DeviceConfig, DfiLpCfg0, DfiLpCfg1, DfiMisc, DfiTmg0, DfiTmg1, DfiTmg2,
    DfiUpd0, DfiUpd1, DimmCtl, DqMap0, DqMap1, DqMap2, DqMap3, DqMap4, DramTmg0, DramTmg1,
    DramTmg11, DramTmg12, DramTmg2, DramTmg3, DramTmg4, DramTmg5, DramTmg8, DramTmg9, EccCfg0,
    Init0, Init1, Init3, Init4, Init5, Init6, Init7, MRCtrl0, Mstr, OdtCfg, OdtMap, Port12RqosCfg0,
    PortCfgR, PortCfgW, PortCtrl, PortRqosCfg0, PortRqosCfg1, PortWqosCfg0, PortWqosCfg1, PwrCtl,
    PwrTmg, RankCtl, RdTrafficClass, RegisterBlock, RfshCtl0, RfshCtl1, RfshCtl3, RfshTmg, SarBase,
    SarSize, SwCtl, ZQCtl0, ZQCtl1,
};
use self::spd::{DeviceType, FineGranularityRefMode, GeneralConfig, ModuleConfig};

use super::slcr::{common::Unlocked, crf_apb};
use super::{print, println};
pub mod phy;
pub mod regs;
pub mod spd;

#[cfg(feature = "target_zcu111")]
// Micron MTA4ATF51264HZ-2G6E1, DDR4, max data rate 2666 MHz
// But US+ only supports up to 2133 MHz for a single-rank DIMM
pub const DDR_FREQ: u32 = 2_133_333_333;

// ZynqMP memory map has the SDRAM starting at address 0,
// and even unsafe Rust requires non-null pointers.
// This const holds the offset to be applied to `ptr_low` so that it doesn't return a null pointer.
// `size_low` is reduced by the same amount.
pub const SKIP_FIRST_BYTES: usize = 2 << 20; // 2 MiB, the size of an L2 block

pub struct DdrRam {
    regs: &'static mut RegisterBlock,
    phy: DdrPhy,
    config: GeneralConfig,
}

impl DdrRam {
    pub fn ddr_ram(config: GeneralConfig) -> Self {
        let self_ = DdrRam {
            regs: RegisterBlock::ddrc(),
            phy: DdrPhy::ddr_phy(),
            config,
        };
        self_
    }

    pub fn init(&mut self) {
        self.configure();
        self.phy.configure(&self.config);
        // self.phy.clear_training_status();
        self.phy.pll_init(&self.config);
        self.phy.execute_zcal_dcal();
        // enable phy initialization complete signal
        self.regs
            .dfi_misc
            .write(DfiMisc::zeroed().dfi_init_complete_en(true));
        // disable quasi-dynamic register programming
        self.regs.sw_ctl.write(SwCtl::zeroed().sw_done(true));
        while !self.regs.sw_stat.read().sw_done_ack() {}

        // wait for operating mode to read normal
        let mut stat_read = self.regs.stat.read();
        while stat_read.operating_mode() != 1 {
            stat_read = self.regs.stat.read();
        }

        self.phy.execute_training();
        self.regs.zq_ctl0.modify(|_, w| w.dis_auto_zq(false));
        self.regs
            .rfsh_ctl3
            .modify(|_, w| w.disable_auto_refresh(false));
        self.phy.regs.pgcr1.modify(|_, w| w.pub_mode(false));
    }

    pub fn configure(&mut self) {
        // assert DDRC reset
        crf_apb::RegisterBlock::unlocked(|crf_apb| {
            crf_apb.rst_ddr_ss.modify(|_, w| w.ddr_reset(true))
        });

        self.write_regs();

        // bring controller out of reset
        crf_apb::RegisterBlock::unlocked(|crf_apb| {
            crf_apb
                .rst_ddr_ss
                .modify(|_, w| w.ddr_reset(false).apm_reset(false))
        });
    }

    pub fn write_regs(&mut self) {
        // master
        let active_ranks = (1 << self.config.logical_ranks()) - 1;
        let device_config = match self.config.device_width {
            8 => DeviceConfig::X8,
            16 => DeviceConfig::X16,
            32 => DeviceConfig::X32,
            _ => panic!("Invalid device width"),
        };
        let burst_rdwr = match self.config.burst_len() {
            4 => BurstRdwr::Len4,
            8 => BurstRdwr::Len8,
            16 => BurstRdwr::Len16,
            _ => panic!("Invalid burst length"),
        };
        let bus_width = match self.config.bus_width {
            16 => DataBusWidth::Quarter,
            32 => DataBusWidth::Half,
            64 => DataBusWidth::Full,
            _ => panic!("Invalid bus width"),
        };
        self.regs.mstr.write(
            Mstr::zeroed()
                .device_config(device_config)
                .active_ranks(active_ranks)
                .burst_rdwr(burst_rdwr)
                .data_bus_width(bus_width)
                .lpddr4(matches!(self.config.device_type, DeviceType::LpDdr4))
                .ddr4(matches!(self.config.device_type, DeviceType::Ddr4))
                .lpddr3(matches!(self.config.device_type, DeviceType::LpDdr3))
                .ddr3(matches!(self.config.device_type, DeviceType::Ddr3)),
        );

        self.regs.mr_ctrl0.write(MRCtrl0::zeroed().mr_rank(0x3));

        // derate_en
        let rc_derate_value = ceilf(3.75 / self.config.ctl_clock_period_ns()) as u8;
        self.regs
            .derate_en
            .write(DerateEn::zeroed().rc_derate_value(rc_derate_value));

        // default value
        self.regs
            .derate_int
            .write(DerateInt::zeroed().mr4_read_interval(0x800000));

        // power_ctrl
        self.regs.pwr_ctl.write(
            PwrCtl::zeroed()
                .en_dfi_dram_clk_disable(self.config.clock_stop_en)
                .powerdown_en(self.config.power_down_en)
                .self_ref_en(self.config.self_ref_en),
        );

        // 500 us
        let t_dpd_x4096 = ceilf(self.config.ns_to_nck(500_000.0 / 4096.0)).min(255.0) as u8;
        self.regs.pwr_tmg.write(
            PwrTmg::zeroed()
                .self_ref_to_x32(0x40)
                .t_dpd_x4096(t_dpd_x4096)
                .powerdown_to_x32(0x10),
        );

        // defaults
        self.regs
            .rfsh_ctl0
            .write(RfshCtl0::zeroed().refresh_margin(0x2).refresh_to_x32(0x10));

        // refresh_ctrl1
        // extra division by 2 since that's what the registers want
        let t_refi_x32 = (self.config.ps_to_nck(self.config.t_refi_ps()) / 64) as u16;
        // if a second rank is present, stagger the refreshes
        let timer1_start_value_x32 = if self.config.rank_addr_bits() == 1 {
            t_refi_x32 / 2
        } else {
            0
        };
        self.regs
            .rfsh_ctl1
            .write(RfshCtl1::zeroed().timer1_start_value_x32(timer1_start_value_x32));

        // refresh_ctrl3
        let refresh_mode = match self.config.fine_granularity_ref_mode {
            FineGranularityRefMode::X1 => 0b000,
            FineGranularityRefMode::X2 => 0b001,
            FineGranularityRefMode::X4 => 0b010,
        };
        self.regs
            .rfsh_ctl3
            .write(RfshCtl3::zeroed().refresh_mode(refresh_mode));

        // refresh_timing
        let t_rfc_min = self
            .config
            .ps_to_nck(self.config.t_rfc_min_ps())
            .div_ceil(2) as u16;
        let t_rfc_nom_x32 = match self.config.fine_granularity_ref_mode {
            FineGranularityRefMode::X1 => t_refi_x32.min(0xffe),
            FineGranularityRefMode::X2 => (t_refi_x32 / 2).min(0x7ff),
            FineGranularityRefMode::X4 => (t_refi_x32 / 4).min(0x3ff),
        };
        self.regs.rfsh_tmg.write(
            RfshTmg::zeroed()
                .t_rfc_min(t_rfc_min)
                .t_rfc_nom_x32(t_rfc_nom_x32),
        );

        // toggle refresh_update_level to signal change
        self.regs
            .rfsh_ctl3
            .modify(|r, w| w.refresh_update_level(!r.refresh_update_level()));

        // TODO: other ecc_cfg, crc_par_ctrl if enabled
        self.regs
            .ecc_cfg0
            .write(EccCfg0::zeroed().ecc_mode((self.config.ecc_en as u8) << 2));

        self.regs.crc_par_ctl1.write(
            CrcParCtl1::zeroed()
                .dfi_t_phy_rdlat(0x10)
                .alert_wait_for_sw(true)
                .crc_enable(self.config.crc_en)
                .parity_enable(self.config.parity_en),
        );

        let t_par_alert_pw_max = (self.config.speed_bin_mhz() * 3).div_ceil(100) as u16;
        self.regs.crc_par_ctl2.write(
            CrcParCtl2::zeroed()
                .t_par_alert_pw_max(t_par_alert_pw_max)
                .t_crc_alert_pw_max(0x5)
                .retry_fifo_max_hold_timer_x4(0x1f),
        );

        // init regs
        let post_cke_x1024 = ceilf(self.config.ns_to_nck(400.0 / (1024.0 * 2.0))) as u16 + 1;
        let pre_cke_x1024 = ceilf(self.config.ns_to_nck(500_000.0 / (1024.0 * 2.0))) as u16 + 1;
        self.regs.init0.write(
            Init0::zeroed()
                .post_cke_x1024(post_cke_x1024)
                .pre_cke_x1024(pre_cke_x1024),
        );

        let dram_rstn_x1024 = ceilf(self.config.ns_to_nck(100.0 / (1024.0 * 2.0))) as u16 + 1;
        self.regs
            .init1
            .write(Init1::zeroed().dram_rstn_x1024(dram_rstn_x1024));

        let idle_after_reset_x32 = ceilf(self.config.ns_to_nck(1_000.0 / 32.0)) as u8 + 1;
        self.regs.init2.write(
            Init2::zeroed()
                .idle_after_reset_x32(idle_after_reset_x32)
                .min_stable_clock_x1(0x5),
        );

        self.regs
            .init3
            .write(Init3::zeroed().mr(self.config.mr0()).emr(self.config.mr1()));

        self.regs.init4.write(
            Init4::zeroed()
                .emr2(self.config.mr2())
                .emr3(self.config.mr3()),
        );

        self.regs.init5.write(
            Init5::zeroed()
                .dev_zqinit_x32(self.config.dev_zqinit_x32())
                .max_auto_init_x1024(self.config.max_auto_init_x1024()),
        );

        self.regs.init6.write(
            Init6::zeroed()
                .mr4(self.config.mr4())
                .mr5(self.config.mr5()),
        );

        self.regs
            .init7
            .write(Init7::zeroed().mr6(self.config.mr6()));

        // dimm_ctl
        let dimm_addr_mirr_en = match self.config.module_config {
            ModuleConfig::Unbuffered(mod_config) => mod_config.rank_1_mirrored,
            // TODO
            _ => false,
        };
        let dimm_stagger_cs_en = match self.config.module_config {
            ModuleConfig::Unbuffered(_) => self.config.rank_addr_bits() == 1,
            // TODO
            _ => false,
        };
        self.regs.dimm_ctl.write(
            DimmCtl::zeroed()
                .dimm_addr_mirr_en(dimm_addr_mirr_en)
                .dimm_stagger_cs_en(dimm_stagger_cs_en),
        );

        // rank_ctl
        // default values
        self.regs.rank_ctl.write(
            RankCtl::zeroed()
                .diff_rank_wr_gap(0x6)
                .diff_rank_rd_gap(0x6)
                .max_rank_rd(0xf),
        );

        // dram_tmg0
        let mut wr2pre = self.config.write_latency_nck()
            + self.config.burst_len() as u32 / 2
            + self.config.ns_to_nck(self.config.t_wr_min_ns()) as u32;
        if self.config.en_2t_timing {
            wr2pre = wr2pre.div_ceil(2);
        } else {
            wr2pre /= 2;
        }
        let t_faw = self.config.ps_to_nck(self.config.t_faw_min_ps).div_ceil(2) as u8;
        let t_ras_max_x1024 =
            ((self.config.ps_to_nck(self.config.t_ras_max_ps()) / 1024 - 1) / 2).max(1) as u8;
        let mut t_ras_min = self.config.ps_to_nck(self.config.t_ras_min_ps);
        if self.config.en_2t_timing || matches!(self.config.device_type, DeviceType::LpDdr4) {
            t_ras_min = t_ras_min.div_ceil(2);
        } else {
            t_ras_min /= 2;
        }
        self.regs.dram_tmg0.write(
            DramTmg0::zeroed()
                .wr2pre(wr2pre as u8)
                .t_faw(t_faw)
                .t_ras_max(t_ras_max_x1024)
                .t_ras_min(t_ras_min as u8),
        );

        // dram_tmg1
        let t_xp = (self.config.t_xp_nck() + self.config.parity_latency_nck()).div_ceil(2) as u8;
        let mut rd2pre = match self.config.device_type {
            DeviceType::Ddr4 => (self.config.additive_latency_nck() + self.config.t_rtp_min_nck())
                .max(
                    self.config.read_latency_nck() + self.config.burst_len() as u32 / 2
                        - self.config.ps_to_nck(self.config.t_rp_min_ps),
                ),
            _ => 2 * 0x4, // default value is 4 after div 2
        };
        if self.config.en_2t_timing || matches!(self.config.device_type, DeviceType::LpDdr4) {
            rd2pre = rd2pre.div_ceil(2);
        } else {
            rd2pre /= 2;
        }
        let t_rc = self.config.ps_to_nck(self.config.t_rc_min_ps).div_ceil(2) as u8 + 1;
        self.regs.dram_tmg1.write(
            DramTmg1::zeroed()
                .t_xp(t_xp)
                .rd2pre(rd2pre as u8)
                .t_rc(t_rc),
        );

        // dram_tmg2
        let write_latency = self.config.write_latency_nck().div_ceil(2) as u8;
        let read_latency = self.config.read_latency_nck().div_ceil(2) as u8;
        let wr_preamble = if self.config.wr_2ck_preamble { 2 } else { 1 };
        let rd2wr = (match self.config.device_type {
            DeviceType::Ddr4 => {
                self.config.read_latency_nck()
                    + self.config.burst_len() as u32 / 2
                    + 1
                    + wr_preamble
                    - self.config.write_latency_nck()
            }
            // TODO
            _ => 2 * 0x6, // default value pre div 2
        })
        .div_ceil(2) as u8;
        let wr2rd = (match self.config.device_type {
            DeviceType::Ddr4 => {
                self.config.cas_write_latency_nck()
                    + self.config.parity_latency_nck()
                    + self.config.burst_len() as u32 / 2
                    + self.config.t_wtr_l_min_nck()
            }
            _ => 2 * 0xd,
        })
        .div_ceil(2) as u8;
        self.regs.dram_tmg2.write(
            DramTmg2::zeroed()
                .write_latency(write_latency)
                .read_latency(read_latency)
                .rd2wr(rd2wr)
                .wr2rd(wr2rd),
        );

        // dram_tmg3
        let t_mrw = 0x5; // couldn't find this in the DDR4 spec? so just using the default
        let t_mrd = self.config.t_mrd_min_nck().div_ceil(2) as u8;
        let t_mod = (match self.config.device_type {
            DeviceType::Ddr4 => self.config.t_mod_min_nck() + self.config.parity_latency_nck(),
            _ => 2 * 0xc,
        })
        .div_ceil(2) as u16;
        self.regs
            .dram_tmg3
            .write(DramTmg3::zeroed().t_mrw(t_mrw).t_mrd(t_mrd).t_mod(t_mod));

        // dram_tmg4
        let t_rcd = (self.config.ps_to_nck(self.config.t_rcd_min_ps)
            - self.config.additive_latency_nck())
        .div_ceil(2)
        .max(1) as u8;
        let t_ccd = self
            .config
            .ps_to_nck(self.config.t_ccd_l_min_ps)
            .div_ceil(2) as u8;
        let t_rrd = self
            .config
            .ps_to_nck(self.config.t_rrd_l_min_ps)
            .div_ceil(2) as u8;
        let t_rp = (self.config.t_rp_min_ps.div_ceil(self.config.t_ckavg_min_ps) / 2 + 1) as u8;
        self.regs.dram_tmg4.write(
            DramTmg4::zeroed()
                .t_rcd(t_rcd)
                .t_ccd(t_ccd)
                .t_rrd(t_rrd)
                .t_rp(t_rp),
        );

        // dram_tmg5
        let t_cksrx = (match self.config.device_type {
            DeviceType::Ddr4 => self.config.t_cksrx_min_nck(),
            // TODO
            _ => 2 * 0x5,
        })
        .div_ceil(2) as u8;
        let t_cksre = (match self.config.device_type {
            DeviceType::Ddr4 => self.config.t_cksre_min_nck(),
            _ => 2 * 0x5,
        })
        .div_ceil(2) as u8;
        let t_ckesr = (match self.config.device_type {
            DeviceType::Ddr4 => self.config.t_cke_min_nck() + 1,
            _ => 2 * 0x4,
        })
        .div_ceil(2) as u8;
        let t_cke = (match self.config.device_type {
            DeviceType::Ddr4 => self.config.t_cke_min_nck(),
            _ => 2 * 0x3,
        })
        .div_ceil(2) as u8;
        self.regs.dram_tmg5.write(
            DramTmg5::zeroed()
                .t_cksrx(t_cksrx)
                .t_cksre(t_cksre)
                .t_ckesr(t_ckesr)
                .t_cke(t_cke),
        );

        let t_ckcsx = t_cke + 1;
        self.regs.dram_tmg6.write(
            DramTmg6::zeroed()
                .t_ckdpde(0x1)
                .t_ckdpdx(0x1)
                .t_ckcsx(t_ckcsx),
        );

        self.regs
            .dram_tmg7
            .write(DramTmg7::zeroed().t_ckpde(t_cksre).t_ckpdx(t_cksrx));

        // dram_tmg8
        let t_xs_fast_x32 = ceilf(
            self.config
                .ns_to_nck(self.config.t_xs_fast_min_ns() / (32.0 * 2.0)),
        ) as u8
            + 1;
        let t_xs_abort_x32 = ceilf(
            self.config
                .ns_to_nck(self.config.t_xs_abort_min_ns() / (32.0 * 2.0)),
        ) as u8
            + 1;
        let t_xs_dll_x32 = self.config.t_xs_dll_min_nck().div_ceil(32 * 2) as u8 + 1;
        let t_xs_x32 = ceilf(
            self.config
                .ns_to_nck(self.config.t_xs_min_ns() / (32.0 * 2.0)),
        ) as u8
            + 1;
        self.regs.dram_tmg8.write(
            DramTmg8::zeroed()
                .t_xs_fast_x32(t_xs_fast_x32)
                .t_xs_abort_x32(t_xs_abort_x32)
                .t_xs_dll_x32(t_xs_dll_x32)
                .t_xs_x32(t_xs_x32),
        );

        // dram_tmg9
        let t_ccd_s = self.config.t_ccd_s_min_nck().div_ceil(2) as u8;
        let t_rrd_s = self
            .config
            .ps_to_nck(self.config.t_rrd_s_min_ps)
            .div_ceil(2) as u8;
        let wr2rd_s = (self.config.cas_write_latency_nck()
            + self.config.parity_latency_nck()
            + self.config.burst_len() as u32 / 2
            + self.config.t_wtr_s_min_nck())
        .div_ceil(2) as u8;
        self.regs.dram_tmg9.write(
            DramTmg9::zeroed()
                .ddr4_wr_preamble(self.config.wr_2ck_preamble)
                .t_ccd_s(t_ccd_s)
                .t_rrd_s(t_rrd_s)
                .wr2rd_s(wr2rd_s),
        );

        // dram_tmg11
        let post_mpsm_gap_x32 = (self.config.t_xmpdll_min_nck()).div_ceil(32 * 2) as u8;
        let t_mpx_lh = ceilf(self.config.ns_to_nck(self.config.t_mpx_lh_min_ns() / 2.0)) as u8 + 1;
        let t_mpx_s = self
            .config
            .ps_to_nck(self.config.t_mpx_s_min_ps())
            .div_ceil(2) as u8;
        let t_ckmpe = self.config.t_ckmpe_min_nck().div_ceil(2) as u8;

        self.regs.dram_tmg11.write(
            DramTmg11::zeroed()
                .post_mpsm_gap_x32(post_mpsm_gap_x32)
                .t_mpx_lh(t_mpx_lh)
                .t_mpx_s(t_mpx_s)
                .t_ckmpe(t_ckmpe),
        );

        // dram_tmg12
        let t_mrd_pda = self.config.t_mrd_pda_min_nck().div_ceil(2) as u8;
        self.regs.dram_tmg12.write(
            DramTmg12::zeroed()
                .t_cmdcke(0x2)
                .t_ckehcmd(0x6)
                .t_mrd_pda(t_mrd_pda),
        );

        // dram_tmg13/14: LP only

        // zqctl
        let t_zq_long_nop = self.config.t_zq_oper_nck().div_ceil(2) as u16;
        let t_zq_short_nop = self.config.t_zq_cs_nck().div_ceil(2) as u16;
        self.regs.zq_ctl0.write(
            ZQCtl0::zeroed()
                .t_zq_long_nop(t_zq_long_nop)
                .t_zq_short_nop(t_zq_short_nop),
        );

        let t_zq_reset_nop = self.config.t_zq_init_nck().div_ceil(2) as u16;
        let t_zq_short_interval_x1024 = self.config.ns_to_nck(100_000_000.0) as u32 / 1024; // 100 ms
        self.regs.zq_ctl1.write(
            ZQCtl1::zeroed()
                .t_zq_reset_nop(t_zq_reset_nop)
                .t_zq_short_interval_x1024(t_zq_short_interval_x1024),
        );

        // dfitmg
        let dfi_t_rddata_en = match self.config.device_type {
            DeviceType::Ddr3 | DeviceType::Ddr4 => self.config.read_latency_nck() - 4,
            _ => 0x2,
        } as u8;
        let dfi_tphy_wrlat = match self.config.device_type {
            DeviceType::Ddr3 | DeviceType::Ddr4 => self.config.write_latency_nck() - 3,
            _ => 0x2,
        } as u8;
        self.regs.dfi_tmg0.write(
            DfiTmg0::zeroed()
                .dfi_t_ctrl_delay(0x4)
                .dfi_rddata_use_sdr(true)
                .dfi_t_rddata_en(dfi_t_rddata_en)
                .dfi_wrdata_use_sdr(true)
                .dfi_tphy_wrdata(0x2)
                .dfi_tphy_wrlat(dfi_tphy_wrlat),
        );

        let dfi_t_wrdata_delay = match self.config.device_type {
            DeviceType::Ddr4 | DeviceType::LpDdr4 => 0x3,
            DeviceType::Ddr3 | DeviceType::LpDdr3 => 0x2,
        };
        self.regs.dfi_tmg1.write(
            DfiTmg1::zeroed()
                .dfi_t_wrdata_delay(dfi_t_wrdata_delay)
                .dfi_t_dram_clk_disable(0x3)
                .dfi_t_dram_clk_enable(0x4),
        );

        self.regs.dfi_tmg2.write(
            DfiTmg2::zeroed()
                .dfi_tphy_rdcslat(dfi_t_rddata_en - 2)
                .dfi_tphy_wrcslat(dfi_tphy_wrlat - 2),
        );

        // dfilpcfg
        self.regs.dfi_lp_cfg0.write(
            DfiLpCfg0::zeroed()
                .dfi_tlp_resp(0x7)
                .dfi_lp_en_sr(true)
                .dfi_lp_en_pd(true),
        );
        self.regs.dfi_lp_cfg1.write(
            DfiLpCfg1::zeroed()
                .dfi_lp_wakeup_mpsm(0x2)
                .dfi_lp_en_mpsm(true),
        );

        // dfiupd
        self.regs.dfi_upd0.write(
            DfiUpd0::zeroed()
                .dfi_t_ctrlup_max(0x40)
                .dfi_t_ctrlup_min(0x3),
        );
        self.regs.dfi_upd1.write(
            DfiUpd1::zeroed()
                .dfi_t_ctrlupd_interval_min_x1024(200)
                .dfi_t_ctrlupd_interval_max_x1024(255),
        );

        // dfimsc
        self.regs.dfi_misc.write(DfiMisc::zeroed());

        // dbictl
        self.regs.dbi_ctl.write(
            DbiCtl::zeroed()
                .rd_dbi_en(self.config.rd_dbi_en)
                .wr_dbi_en(self.config.wr_dbi_en)
                .dm_en(self.config.dm_en),
        );

        // address map
        self.regs
            .addr_map0
            .write(AddrMap0::zeroed().addrmap_cs_bit0(0x1f));

        let bank_addr_map = self.config.bank_addr_map();
        self.regs.addr_map1.write(
            AddrMap1::zeroed()
                .addrmap_bank_b2(bank_addr_map[2] as u8)
                .addrmap_bank_b1(bank_addr_map[1] as u8)
                .addrmap_bank_b0(bank_addr_map[0] as u8),
        );

        let col_addr_map = self.config.col_addr_map();
        self.regs.addr_map2.write(
            AddrMap2::zeroed()
                .addrmap_col_b5(col_addr_map[5] as u8)
                .addrmap_col_b4(col_addr_map[4] as u8)
                .addrmap_col_b3(col_addr_map[3] as u8)
                .addrmap_col_b2(col_addr_map[2] as u8),
        );
        self.regs.addr_map3.write(
            AddrMap3::zeroed()
                .addrmap_col_b9(col_addr_map[9] as u8)
                .addrmap_col_b8(col_addr_map[8] as u8)
                .addrmap_col_b7(col_addr_map[7] as u8)
                .addrmap_col_b6(col_addr_map[6] as u8),
        );
        self.regs.addr_map4.write(
            AddrMap4::zeroed()
                .addrmap_col_b11(col_addr_map[11] as u8)
                .addrmap_col_b10(col_addr_map[10] as u8),
        );

        let row_addr_map = self.config.row_addr_map();
        self.regs.addr_map5.write(
            AddrMap5::zeroed()
                .addrmap_row_b11(row_addr_map[11] as u8)
                .addrmap_row_b2_10(0xf)
                .addrmap_row_b1(row_addr_map[1] as u8)
                .addrmap_row_b0(row_addr_map[0] as u8),
        );
        self.regs.addr_map6.write(
            AddrMap6::zeroed()
                .addrmap_row_b15(row_addr_map[15] as u8)
                .addrmap_row_b14(row_addr_map[14] as u8)
                .addrmap_row_b13(row_addr_map[13] as u8)
                .addrmap_row_b12(row_addr_map[12] as u8),
        );
        self.regs.addr_map7.write(
            AddrMap7::zeroed()
                .addrmap_row_b17(row_addr_map[17] as u8)
                .addrmap_row_b16(row_addr_map[16] as u8),
        );

        let bg_addr_map = self.config.bg_addr_map();
        self.regs.addr_map8.write(
            AddrMap8::zeroed()
                .addrmap_bg_b1(bg_addr_map[1] as u8)
                .addrmap_bg_b0(bg_addr_map[0] as u8),
        );
        self.regs.addr_map9.write(
            AddrMap9::zeroed()
                .addrmap_row_b5(row_addr_map[5] as u8)
                .addrmap_row_b4(row_addr_map[4] as u8)
                .addrmap_row_b3(row_addr_map[3] as u8)
                .addrmap_row_b2(row_addr_map[2] as u8),
        );
        self.regs.addr_map10.write(
            AddrMap10::zeroed()
                .addrmap_row_b9(row_addr_map[9] as u8)
                .addrmap_row_b8(row_addr_map[8] as u8)
                .addrmap_row_b7(row_addr_map[7] as u8)
                .addrmap_row_b6(row_addr_map[6] as u8),
        );
        self.regs
            .addr_map11
            .write(AddrMap11::zeroed().addrmap_row_b10(row_addr_map[10] as u8));

        // odtcfg
        let wr_odt_hold = match self.config.device_type {
            DeviceType::Ddr4 => {
                5 + self.config.wr_2ck_preamble as u8 + 1 + self.config.crc_en as u8
            }
            _ => 0x4,
        };
        let rd_odt_hold = match self.config.device_type {
            DeviceType::Ddr4 => 5 + self.config.rd_2ck_preamble as u8 + 1,
            _ => 0x4,
        };
        let rd_odt_delay = match self.config.device_type {
            DeviceType::Ddr4 => ((self.config.cas_latency_nck()
                - self.config.cas_write_latency_nck()) as i8
                - self.config.rd_2ck_preamble as i8
                + self.config.wr_2ck_preamble as i8
                - 1)
            .max(0) as u8,
            _ => 0,
        };
        self.regs.odt_cfg.write(
            OdtCfg::zeroed()
                .wr_odt_hold(wr_odt_hold)
                .rd_odt_hold(rd_odt_hold)
                .rd_odt_delay(rd_odt_delay),
        );

        let rank1_wr_odt = 2 * self.config.rank_addr_bits();
        self.regs.odt_map.write(
            OdtMap::zeroed()
                .rank1_wr_odt(rank1_wr_odt)
                .rank0_wr_odt(0x1),
        );

        // sched
        self.regs.sched.write(
            Sched::zeroed()
                .rdwr_idle_gap(0x1)
                .lpr_num_entries(0x20)
                .force_low_pri_n(true),
        );

        // perf regs
        self.regs.perf_lpr1.write(
            PerfLPR1::zeroed()
                .lpr_xact_run_length(0x8)
                .lpr_max_starve(0x40),
        );
        self.regs
            .perf_wr1
            .write(PerfWR1::zeroed().w_xact_run_length(0x8).w_max_starve(0x40));

        // dq map
        self.regs.dq_map0.write(
            DqMap0::zeroed()
                .dq_nibble_map_12_15(self.config.dq_map[3])
                .dq_nibble_map_8_11(self.config.dq_map[2])
                .dq_nibble_map_4_7(self.config.dq_map[1])
                .dq_nibble_map_0_3(self.config.dq_map[0]),
        );
        self.regs.dq_map1.write(
            DqMap1::zeroed()
                .dq_nibble_map_28_31(self.config.dq_map[7])
                .dq_nibble_map_24_27(self.config.dq_map[6])
                .dq_nibble_map_20_23(self.config.dq_map[5])
                .dq_nibble_map_16_19(self.config.dq_map[4]),
        );
        self.regs.dq_map2.write(
            DqMap2::zeroed()
                .dq_nibble_map_44_47(self.config.dq_map[13])
                .dq_nibble_map_40_43(self.config.dq_map[12])
                .dq_nibble_map_36_39(self.config.dq_map[11])
                .dq_nibble_map_32_35(self.config.dq_map[10]),
        );
        self.regs.dq_map3.write(
            DqMap3::zeroed()
                .dq_nibble_map_60_63(self.config.dq_map[17])
                .dq_nibble_map_56_59(self.config.dq_map[16])
                .dq_nibble_map_52_55(self.config.dq_map[15])
                .dq_nibble_map_48_51(self.config.dq_map[14]),
        );
        self.regs.dq_map4.write(
            DqMap4::zeroed()
                .dq_nibble_map_cb_4_7(self.config.dq_map[9])
                .dq_nibble_map_cb_0_3(self.config.dq_map[8]),
        );
        self.regs
            .dq_map5
            .write(DqMap5::zeroed().dis_dq_rank_swap(true));

        self.regs.dbg0.write(Dbg0::zeroed());
        self.regs.dbg_cmd.write(DbgCmd::zeroed());

        self.regs.sw_ctl.write(SwCtl::zeroed());

        // Port config and QOS
        self.regs.pc_cfg.write(PCCfg::zeroed().go2critical_en(true));
        self.regs.port0_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port0_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port0_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port0_rqos_cfg0.write(
            PortRqosCfg0::zeroed()
                .rqos_map_region1(RdTrafficClass::HPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level1(0xb),
        );
        self.regs.port0_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port0_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port0_wqos_cfg1.write(PortWqosCfg1::zeroed());

        self.regs.port1_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port1_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port1_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port1_rqos_cfg0.write(
            Port12RqosCfg0::zeroed()
                .rqos_map_region2(RdTrafficClass::HPR)
                .rqos_map_region1(RdTrafficClass::LPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level2(0xb)
                .rqos_map_level1(0x3),
        );
        self.regs.port1_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port1_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port1_wqos_cfg1.write(PortWqosCfg1::zeroed());

        self.regs.port2_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port2_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port2_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port2_rqos_cfg0.write(
            Port12RqosCfg0::zeroed()
                .rqos_map_region2(RdTrafficClass::HPR)
                .rqos_map_region1(RdTrafficClass::LPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level2(0xb)
                .rqos_map_level1(0x3),
        );
        self.regs.port2_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port2_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port2_wqos_cfg1.write(PortWqosCfg1::zeroed());

        self.regs.port3_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port3_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port3_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port3_rqos_cfg0.write(
            PortRqosCfg0::zeroed()
                .rqos_map_region1(RdTrafficClass::HPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level1(0x3),
        );
        self.regs.port3_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port3_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port3_wqos_cfg1.write(PortWqosCfg1::zeroed());

        self.regs.port4_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port4_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port4_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port4_rqos_cfg0.write(
            PortRqosCfg0::zeroed()
                .rqos_map_region1(RdTrafficClass::HPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level1(0x3),
        );
        self.regs.port4_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port4_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port4_wqos_cfg1.write(PortWqosCfg1::zeroed());

        self.regs.port5_cfg_r.write(
            PortCfgR::zeroed()
                .rd_port_urgent_en(true)
                .rd_port_priority(0xf),
        );
        self.regs.port5_cfg_w.write(
            PortCfgW::zeroed()
                .wr_port_urgent_en(true)
                .wr_port_priority(0xf),
        );
        self.regs.port5_ctrl.write(PortCtrl::zeroed().port_en(true));
        self.regs.port5_rqos_cfg0.write(
            PortRqosCfg0::zeroed()
                .rqos_map_region1(RdTrafficClass::HPR)
                .rqos_map_region0(RdTrafficClass::LPR)
                .rqos_map_level1(0x3),
        );
        self.regs.port5_rqos_cfg1.write(PortRqosCfg1::zeroed());
        self.regs.port5_wqos_cfg0.write(PortWqosCfg0::zeroed());
        self.regs.port5_wqos_cfg1.write(PortWqosCfg1::zeroed());

        // SAR base/size
        // DDR_LO starts at 0x0
        self.regs.sar_base0.write(SarBase::zeroed());
        // 2 GB (1 block)
        self.regs.sar_size0.write(SarSize::zeroed());
        // DDR_HI @ 0x08_0000_0000
        self.regs.sar_base1.write(SarBase::zeroed().base_addr(0x10));
        // up to 32 GB (16 blocks)
        // TODO: calculate based on actual SDRAM capacity(?)
        self.regs.sar_size1.write(SarSize::zeroed().nblocks(0xf));

        self.regs.dfi_tmg0_shadow.write(
            DfiTmg0::zeroed()
                .dfi_t_ctrl_delay(0x7)
                .dfi_rddata_use_sdr(true)
                .dfi_t_rddata_en(0x2)
                .dfi_wrdata_use_sdr(true)
                .dfi_tphy_wrlat(0x2),
        );
    }

    fn true_ptr_low(&mut self) -> usize {
        0x0000_0000
    }

    pub fn ptr_low<T>(&mut self) -> *mut T {
        (self.true_ptr_low() + SKIP_FIRST_BYTES) as *mut _
    }

    pub fn ptr_high<T>(&mut self) -> *mut T {
        0x8_0000_0000 as *mut _
    }

    pub fn size(&self) -> usize {
        (self.config.module_capacity_megabytes() as usize) * 1024 * 1024
    }

    fn true_size_low(&self) -> usize {
        let two_gib = 2 << 30;
        if self.size() > two_gib {
            two_gib
        } else {
            self.size()
        }
    }

    pub fn size_low(&self) -> usize {
        self.true_size_low() - SKIP_FIRST_BYTES
    }

    pub fn size_high(&self) -> usize {
        let two_gib = 2 << 30;
        if self.size() > two_gib {
            self.size() - two_gib
        } else {
            0
        }
    }

    pub fn update_mmu_entries(&mut self) {
        let start_low = self.true_ptr_low();
        for ptr in (start_low..(start_low + self.true_size_low())).step_by(mmu::L2_BLOCK_SIZE) {
            mmu::update_block(ptr as *const (), |_| {
                Some(mmu::BlockAttrs {
                    xn: false,
                    // TODO: see libcortex_a53/uncached.rs
                    contiguous: false,
                    access_flag: true,
                    shareability: Shareability::InnerShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 0, // normal WB/WA/RA cacheable
                })
            });
        }
        let start_high = self.ptr_high() as *const () as usize;
        for ptr in (start_high..(start_high + self.size_high())).step_by(mmu::L1_BLOCK_SIZE) {
            mmu::update_block(ptr as *const (), |_| {
                Some(mmu::BlockAttrs {
                    xn: false,
                    contiguous: false,
                    access_flag: true,
                    shareability: Shareability::InnerShareable,
                    read_only: false,
                    ns: false,
                    attr_indx: 0, // normal WB/WA/RA cacheable
                })
            });
        }
    }

    pub fn memtest(&mut self) {
        let mut slices = unsafe {
            [
                core::slice::from_raw_parts_mut(self.ptr_low(), self.size_low()),
                core::slice::from_raw_parts_mut(self.ptr_high(), self.size_high()),
            ]
        };
        let patterns: &'static [u32] = &[0xffff_ffff, 0x5555_5555, 0xaaaa_aaaa, 0];
        for (i, slice) in slices.iter_mut().enumerate() {
            println!("memtest step {}", i);
            for (j, pattern) in patterns.iter().enumerate() {
                println!("memtest phase {}: {:#08X}", j, pattern);

                println!("writing...");
                for megabyte in 0..slice.len() / (1024 * 1024) {
                    let start = megabyte * 1024 * 1024 / 4;
                    let end = (megabyte + 1) * 1024 * 1024 / 4;
                    for b in slice[start..end].iter_mut() {
                        *b = *pattern;
                    }

                    print!("\r{} MB", megabyte);
                }
                println!(" Ok");

                println!("reading...");
                let expected = *pattern;
                for megabyte in 0..slice.len() / (1024 * 1024) {
                    let start = megabyte * 1024 * 1024 / 4;
                    let end = (megabyte + 1) * 1024 * 1024 / 4;
                    for b in slice[start..end].iter_mut() {
                        let read: u32 = *b;
                        if read != expected {
                            println!(
                                "{:08X}: expected {:08X}, read {:08X}",
                                b as *mut _ as usize, expected, read
                            );
                        };
                    }

                    print!("\r{} MB", megabyte);
                }
                println!(" Ok");
            }
        }
    }
}
