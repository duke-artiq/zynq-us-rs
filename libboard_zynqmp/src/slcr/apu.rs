use libregister::{register, register_at, register_bit, register_bits};

#[repr(C)]
pub struct RegisterBlock {
    pub err_ctrl: ErrCtrl, // 0x0
    unused0: [u32; 3],
    pub isr: ISR, // 0x10
    pub imr: IMR,
    pub ien: IEN,
    pub ids: IDS,
    pub config_0: Config0,
    pub config_1: Config1, // 0x24
    unused1: [u32; 6],
    pub rvbar_addr0_l: RVBarAddrL, // 0x40
    pub rvbar_addr0_h: RVBarAddrH,
    pub rvbar_addr1_l: RVBarAddrL,
    pub rvbar_addr1_h: RVBarAddrH,
    pub rvbar_addr2_l: RVBarAddrL,
    pub rvbar_addr2_h: RVBarAddrH,
    pub rvbar_addr3_l: RVBarAddrL,
    pub rvbar_addr3_h: RVBarAddrH,
    pub ace_ctrl: AceCtrl, // 0x60
    unused2: [u32; 7],
    pub snoop_ctrl: SnoopCtrl, // 0x80
    unused3: [u32; 3],
    pub pwr_ctl: PwrCtl, // 0x90
    pub pwr_stat: PwrStat,
}

register_at!(RegisterBlock, 0xFD5C_0000, apu);

register!(err_ctrl, ErrCtrl, RW, u32);
register_bit!(err_ctrl, pslverr, 0);

register!(isr, ISR, RW, u32);
register_bit!(isr, inv_apb, 0, WTC);

register!(imr, IMR, RO, u32);
register_bit!(imr, inv_apb, 0);

register!(ien, IEN, WO, u32);
register_bit!(ien, inv_apb, 0);

register!(ids, IDS, WO, u32);
register_bit!(ids, inv_apb, 0);

register!(config_0, Config0, RW, u32);
register_bits!(config_0, cfg_te, u8, 24, 27);
register_bits!(config_0, cfg_end, u8, 16, 19);
register_bits!(config_0, v_init_hi, u8, 8, 11);
register_bits!(config_0, aa64_n_aa32, u8, 0, 3);

register!(config_1, Config1, RW, u32);
register_bit!(config_1, l2_rst_disable, 29);
register_bit!(config_1, l1_rst_disable, 28);
register_bits!(config_1, cp15_disable, u8, 0, 3);

register!(rvbar_addr_l, RVBarAddrL, RW, u32);
register_bits!(rvbar_addr_l, addr, u32, 2, 31);

register!(rvbar_addr_h, RVBarAddrH, RW, u32);
register_bits!(rvbar_addr_h, addr, u8, 0, 7);

register!(ace_ctrl, AceCtrl, RW, u32);
register_bits!(ace_ctrl, awqos, u8, 16, 19);
register_bits!(ace_ctrl, arqos, u8, 0, 3);

register!(snoop_ctrl, SnoopCtrl, RW, u32);
register_bit!(snoop_ctrl, ace_inact, 4);
register_bit!(snoop_ctrl, acp_inact, 0);

register!(pwr_ctl, PwrCtl, RW, u32);
register_bit!(pwr_ctl, clr_ex_mon_req, 17);
register_bit!(pwr_ctl, l2_flush_req, 16);
register_bits!(pwr_ctl, cpu_pwrdwn_req, u8, 0, 3);

register!(pwr_stat, PwrStat, RO, u32);
register_bit!(pwr_stat, clr_ex_mon_ack, 17);
register_bit!(pwr_stat, l2_flush_done, 16);
register_bits!(pwr_stat, dbg_no_pwrdwn, u8, 0, 3);
