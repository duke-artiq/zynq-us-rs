use libregister::{register, register_at, register_bits};
use volatile_register::{RO, RW, WO};

pub struct RegisterBlock {
    pub iou_axi_wprtcn: IouAxiPrtcn,
    pub iou_axi_rprtcn: IouAxiPrtcn, // 0x4
    _unused0: [u32; 14],
    pub ctrl: RW<u32>, // 0x40
    pub isr: RW<u32>,
    pub imr: RO<u32>,
    pub ier: WO<u32>,
    pub idr: WO<u32>,
    pub itr: WO<u32>,
}
register_at!(RegisterBlock, 0xFF24_0000, iou_secure_slcr);

register!(iou_axi_prtcn, IouAxiPrtcn, RW, u32);
register_bits!(iou_axi_prtcn, qspi, u8, 25, 27);
register_bits!(iou_axi_prtcn, nand, u8, 22, 24);
register_bits!(iou_axi_prtcn, sd1, u8, 19, 21);
register_bits!(iou_axi_prtcn, sd0, u8, 16, 18);
register_bits!(iou_axi_prtcn, gem3, u8, 9, 11);
register_bits!(iou_axi_prtcn, gem2, u8, 6, 8);
register_bits!(iou_axi_prtcn, gem1, u8, 3, 5);
register_bits!(iou_axi_prtcn, gem0, u8, 0, 2);
