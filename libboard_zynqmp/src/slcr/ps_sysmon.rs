use libregister::{register, register_at, register_bits, RegisterW};

#[repr(C)]
pub struct RegisterBlock {
    dont_care: [u32; 69],
    pub analog_bus: AnalogBus, // 0x114
}
register_at!(RegisterBlock, 0xFFA5_0800, ps_sysmon);

register!(analog_bus, AnalogBus, RW, u32);
register_bits!(analog_bus, bus_val, u32, 0, 31);

impl RegisterBlock {
    pub fn write_magic_number(&mut self) {
        // from UG1085, apparently the boot value is incorrect
        self.analog_bus.write(AnalogBus::zeroed().bus_val(0x3210));
    }
}
