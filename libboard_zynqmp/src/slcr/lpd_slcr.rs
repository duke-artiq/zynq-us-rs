///! LPD Control
use libregister::{register, register_at, register_bit, register_bits};
use volatile_register::{RO, RW, WO};

#[repr(C)]
pub struct RegisterBlock {
    unused0: u32,
    pub ctrl: RW<u32>, // 0x4
    pub isr: RW<u32>,
    pub imr: RW<u32>,
    pub ier: RW<u32>,
    pub idr: RW<u32>,
    pub itr: RW<u32>,
    unused1: u32,
    pub persistent: [PersistentN; 8], // 0x20
    pub safety_chk: [RW<u32>; 4],
    pub csu_pmu_wdt_clk_sel: WdtClkSel,
    unused2: [u32; 2030],
    pub adma_cfg: RW<u32>, // 0x200C
    pub adma_ram: RW<u32>,
    unused3: [u32; 3067],
    pub afi_fs: RW<u32>, // 0x5000
    unused4: [u32; 1023],
    pub err_atb_isr: RW<u32>, // 0x6000
    pub err_atb_imr: RW<u32>,
    pub err_atb_ier: RW<u32>,
    pub err_atb_idr: RW<u32>,
    pub atb_cmd_store_en: RW<u32>,
    pub atb_resp_en: RW<u32>,
    pub atb_resp_type: RW<u32>,
    unused5: u32,
    pub atb_prescale: RW<u32>,
    unused6: [u32; 2039],
    pub gicp0_irq_status: RW<u32>, // 0x8000
    pub gicp0_irq_mask: RO<u32>,
    pub gicp0_irq_enable: WO<u32>,
    pub gicp0_irq_disable: WO<u32>,
    pub gicp0_irq_trigger: WO<u32>,
    pub gicp1_irq_status: RW<u32>,
    pub gicp1_irq_mask: RO<u32>,
    pub gicp1_irq_enable: WO<u32>,
    pub gicp1_irq_disable: WO<u32>,
    pub gicp1_irq_trigger: WO<u32>,
    pub gicp2_irq_status: RW<u32>,
    pub gicp2_irq_mask: RO<u32>,
    pub gicp2_irq_enable: WO<u32>,
    pub gicp2_irq_disable: WO<u32>,
    pub gicp2_irq_trigger: WO<u32>,
    pub gicp3_irq_status: RW<u32>,
    pub gicp3_irq_mask: RO<u32>,
    pub gicp3_irq_enable: WO<u32>,
    pub gicp3_irq_disable: WO<u32>,
    pub gicp3_irq_trigger: WO<u32>,
    pub gicp4_irq_status: RW<u32>,
    pub gicp4_irq_mask: RO<u32>,
    pub gicp4_irq_enable: WO<u32>,
    pub gicp4_irq_disable: WO<u32>,
    pub gicp4_irq_trigger: WO<u32>,
    unused7: [u32; 15],
    pub gicp_pmu_irq_status: RW<u32>, // 0x80A0
    pub gicp_pmu_irq_mask: RO<u32>,
    pub gicp_pmu_irq_enable: WO<u32>,
    pub gicp_pmu_irq_disable: WO<u32>,
    pub gicp_pmu_irq_trigger: WO<u32>,
}
register_at!(RegisterBlock, 0xFD41_0000, lpd_slcr);

register!(persistent_n, PersistentN, RW, u32);
register_bits!(persistent_n, value, u32, 0, 31);

register!(wdt_clk_sel, WdtClkSel, RW, u32);
// 0: internal APB clock, 1: external
register_bit!(wdt_clk_sel, select, 0);
