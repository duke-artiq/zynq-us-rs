///! Register definitions for UltraScale+ System Level Control
pub mod common;
pub mod crf_apb;
pub mod apu;
pub mod fpd_slcr;
// FPD_SLCR_SECURE
pub mod iou_slcr;
pub mod iou_secure_slcr;
pub mod iou_scntrs;
pub mod lpd_slcr;
pub mod lpd_slcr_secure;
pub mod crl_apb;
// RPU
pub mod cci400;
// FPD_GPV
pub mod ps_sysmon;
