///! FPD Control
use libregister::{
    register, register_at, register_bit, RegisterW,
};
use volatile_register::RW;

use super::common::{Unlocked, WProt};

#[repr(C)]
pub struct RegisterBlock {
    pub wprot0: WProt,
    pub ctrl: RW<u32>,
    pub isr: RW<u32>,
    pub imr: RW<u32>,
    pub ier: RW<u32>,
    pub idr: RW<u32>,
    pub itr: RW<u32>,
    unused0: [u32; 57],
    pub wdt_clk_sel: WdtClkSel, // 0x100
    unused1: [u32; 63],
    pub int_fpd: RW<u32>, // 0x200
    unused2: [u32; 898],
    pub gpu: RW<u32>, // 0x100C
    unused3: [u32; 2044],
    pub gdma_cfg: RW<u32>, // 0x3000
    unused4: [u32; 3],
    pub gdma_ram: RW<u32>, // 0x3010
    unused5: [u32; 2043],
    pub afi_fs: RW<u32>, // 0x5000
    unused6: [u32; 1023],
    pub err_atb_isr: RW<u32>, // 0x6000
    pub err_atb_imr: RW<u32>,
    pub err_atb_ier: RW<u32>,
    pub err_atb_idr: RW<u32>,
    pub atb_cmd_store_en: RW<u32>,
    pub atb_resp_en: RW<u32>,
    pub atb_resp_type: RW<u32>,
    unused7: u32,
    pub atb_prescale: RW<u32>,
}
register_at!(RegisterBlock, 0xFD61_0000, fpd_slcr);

impl Unlocked for RegisterBlock {
    fn unlocked<F: FnMut(&mut Self) -> R, R>(mut f: F) -> R {
        let mut self_ = Self::fpd_slcr();
        self_.wprot0.write(WProt::zeroed().active(false));
        let r = f(&mut self_);
        self_.wprot0.write(WProt::zeroed().active(true));
        r
    }
}

register!(wdt_clk_sel, WdtClkSel, RW, u32);
// 0: internal APB clock, 1: external
register_bit!(wdt_clk_sel, select, 0);
