///! System Timestamp Generator - Secure
use libregister::{register, register_at, register_bit, register_bits};

#[repr(C)]
pub struct RegisterBlock {
    pub counter_control: CounterControl,
    pub counter_status: CounterStatus,
    pub current_counter_value_lower: CurrentCounterValueLower,
    pub current_counter_value_upper: CurrentCounterValueUpper,
    unused0: [u32; 4],
    pub base_frequency_id: BaseFrequencyID,
}
register_at!(RegisterBlock, 0xFF26_0000, iou_scntrs);

register!(counter_control, CounterControl, RW, u32);
register_bit!(counter_control, hdbg, 1);
register_bit!(counter_control, en, 0);

register!(counter_status, CounterStatus, RO, u32);
register_bit!(counter_status, dbgh, 1);

register!(
    current_counter_value_lower,
    CurrentCounterValueLower,
    RW,
    u32
);
register_bits!(current_counter_value_lower, count_lower, u32, 0, 31);

register!(
    current_counter_value_upper,
    CurrentCounterValueUpper,
    RW,
    u32
);
register_bits!(current_counter_value_upper, count_upper, u32, 0, 31);

register!(base_frequency_id, BaseFrequencyID, RW, u32);
register_bits!(base_frequency_id, freq, u32, 0, 31);
