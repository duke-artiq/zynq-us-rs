//! CCI400 (CCI_GPV) Registers
use libregister::{register, register_at, register_bit};
use volatile_register::RW;

#[repr(C)]
pub struct RegisterBlock {
    pub control_override_register: ControlOverrideRegister,
    pub speculation_control_register: RW<u32>,
    pub secure_access_register: RW<u32>,
    pub status_register: StatusRegister,
}

register_at!(RegisterBlock, 0xFD6E_0000, cci400);

register!(control_override_register, ControlOverrideRegister, RW, u32);
register_bit!(
    control_override_register,
    disable_retry_reduction_buffers,
    5
);
register_bit!(control_override_register, disable_priority_promotion, 4);
register_bit!(control_override_register, terminate_barriers, 3);
register_bit!(control_override_register, disable_speculative_fetches, 2);
register_bit!(control_override_register, dvm_message_disable, 1);
register_bit!(control_override_register, snoop_disable, 0);

register!(
    speculation_control_register,
    SpeculationControlRegister,
    RW,
    u32
);
register_bit!(speculation_control_register, disable_s4, 20);
register_bit!(speculation_control_register, disable_s3, 19);
register_bit!(speculation_control_register, disable_s2, 18);
register_bit!(speculation_control_register, disable_s1, 17);
register_bit!(speculation_control_register, disable_s0, 16);
register_bit!(speculation_control_register, disable_m2, 2);
register_bit!(speculation_control_register, disable_m1, 1);
register_bit!(speculation_control_register, disable_m0, 0);

register!(status_register, StatusRegister, RO, u32);
register_bit!(status_register, cci_status, 0);
