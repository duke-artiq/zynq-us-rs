use libregister::{register, register_at, register_bits};
use volatile_register::{RO, RW, WO};

#[repr(C)]
pub struct RegisterBlock {
    pub ctrl: RW<u32>,
    pub isr: RW<u32>,
    pub imr: RO<u32>,
    pub ier: WO<u32>,
    pub idr: WO<u32>,
    pub itr: WO<u32>,
    _unused0: u32,
    pub slcr_rpu: RW<u32>,
    pub slcr_adma: SlcrADMA,
    _unused1: [u32; 2],
    pub safety_chk: RW<u32>,
    pub slcr_usb: RW<u32>,
}
register_at!(RegisterBlock, 0xFF4B_0000, lpd_slcr_secure);

register!(slcr_adma, SlcrADMA, RW, u32);
register_bits!(slcr_adma, tz, u8, 0, 7);
