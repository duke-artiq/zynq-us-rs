// Adapted from https://git.m-labs.hk/M-labs/zynq-rs
// Commit: 5a8d714627
// File: experiments/src/main.rs
// Original author: Astro
#![no_std]
#![no_main]
#![feature(panic_info_message)]
#![feature(naked_functions)]
#![feature(stmt_expr_attributes)]

extern crate alloc;

use core::ops::Range;

use libregister::{RegisterR, RegisterRW, RegisterW};
use log::{info, warn};

use libasync::{
    delay,
    smoltcp::{Sockets, TcpStream},
    task,
};
use libboard_zynqmp::{
    clocks, ddr,
    eth::{Eth, Gem, Gem3},
    logger, print, println,
    slcr::{apu, common::Unlocked, crf_apb, crl_apb, iou_slcr, lpd_slcr_secure},
    smoltcp::{
        iface::{Config, Interface},
        time::Instant,
        wire::{EthernetAddress, IpAddress, IpCidr},
    },
    time::Milliseconds,
    timer::GlobalTimer,
};
use libcortex_a53::{
    mmu,
    regs::MPIDR_EL1,
    sync_channel,
    sync_channel::{Receiver, Sender},
};
use libsupport_zynqmp::{allocator::init_alloc_core0, boot::Core1};

extern "C" {
    static mut _vector_table: u64;
}

static mut CORE1_REQ: (Sender<usize>, Receiver<usize>) = sync_channel!(usize, 10);
static mut CORE1_RES: (Sender<usize>, Receiver<usize>) = sync_channel!(usize, 10);

// ethernet MAC address
const HWADDR: [u8; 6] = [0x02, 0x23, 0xde, 0xad, 0xbe, 0xef];

#[no_mangle]
fn main_core0() {
    // setup MIO pins
    iou_slcr::RegisterBlock::unlocked(|iou_slcr| iou_slcr.mio_init());

    // Initialize PLLs, dividers, source selects, etc.
    crl_apb::RegisterBlock::unlocked(|crl_apb| crl_apb.pre_pll_init());
    clocks::Clocks::init();
    logger::init().unwrap();
    log::set_max_level(log::LevelFilter::Debug);
    info!("Clock initialization complete.");

    crf_apb::RegisterBlock::unlocked(|crf_apb| {
        crf_apb
            .rst_fpd_top
            .modify(|_, w| w.swdt_reset(false).fpd_dma_reset(false).gt_reset(false))
    });

    crl_apb::RegisterBlock::unlocked(|crl_apb| {
        let reset_reason = crl_apb.reset_reason.read();
        info!("Reset reason: {:#X}", reset_reason.inner);
        crl_apb.peri_rst_ctrl.modify(|_, w| {
            w.timestamp_rst(false)
                .iou_cc_rst(false)
                .lpd_dma_rst(false)
                .gpio_rst(false)
        });

        crl_apb.rst_lpd_top.modify(|_, w| {
            w.fpd_rst(false)
                .lpd_swdt_rst(false)
                .sysmon_rst(false)
                .rtc_rst(false)
                .apm_rst(false)
                .ipi_rst(false)
                .rpu_pge_rst(false)
                .ocm_rst(false)
        });

        crl_apb.gem_rst_ctrl.modify(|_, w| w.gem3_rst(false));
    });

    lpd_slcr_secure::RegisterBlock::lpd_slcr_secure()
        .slcr_adma
        .write(lpd_slcr_secure::SlcrADMA::zeroed().tz(0xff));

    apu::RegisterBlock::apu()
        .ace_ctrl
        .write(apu::AceCtrl::zeroed());

    crf_apb::RegisterBlock::unlocked(|crf_apb| {
        crf_apb.rst_fpd_top.modify(|_, w| {
            w.s_axi_hpc_3_fpd_reset(false)
                .s_axi_hpc_2_fpd_reset(false)
                .s_axi_hp_1_fpd_reset(false)
                .s_axi_hp_0_fpd_reset(false)
                .s_axi_hpc_1_fpd_reset(false)
                .s_axi_hpc_0_fpd_reset(false)
        });
    });

    crl_apb::RegisterBlock::unlocked(|crl_apb| {
        crl_apb.rst_lpd_top.modify(|_, w| w.s_axi_lpd_rst(false));
    });

    info!("Initializing SDRAM...");
    let ddr_config = ddr::spd::read_spd_eeprom();
    let mut ddr = ddr::DdrRam::ddr_ram(ddr_config);
    ddr.init();
    info!("SDRAM initialization complete.");

    info!("Updating SDRAM MMU entries...");
    ddr.update_mmu_entries();
    info!("Done.");

    info!("Initializing heap");
    init_alloc_core0();

    Core1::start(unsafe { &_vector_table as *const _ as u64 });

    let core1_req = unsafe { &mut CORE1_REQ.0 };
    let core1_res = unsafe { &mut CORE1_RES.1 };
    task::block_on(async {
        for i in 0..10 {
            core1_req.async_send(i).await;
            let j = core1_res.async_recv().await;
            println!("{} -> {}", i, j);
        }
    });
    unsafe {
        core1_req.drop_elements();
    }

    info!("main done");
    loop {}
}

#[no_mangle]
fn main_cores123() {
    let cpu_id = MPIDR_EL1.read().cpu_id();
    println!("Core {} running", cpu_id);

    let req = unsafe { &mut CORE1_REQ.1 };
    let res = unsafe { &mut CORE1_RES.0 };

    for i in req {
        res.send(i * i);
    }

    loop {}
}

#[allow(unused)]
fn test_eth() {
    let timer = GlobalTimer::start();
    info!("Initializing GEM3");
    let eth = Eth::eth3(HWADDR.clone());
    info!("Starting RX");
    const RX_LEN: usize = 2048;
    let eth = eth.start_rx(RX_LEN);
    info!("Starting TX");
    const TX_LEN: usize = 2048;
    let mut eth = eth.start_tx(TX_LEN);

    let config = Config::new(EthernetAddress(HWADDR).into());
    let mut iface = Interface::new(
        config,
        &mut eth,
        Instant::from_millis(timer.get_time().0 as i64),
    );
    iface.update_ip_addrs(|ip_addrs| {
        ip_addrs
            .push(IpCidr::new(IpAddress::v4(10, 236, 88, 216), 23))
            .unwrap();
    });

    Sockets::init(32);
    const TCP_PORT: u16 = 19;
    let stats = alloc::rc::Rc::new(core::cell::RefCell::new((0, 0)));

    let stats_tx = stats.clone();
    task::spawn(async move {
        // FIXME: 512 KiB (0x8_0000) seems to be the limit -- at 1 MiB tx starts throwing fits
        while let Ok(stream) = TcpStream::accept(TCP_PORT, 0x8_0000, 0x8_0000).await {
            let stats_tx = stats_tx.clone();
            task::spawn(async move {
                let tx_payload = 65_536;
                let tx_data = (0..=255)
                    .cycle()
                    .take(tx_payload)
                    .collect::<alloc::vec::Vec<u8>>();
                loop {
                    match stream.send_slice(&tx_data[..]).await {
                        Ok(()) => stats_tx.borrow_mut().1 += tx_data.len(),
                        Err(e) => {
                            warn!("tx: {e}");
                            break;
                        }
                    }
                }
            });
        }
    });

    let stats_rx = stats.clone();
    task::spawn(async move {
        while let Ok(stream) = TcpStream::accept(TCP_PORT + 1, 0x8_0000, 0x8_0000).await {
            let stats_rx = stats_rx.clone();
            task::spawn(async move {
                loop {
                    match stream.recv(|buf| (buf.len(), buf.len())).await {
                        Ok(len) => stats_rx.borrow_mut().0 += len,
                        Err(e) => {
                            warn!("rx: {e}");
                            break;
                        }
                    }
                }
            });
        }
    });

    let mut countdown = timer.countdown();
    task::spawn(async move {
        loop {
            delay(&mut countdown, Milliseconds(1000)).await;

            let timestamp = timer.get_us().0;
            let seconds = timestamp / 1_000_000;
            let micros = timestamp % 1_000_000;
            let (rx, tx) = {
                let mut stats = stats.borrow_mut();
                let result = *stats;
                *stats = (0, 0);
                result
            };
            info!(
                "time: {:6}.{:06}s, rx: {:4} Mb/s, tx: {:4} Mb/s",
                seconds,
                micros,
                rx / 125_000,
                tx / 125_000
            );
        }
    });

    info!("Starting TCP stream");
    Sockets::run(&mut iface, &mut eth, || {
        Instant::from_millis(timer.get_time().0 as i64)
    })
}

#[allow(unused)]
fn test_addrs() {
    let crf_apb = crf_apb::RegisterBlock::crf_apb();
    let mut start = &crf_apb.err_ctrl as *const _ as u32;
    let mut end = &crf_apb.rst_ddr_ss as *const _ as u32;
    assert_eq!(start, 0xFD1A_0000);
    assert_eq!(end, 0xFD1A_0108);

    let crl_apb = crl_apb::RegisterBlock::crl_apb();
    start = &crl_apb.err_ctrl as *const _ as u32;
    end = &crl_apb.bank3_status as *const _ as u32;
    assert_eq!(start, 0xFF5E_0000);
    assert_eq!(end, 0xFF5E_0288);

    let iou_slcr = iou_slcr::RegisterBlock::iou_slcr();
    start = &iou_slcr.mio_pin[0] as *const _ as u32;
    end = &iou_slcr.itr as *const _ as u32;
    assert_eq!(start, 0xFF18_0000);
    assert_eq!(end, 0xFF18_0710);

    let gem_regs = Gem3::regs();
    start = &gem_regs.network_control as *const _ as u32;
    end = &gem_regs.type2_compare_3_word_1 as *const _ as u32;
    assert_eq!(start, 0xFF0E_0000);
    assert_eq!(end, 0xFF0E_071C);
}

#[allow(unused)]
fn test_mmu(ddr: bool) {
    let cpu_id = MPIDR_EL1.read().cpu_id();
    println!("MMU test starting on core {}", cpu_id);
    if ddr {
        let ddr_lo = 0x0000_0000..0x8000_0000;
        println!("DDR_LO: [{:#08X}, {:#08X})...", ddr_lo.start, ddr_lo.end);
        test_mmu_range(ddr_lo, 1 << 20);
    }
    let axi = 0x8000_0000..0xC000_0000;
    println!("AXI: [{:#08X}, {:#08X})...", axi.start, axi.end);
    test_mmu_range(axi, 1 << 20);
    let qspi = 0xC000_0000..0xE000_0000;
    println!("QSPI: [{:#08X}, {:#08X})...", qspi.start, qspi.end);
    test_mmu_range(qspi, 1 << 20);
    let pcie_lo = 0xE000_0000..0xF000_0000;
    println!("PCIE_LO: [{:#08X}, {:#08X})...", pcie_lo.start, pcie_lo.end);
    test_mmu_range(pcie_lo, 1 << 20);
    let coresight = 0xF800_0000..0xF900_0000;
    println!(
        "Coresight: [{:#08X}, {:#08X})...",
        coresight.start, coresight.end
    );
    test_mmu_range(coresight, 1 << 20);
    let rpu_ll = 0xF900_0000..0xF920_0000;
    println!("RPU_LL: [{:#08X}, {:#08X})...", rpu_ll.start, rpu_ll.end);
    test_mmu_range(rpu_ll, 1 << 12);
    let periph = 0xFD00_0000..0xFFE0_0000;
    println!(
        "Peripherals, CSU, and PMU: [{:#08X}, {:#08X})...",
        periph.start, periph.end
    );
    test_mmu_range(periph, 1 << 20);
    let tcm_ocm = 0xFFE0_0000..0x1_0000_0000;
    println!("TCM/OCM: [{:#08X}, {:#08X})...", tcm_ocm.start, tcm_ocm.end);
    test_mmu_range(tcm_ocm, 1 << 12);
}

#[allow(unused)]
fn test_mmu_range(range: Range<u64>, step: usize) {
    for va in range.step_by(step) {
        print!("\r{:#08X}", va);
        let untranslated = va & ((1 << 12) - 1);
        let par_el1 = mmu::translate_address(va);
        let pa = (par_el1.pa() << 12) | untranslated;
        assert!(va == pa, "ERROR: VA: {:#08X}, PA: {:#08X}", va, pa);
    }
    println!(" Ok");
}
