ENTRY(_vector_table);

MEMORY
{
    /* 256 kB On-Chip Memory */
    OCM : ORIGIN = 0xFFFC0000, LENGTH = 0x40000
    /* 2 GB SDRAM, low address range (skipping first 2 MB page) */
    SDRAM_LO : ORIGIN = 0x200000, LENGTH = 0x7FE00000 
}

SECTIONS
{
    .heap (NOLOAD) : ALIGN(8)
    {
        __heap0_start = .;
        . += LENGTH(SDRAM_LO);
        __heap0_end = .;
    } > SDRAM_LO
    .text :
    {
        KEEP(*(.text.exceptions));
        *(.text.boot);
        *(.text .text.*);
    } > OCM

    .rodata : ALIGN(8)
    {
        *(.rodata .rodata.*);
    } > OCM

    .data : ALIGN(8)
    {
        *(.data .data.*);
    } > OCM

    .bss (NOLOAD) : ALIGN(8)
    {
        __bss_start = .;
        *(.bss .bss.*);
        . = ALIGN(8);
        __bss_end = .;
    } > OCM

    .irq_stack3 (NOLOAD): ALIGN(16) {
        __irq_stack3_end = .;
        . += 0x1000;
        __irq_stack3_start = .;
    } > OCM

    .irq_stack2 (NOLOAD): ALIGN(16) {
        __irq_stack2_end = .;
        . += 0x1000;
        __irq_stack2_start = .;
    } > OCM

    .irq_stack1 (NOLOAD): ALIGN(16) {
        __irq_stack1_end = .;
        . += 0x1000;
        __irq_stack1_start = .;
    } > OCM

    .irq_stack0 (NOLOAD) : ALIGN(16) {
        __irq_stack0_end = .;
        . += 0x1000;
        __irq_stack0_start = .;
    } > OCM

    .stack3 (NOLOAD): ALIGN(16) {
        __stack3_end = .;
        . += 0x1000;
        __stack3_start = .;
    } > OCM

    .stack2 (NOLOAD): ALIGN(16) {
        __stack2_end = .;
        . += 0x1000;
        __stack2_start = .;
    } > OCM

    .stack1 (NOLOAD): ALIGN(16) {
        __stack1_end = .;
        . += 0x1000;
        __stack1_start = .;
    } > OCM

    .stack0 (NOLOAD) : ALIGN(16) {
        __stack0_end = .;
        . = ORIGIN(OCM) + LENGTH(OCM) - 64;
        __stack0_start = .;
    } > OCM

    /DISCARD/ :
    {
        /* Unused exception related info that only wastes space */
        *(.ARM.exidx);
        *(.ARM.exidx.*);
        *(.ARM.extab.*);
    }
}
