#!/usr/bin/env bash

project_root=$(dirname $(realpath $0))
pushd $project_root
cargo build -p tests --release && cp ./target/aarch64-unknown-none/release/tests ./openocd/tests.elf
popd

