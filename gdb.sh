#!/usr/bin/env bash

project_root=$(dirname $(realpath $0))
pushd $project_root/openocd

# Uses whichever gdb is on your path
# The nix version of gdb (included in the dev shell) supports all target architectures by default
rust-gdb -q -nx -x .gdbinit tests.elf

popd

